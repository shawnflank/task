<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\components\BaseAction;

/**
 * Class BaseController
 * @package frontend\controllers
 * @author 原创脉冲 <QQ：2790684490>
 */
class BaseController extends Controller
{
    use BaseAction;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        $this->layout = false;
        // 指定使用哪个语言翻译
        // Yii::$app->language = 'en';
        return parent::init();
    }
}