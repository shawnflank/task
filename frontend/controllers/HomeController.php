<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/12/8
 * Time: 18:50
 */

namespace frontend\controllers;


use yii\web\Controller;
use Yii;

class HomeController extends Controller
{
    /**
     * 首页
     * @return \yii\web\Response
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionIndex()
    {
        $app_download_link = Yii::$app->debris->config('app_download_link');
        $my_host_link = Yii::$app->request->getHostInfo();
        if ($app_download_link != $my_host_link) {
            return $this->redirect($app_download_link);
        } else {
            return $this->redirect('/wap/index.html');
        }

    }
}