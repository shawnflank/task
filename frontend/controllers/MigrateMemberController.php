<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace frontend\controllers;


use common\helpers\BcMathHelper;
use common\models\member\Account;
use common\models\member\BindingAccount;
use common\models\member\Member;
use common\models\member\MiUser;
use yii\web\Controller;
use Yii;

class MigrateMemberController extends Controller
{
    public function actionIndex()
    {
        // 解除内存限制
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $last_model = Member::find()->select(['id'])->orderBy(['id' => SORT_DESC])->one();
        if (!empty($last_model)) {
            $id = $last_model['id'];
        } else {
            $id = 0;
        }
        $old_user = MiUser::find()->where(['>', 'id', $id])->orderBy(['id' => SORT_ASC])->all();
        $have_member = Member::find()->select(['username'])->where(['>', 'id', $id])->column();
        foreach ($old_user as $k1 => $v1) {
            // 添加用户表
            if (!in_array($v1->username, $have_member)) {
                // 开启事务
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $member_model = new Member();
                    $member_model->id = $v1->id;
                    $member_model->username = $v1->username;
                    $member_model->merchant_id = 1;
                    $member_model->password_hash = $v1->password_hash;
                    $member_model->last_ip = $v1->ip ?? Yii::$app->request->getUserIP();
                    $member_model->register_ip = $v1->ip ?? Yii::$app->request->getUserIP();
                    $member_model->nickname = $v1->nickname ?? "Task" . time();
                    $member_model->recommend_id = $v1->p_id;
                    $member_model->notice = $v1->notice;
                    $member_model->save(false);
                    // 添加钱包
                    $account_model = new Account();
                    $account_model->id = $member_model->id;
                    $account_model->member_id = $member_model->id;
                    $account_model->user_money = BcMathHelper::div($v1->wallet);
                    $account_model->accumulate_money = BcMathHelper::div($v1->wallet);
                    $account_model->save(false);
                    // 添加绑定账号
                    $binding_model = new BindingAccount();
                    $binding_model->id = $member_model->id;
                    $binding_model->member_id = $member_model->id;
                    $binding_model->save(false);
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    echo "失败,请重新开始";
                    exit;
                }
            }

        }
        echo "完成";
        exit;
    }
}