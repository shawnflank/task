<?php

namespace api\controllers;


use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use common\enums\StatusEnum;
use common\helpers\ResultDataHelper;

/**
 * 需要授权登录访问基类
 *
 * Class OnAuthController
 * @package api\controllers
 * @property yii\db\ActiveRecord|yii\base\Model $modelClass
 * @author 原创脉冲 <QQ：2790684490>
 */
class OnAuthController extends ActiveController
{
    public $memberId;

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        // 注销系统自带的实现方法
        unset($actions['index'], $actions['update'], $actions['create'], $actions['view'], $actions['delete']);
        // 自定义数据indexDataProvider覆盖IndexAction中的prepareDataProvider()方法
        // $actions['index']['prepareDataProvider'] = [$this, 'indexDataProvider'];
        return $actions;
    }

    /**
     * 验证更新是否本人
     *
     * @param $action
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        // 临时关闭跨域问题
        //指定允许其他域名访问
//        $allow_origin = array(
//            'htts://127.0.0.1',
//        );
//        $origin = isset($_SERVER['HTTP_ORIGIN'])? $_SERVER['HTTP_ORIGIN'] : '';
//        // 指定允许其他域名访问
//        if(in_array($origin, $allow_origin)){
//            header('Access-Control-Allow-Origin:'.$origin);
//        }
//        开放所有域名
//        header('Access-Control-Allow-Origin:*');
//        // 响应类型
//        header('Access-Control-Allow-Methods:*');
//        // 响应头设置
//        header('Access-Control-Allow-Headers:x-requested-with,content-type,access-token');


        // 判断网站是否关闭
        if(!Yii::$app->debris->config('web_switch')){
            throw new NotFoundHttpException('网站已关闭.');
        }
        parent::beforeAction($action);
        // 如果是更改方法,获取id和identity里面的id对别
        if ($action == 'update' && Yii::$app->user->identity->member_id != Yii::$app->request->get('id', null)) {
            throw new NotFoundHttpException('权限不足.');
        }
        // 设置用户ID
        if (isset(Yii::$app->user->identity->member_id)) {
            $this->memberId = Yii::$app->user->identity->member_id;
        }
        return true;

    }

    /**
     * 首页
     *
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query' => $this->modelClass::find()
                ->where(['status' => StatusEnum::ENABLED])
                ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
                ->orderBy('id desc')
                ->asArray(),
            'pagination' => [
                'pageSize' => $this->pageSize,
                'validatePage' => false,// 超出分页不返回data
            ],
        ]);
    }

    /**
     * 创建
     *
     * @return mixed|\yii\db\ActiveRecord
     */
    public function actionCreate()
    {
        /* @var $model \yii\db\ActiveRecord */
        $model = new $this->modelClass();
        $model->attributes = Yii::$app->request->post();
        //$model->member_id = Yii::$app->user->identity->member_id;
        if (!$model->save()) {
            return ResultDataHelper::api(422, $this->getError($model));
        }

        return $model;
    }

    /**
     * 更新
     *
     * @param $id
     * @return mixed|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->attributes = Yii::$app->request->post();
        if (!$model->save()) {
            return ResultDataHelper::api(422, $this->getError($model));
        }

        return $model;
    }

    /**
     * 删除
     *
     * @param $id
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = StatusEnum::DELETE;

        return $model->save();
    }

    /**
     * 单个显示
     *
     * @param $id
     * @return \yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->findModel($id);
    }

    /**
     * @param $id
     * @return \yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        /* @var $model \yii\db\ActiveRecord */
        if (empty($id) || !($model = $this->modelClass::find()->where([
                'id' => $id,
                'status' => StatusEnum::ENABLED,
            ])->andFilterWhere(['merchant_id' => $this->getMerchantId()])->one())) {
            throw new NotFoundHttpException('请求的数据不存在');
        }

        return $model;
    }


}
