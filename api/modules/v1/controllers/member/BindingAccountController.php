<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\controllers\member;


use api\controllers\OnAuthController;
use api\modules\v1\forms\member\UpdateBindingAccountForm;
use common\helpers\ArrayHelper;
use common\helpers\CommonPluginHelper;
use common\helpers\FileUploadHelper;
use common\helpers\ResultDataHelper;
use common\helpers\UploadHelper;
use common\models\member\BindingAccount;
use common\models\member\Member;
use Yii;

class BindingAccountController extends OnAuthController
{
    public $modelClass = BindingAccount::class;

    public function actionUpdateBindingAccount()
    {
        $username = Member::find()->where(['id' => $this->memberId])->select(['username'])->asArray()->one()['username'];
        $form = new UpdateBindingAccountForm(['username' => $username]);
        // wechat 微信 alipay 支付宝 wechat_url 微信收款码 alipay_url
        $scenario_type = Yii::$app->request->post('scenario_type');
        if (empty($scenario_type) || !in_array($scenario_type, $form->scenario_type_array)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "类型不正确");
        }
        $form->scenario = $scenario_type;
        $form->attributes = Yii::$app->request->post();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }
        $model = BindingAccount::findOne(['member_id' => $this->memberId]);
        $binding_account = CommonPluginHelper::notNullArray(ArrayHelper::toArray($form));
        $model->attributes = $binding_account;
        if (!$model->save()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($model));
        }
        return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "绑定成功");
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['index', 'view', 'create', 'update', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}