<?php

namespace api\modules\v1\controllers\member;

use api\controllers\OnAuthController;
use api\modules\v1\forms\member\UpdateMemberForm;
use api\modules\v1\forms\member\UpdatePasswordForm;
use api\modules\v1\forms\member\UpdateSafetyPasswordForm;
use common\helpers\ArrayHelper;
use common\helpers\CommonPluginHelper;
use common\helpers\ResultDataHelper;
use common\models\member\BindingAccount;
use common\models\member\Member;
use Yii;

/**
 * 会员接口
 *
 * Class MemberController
 * @package api\modules\v1\controllers\member
 * @property \yii\db\ActiveRecord $modelClass
 * @author 原创脉冲 <QQ：2790684490>
 */
class MemberController extends OnAuthController
{
    /**
     * @var Member
     */
    public $modelClass = Member::class;


    /**
     * 用户中心资料
     * @return \yii\data\ActiveDataProvider
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionIndex()
    {
        $member = $this->modelClass::getUserInfoByMemberId($this->memberId);
        return $member;
    }

    /**
     * 修改密码
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionUpdatePassword()
    {
        $form = new UpdatePasswordForm();
        $form->attributes = Yii::$app->request->post();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }
        $model = $form->getMember();
        $model->setPassword($form->new_password);
        $model->save();
        return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "修改成功", Yii::$app->services->apiAccessToken->getAccessToken($model, $form->group));
    }

    /**
     * 修改安全密码
     * @return mixed
     * @throws \yii\base\Exception
     */
    public function actionUpdateSafetyPassword()
    {

        $form = new UpdateSafetyPasswordForm();
        $form->attributes = Yii::$app->request->post();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }
        $model = BindingAccount::findOne(['member_id' => $this->memberId]);
        $model->setPassword($form->new_safety_password);
        $model->save();
        return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "修改成功");
    }

    /**
     * 修改个人资料
     * @return mixed
     */
    public function actionUpdateMember()
    {
        $form = new UpdateMemberForm();
        $scenario_type = Yii::$app->request->post('scenario_type');
        if (empty($scenario_type) || !in_array($scenario_type, $form->scenario_type_array)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "类型不正确");
        }
        $form->scenario = $scenario_type;
        $form->attributes = Yii::$app->request->post();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }
        $model = Member::findOne($this->memberId);
        $member = CommonPluginHelper::notNullArray(ArrayHelper::toArray($form));
        $model->attributes = $member;
        if (!$model->save()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($model));
        }
        return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "修改成功");
    }




    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}
