<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\controllers\member;


use api\controllers\OnAuthController;
use api\modules\v1\forms\member\QualificationAuditForm;
use common\helpers\ArrayHelper;
use common\helpers\ResultDataHelper;
use common\models\member\BindingAccount;
use common\models\member\Member;
use common\models\member\QualificationAuditLists;
use Yii;

class QualificationAuditListsController extends OnAuthController
{
    /**
     * @var QualificationAuditLists
     */
    public $modelClass = QualificationAuditLists::class;

    /**
     * 资质提交
     * @return mixed|\yii\db\ActiveRecord
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionCreate()
    {
        $form = new QualificationAuditForm();
        $form->attributes = Yii::$app->request->post();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }

        // 判断提交资质审核时间段
        $now_time = time();
        $submit_qualification_start_time = Yii::$app->debris->config('submit_qualification_start_time');
        $submit_qualification_end_time = Yii::$app->debris->config('submit_qualification_end_time');
        if ($now_time < strtotime($submit_qualification_start_time) || $now_time > strtotime($submit_qualification_end_time)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '请您在每天' . $submit_qualification_end_time . '到' . $submit_qualification_end_time . '时间段内提交资质进行审核！');
        }

        // 验证是否存在正在审核的内容
        if (QualificationAuditLists::find()->where(['member_id' => $this->memberId, 'status' => 0])->exists()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "您提交的信息正在审核中,请耐心等待");
        }

        // 获取用户是否已通过资质验证
        $model = new QualificationAuditLists();
        if (QualificationAuditLists::find()->where(['member_id' => $this->memberId, 'status' => 1])->exists()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "您已通过资质审核,无需再次提交");
        }

        // 判断资质已提交次数
        $max_submit_qualification = Yii::$app->debris->config('max_submit_qualification');
        if ($max_submit_qualification > 0 && QualificationAuditLists::find()->select(['id'])->where(['member_id' => $this->memberId, 'status' => 2])->count() >= $max_submit_qualification) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "您提交的资质审核次数已超过最大值,请联系客服解决");
        }
        $qualificationAudit = ArrayHelper::toArray($form);
        if (empty(Yii::$app->debris->config('qualification_audit_input_prompt'))) {
            $qualificationAudit['verification_text_data'] = $form->verification_text_data;
        }
        $qualificationAudit['verification_img_data'] = serialize($qualificationAudit['verification_img_data']);
        $model->attributes = $qualificationAudit;
        $memberInfo = Member::findOne($this->memberId);
        //判断如果用户有绑定微信号
        if (!empty($memberInfo->w_id)) {
            $model->w_id = $memberInfo->w_id;
        }
        if ($model->save()) {
            // 向后台发送消息
            Yii::$app->services->webSocket->sendMessageToGroup($this->memberId, '资质审核', '您有新的资质审核信息，请及时处理！', '/backend/member/qualification-audit-lists/index');
            return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "提交成功,请等待管理员审核！");
        }
        return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "提交时错问题了！");
    }


    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'delete', 'index'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}