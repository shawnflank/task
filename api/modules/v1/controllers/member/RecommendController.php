<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\controllers\member;


use api\controllers\OnAuthController;
use common\helpers\ResultDataHelper;
use common\models\member\CreditsLog;
use common\models\member\Member;
use common\models\task\TaskBill;
use Yii;

class RecommendController extends OnAuthController
{
    /**
     * @var \yii\db\ActiveRecord
     */
    public $modelClass = Member::class;

    /**
     * 我的推荐人列表
     * @return array|\yii\data\ActiveDataProvider|\yii\db\ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionIndex()
    {
        $page = Yii::$app->request->get('page');
        $id = Yii::$app->request->get('id');
        if (empty($id)) {
            $id = $this->memberId;
        }
        $model = Member::find()->where(['id'=>$this->memberId])->select(['recommend_number'])->asArray()->one();
        $model['lists'] = Member::getRecommendListsByRecommendId($id, $page);
        return $model;
    }

    /**
     * 根据下级id,查询他完成的任务我赚的佣金
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionDetail()
    {
        $id = Yii::$app->request->get('id');
        if (empty($id)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "会员ID必须填写");
        }
        return TaskBill::getTaskBillByMemberId($id);

    }


    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}