<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/25
 * Time: 1:55
 */

namespace api\modules\v1\controllers\member;


use api\controllers\OnAuthController;
use common\helpers\ResultDataHelper;
use common\models\member\RechargeBill;
use Yii;

class RechargeBillController extends OnAuthController
{
    public $modelClass = RechargeBill::class;

    /**
     * 添加充值记录
     * @return mixed|\yii\db\ActiveRecord
     */
    public function actionCreate()
    {
        if (intval(Yii::$app->debris->config('recharge_switch')) === 0) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "充值功能暂未开启！");
        }
        $model = new RechargeBill();
        $model->attributes = Yii::$app->request->post();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $model->save(false)) {
            // 向后台发送消息
            Yii::$app->services->webSocket->sendMessageToGroup($this->memberId, '充值审核', '您有新的充值信息，请及时审核！', '/backend/member/recharge-bill/index');
            return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "提交成功,请等待管理员审核！");
        } else {
            $error = array_values($model->errors) ? array_values($model->errors) : [['系统繁忙,请稍后再试!']];
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $error[0][0]);
        }
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['index', 'view', 'update', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}