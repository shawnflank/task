<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/21
 * Time: 4:07
 */

namespace api\modules\v1\controllers\member;


use api\controllers\OnAuthController;
use api\modules\v1\forms\member\CreditsLogForm;
use common\helpers\ResultDataHelper;
use common\models\member\CreditsLog;
use common\models\member\Member;
use Yii;

class CreditsLogController extends OnAuthController
{
    /**
     * @var \yii\db\ActiveRecord
     */
    public $modelClass = CreditsLog::class;

    /**
     * 账变记录
     * @return array|mixed|\yii\data\ActiveDataProvider
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionIndex()
    {
        $form = new CreditsLogForm();
        $form->attributes = Yii::$app->request->get();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }

        $return_data = CreditsLog::getLists($this->memberId, $form->page, $form->type, $form->select_type);
        return $return_data;
    }

    /**
     * 获取签到天数和累积签到金额
     * @return array|null|\yii\db\ActiveRecord
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionSign()
    {
        return Member::getTypeAmountByMemberId($this->memberId, 4);
    }


    /**
     * 今日收入记录
     * @return mixed
     */
    public function actionTodayIncome()
    {
        $form = new CreditsLogForm();
        $form->attributes = Yii::$app->request->get();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }
        $resultData = CreditsLog::DateGetTodayList($this->memberId, $form->page, $form->type);
        return $resultData;
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}