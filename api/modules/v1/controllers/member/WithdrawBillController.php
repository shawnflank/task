<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/25
 * Time: 1:55
 */

namespace api\modules\v1\controllers\member;


use api\controllers\OnAuthController;
use common\helpers\ResultDataHelper;
use common\models\member\QualificationAuditLists;
use common\models\member\WithdrawBill;


use common\models\task\TaskBill;
use Yii;

class WithdrawBillController extends OnAuthController
{
    public $modelClass = WithdrawBill::class;

    public function actionIndex()
    {
        $page = Yii::$app->request->get('page');
        return WithdrawBill::getWithDrawBillByMemberId($this->memberId, $page);
    }


    /**
     * 添加提现记录
     * @return mixed|\yii\db\ActiveRecord
     */
    public function actionCreate()
    {
        // redis判断并发
        $redis = Yii::$app->redis;
        $member_redis_key = 'member_id_' . $this->memberId . '_withdraw_bill';
        // $redis_result = $redis->get($member_redis_key);
        // if (empty($redis_result)) {
        //     $redis->set($member_redis_key, 1);
        //     $redis->expire($member_redis_key, 5);
        // } else {
        //     return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '请勿频繁操作！');
        // }
        // if (intval(Yii::$app->debris->config('withdraw_switch')) === 0) {
        //     return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "提现功能暂未开启！");
        // }

        // // 判断每日提现次数
        // $today_withdraw_times = Yii::$app->debris->config('today_withdraw_times');
        // if ($today_withdraw_times > 0) {
        //     $today_submit_withdraw_times = WithdrawBill::getTodaySubmitWithdrawTimes($this->memberId);
        //     if ($today_submit_withdraw_times >= $today_withdraw_times) {
        //         return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '今日可申请提现次数为' . $today_withdraw_times . '次，您今日已申请提现' . $today_submit_withdraw_times . '次，请明日再来！');
        //     }
        // }

        // // 判断总提现次数
        // $total_withdraw_times = Yii::$app->debris->config('total_withdraw_times');
        // if ($total_withdraw_times > 0) {
        //     $totalSubmitWithdrawTimes = WithdrawBill::getTotalSubmitWithdrawTimes($this->memberId);
        //     if ($totalSubmitWithdrawTimes >= $total_withdraw_times) {
        //         return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '可申请提现总次数为' . $today_withdraw_times . '次');
        //     }
        // }

        // // 判断是否资质认证
        // if (empty(QualificationAuditLists::find()->where(['member_id' => $this->memberId, 'status' => 1])->exists())) {
        //     return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '您尚未通过资质审核，通过后方可提现！');
        // }

        // // 判断提现时间段
        // $now_time = time();
        // $withdraw_time_start = Yii::$app->debris->config('withdraw_time_start');
        // $withdraw_time_end = Yii::$app->debris->config('withdraw_time_end');
        // if ($now_time < strtotime($withdraw_time_start) || $now_time > strtotime($withdraw_time_end)) {
        //     return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '请您在每天' . $withdraw_time_start . '到' . $withdraw_time_end . '时间段内申请提现！');
        // }

        // // 判断任务完成数
        // $task_count_can_withdraw = Yii::$app->debris->config('task_count_can_withdraw');
        // if (intval($task_count_can_withdraw) > 0) {
        //     // 查询完成的任务数
        //     $task_done_count = TaskBill::find()->select('id')->where(['member_id' => $this->memberId, 'status' => TaskBill::TaskBillStatus_4])->count();
        //     if ($task_done_count < $task_count_can_withdraw) {
        //         return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "请完成" . $task_count_can_withdraw . '次任务后再提交提现申请！');
        //     }
        // }
        $request = Yii::$app->request;
        $post = $request->post(); 

        $model = new WithdrawBill();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $model->save(false)) {
            // 向后台发送消息
          //  Yii::$app->services->webSocket->sendMessageToGroup($this->memberId, '提现审核', '您有新的提现信息，请及时审核！', '/backend/member/withdraw-bill/index');
            return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "提交成功,请等待管理员审核！");
        } else {
            $error = array_values($model->errors) ? array_values($model->errors) : [['系统繁忙,请稍后再试!']];
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $error[0][0]);
        }
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }

    
}