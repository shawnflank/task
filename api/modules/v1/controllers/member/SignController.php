<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/21
 * Time: 2:21
 */

namespace api\modules\v1\controllers\member;


use backend\modules\member\forms\RechargeForm;
use common\enums\AppEnum;
use common\helpers\ResultDataHelper;
use common\models\common\Statistics;
use common\models\member\CreditsLog;
use api\controllers\OnAuthController;


use common\models\member\Member;
use Yii;
use yii\db\Exception;


class SignController extends OnAuthController
{
    /**
     * @var \yii\db\ActiveRecord
     */
    public $modelClass = Member::class;

    /**
     * 签到入口
     * @return mixed|\yii\data\ActiveDataProvider
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionIndex()
    {
        // 获取配置
        // 签到开关 1开0关
        $check_in_switch = Yii::$app->debris->config('check_in_switch');
        // 签到赠送数量
        $check_in_gift_amount = Yii::$app->debris->config('check_in_gift_amount');
        // 签到赠送类型 0积分1余额
        $check_in_gift_reward = Yii::$app->debris->config('check_in_gift_reward');
        if ($check_in_gift_reward == 1) {
            $type = RechargeForm::TYPE_MONEY;
            $remark = "签到成功，获得" . $check_in_gift_amount . "元奖金";
        } else {
            $type = RechargeForm::TYPE_INT;
            $remark = "签到获得，" . $check_in_gift_amount . "点积分";
        }
        if ($check_in_switch == 1 && $check_in_gift_amount > 0) {
            // 判断用户当天已签到次数
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $member_sql = "select * from rf_member where `id`={$this->memberId} and `today_sign_count` = 0 for update";
                $member_lock = Yii::$app->db->createCommand($member_sql)->queryOne();
                if(empty($member_lock)){
                    return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "您今天已经签过到啦！");
                }
                //更新数据
                $member = $this->findModel(['id' => $member_lock['id']]);
                $rechargeForm = new RechargeForm([
                    'type' => $type,
                    'money' => $check_in_gift_amount,
                    'int' => $check_in_gift_amount,
                    'remark' => $remark,
                ]);
                $result = $rechargeForm->save($member, CreditsLog::SIGN_TYPE, AppEnum::API, CreditsLog::CREDIT_GROUP_MEMBER);
                $member->sign_days = $member->sign_days + 1;
                $member->today_sign_count = 1;
                $member->save();
                // 加入统计表
                Statistics::updateSignMember(date("Y-m-d"));
                $transaction->commit();
                return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "签到成功！",
                    ['id' => strval($result['id']),
                        'credit_type' => $result['credit_type'],
                        'num' => $result['num'] . ".00",
                        'remark' => $result['remark'],
                        'created_at' => date('Y-m-d H:i:s', $result['created_at']),
                        'type' => strval($result['type'])]
                );
            } catch (\Exception $e) {
                $transaction->rollBack();
                return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "签到失败！");
            }
        } else {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "签到功能已关闭！");
        }
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }

}