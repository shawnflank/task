<?php

namespace api\modules\v1\controllers\member;

use api\controllers\UserAuthController;
use common\models\member\Auth;

/**
 * Class AuthController
 * @package api\modules\v1\controllers\member
 * @author 原创脉冲 <QQ：2790684490>
 */
class AuthController extends UserAuthController
{
    /**
     * @var Auth
     */
    public $modelClass = Auth::class;
}