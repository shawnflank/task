<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/10
 * Time: 21:54
 */

namespace api\modules\v1\controllers;


use api\controllers\OnAuthController;
use common\helpers\InviteCodeHelper;
use Yii;

class SettingController extends OnAuthController
{

    public $modelClass = '';

    protected $optional = ['index'];

    /**
     * 配置接口
     * @return array|\yii\data\ActiveDataProvider
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionIndex()
    {
        $datas = array(
            // 网站开关
            'web_switch' => intval(Yii::$app->debris->config('web_switch')),
            // 短信开关
            'sms_switch' => intval(Yii::$app->debris->config('sms_switch')),
            // 注册开关
            'register_switch' => intval(Yii::$app->debris->config('register_switch')),
            // 邀请码开关
            'invitation_code_switch' => intval(Yii::$app->debris->config('invitation_code_switch')),
            // 签到开关
            'check_in_switch' => intval(Yii::$app->debris->config('check_in_switch')),
            // 抽奖开关
            'lottery_switch' => intval(Yii::$app->debris->config('lottery_switch')),
            // 网站名称
            'web_site_title' => Yii::$app->debris->config('web_site_title'),
            // 版权所有
            'web_copyright' => Yii::$app->debris->config('web_copyright'),
            // 备案号
            'web_site_icp' => Yii::$app->debris->config('web_site_icp'),
            // 网站logo
            'web_logo' => Yii::$app->debris->config('web_logo'),
            // 最低充值金额
            'minimum_recharge_amount' => intval(Yii::$app->debris->config('minimum_recharge_amount')),
            // 能提现的金额
            'can_withdraw_amount' => explode('/', Yii::$app->debris->config('can_withdraw_amount')),
            // 充值开关
            'recharge_switch' => intval(Yii::$app->debris->config('recharge_switch')),
            // 提现开关
            'withdraw_switch' => intval(Yii::$app->debris->config('withdraw_switch')),
        );
        return $datas;
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }

}