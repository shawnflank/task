<?php

namespace api\modules\v1\controllers;

use backend\modules\member\forms\RechargeForm;
use common\enums\AppEnum;
use common\helpers\ImageHelper;
use common\models\common\Statistics;
use common\models\member\CreditsLog;
use Yii;
use yii\web\NotFoundHttpException;
use common\helpers\ResultDataHelper;
use common\helpers\ArrayHelper;
use common\models\member\Member;
use api\modules\v1\forms\member\UpPwdForm;
use api\controllers\OnAuthController;
use api\modules\v1\forms\member\LoginForm;
use api\modules\v1\forms\RefreshForm;
use api\modules\v1\forms\MobileLogin;
use api\modules\v1\forms\member\SmsCodeForm;
use api\modules\v1\forms\member\RegisterForm;

/**
 * 登录接口
 * Class SiteController
 *
 * @package api\modules\v1\controllers
 * @author 原创脉冲 <QQ：2790684490>
 */
class SiteController extends OnAuthController
{

    public $modelClass = '';

    /**
     * 不用进行登录验证的方法
     *
     * 例如： ['index', 'update', 'create', 'view', 'delete']
     * 默认全部需要验证
     *
     * @var array
     */
    protected $optional = ['login', 'refresh', 'mobile-login', 'sms-code', 'register', 'up-pwd',];

    /**
     * 注册
     *
     * @return array|mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionRegister()
    {
        // 判断注册功能是否开放
        if (!Yii::$app->debris->config('register_switch')) {
            throw new NotFoundHttpException('注册功能暂未开放');
        }

        // 判断同一IP注册数
        $register_ip_max = intval(Yii::$app->debris->config('register_ip_max'));
        if ($register_ip_max > 0) {
            // 拿取当前用户IP
            $register_ip = Yii::$app->request->getUserIP();
            // 根据当前ip查询用户表IP数量
            $user_ip_register_count = Member::find()
                ->where(['register_ip' => $register_ip])->select(['id'])
                ->count();
            if ($user_ip_register_count >= $register_ip_max) {
                return ResultDataHelper::api(422, "当前IP注册量已达到最大值");
            }
        }
        $model = new RegisterForm();
        $model->attributes = Yii::$app->request->post();
        if (!$model->validate()) {
            return ResultDataHelper::api(422, $this->getError($model));
        }
        $member = new Member();
        $userInfo = ArrayHelper::toArray($model);
        $member->attributes = $userInfo;
        $member->password_hash = Yii::$app->security->generatePasswordHash($model->password);
        // 注册商家
        $member->merchant_id = Yii::$app->services->merchant->getId();
        if (!$member->save()) {
            return ResultDataHelper::api(422, $this->getError($member));
        }

        // 如果有推荐人 推荐人数+1
        if(!empty($member->recommend_id)){
            $recommendInfo = Member::findOne($member->recommend_id);
            $recommendInfo->recommend_number += 1;
            $recommendInfo->save(false);
        }

        // 判断是否赠送用户奖金和积分
        // 获取注册赠送金额
        $register_gift_amount = Yii::$app->debris->config('register_gift_amount');
        if ($register_gift_amount > 0) {
            $remark = "注册成功，获得" . $register_gift_amount . "元奖金";
            $type = RechargeForm::TYPE_MONEY;
            $rechargeForm = new RechargeForm([
                'type' => $type, 'money' => $register_gift_amount, 'int' => 0,
                'remark' => $remark,
            ]);
            $rechargeForm->save($member, CreditsLog::REGISTER_TYPE, AppEnum::API, CreditsLog::CREDIT_GROUP_MEMBER);
        }
        // 获取赠送积分
        $register_gift_integral = Yii::$app->debris->config('register_gift_integral');
        if ($register_gift_integral > 0) {
            $remark = "注册成功，获得" . $register_gift_integral . "点积分";
            $type = RechargeForm::TYPE_INT;
            $rechargeForm = new RechargeForm([
                'type' => $type, 'money' => 0,
                'int' => $register_gift_integral, 'remark' => $remark,
            ]);
            $rechargeForm->save($member, CreditsLog::REGISTER_TYPE, AppEnum::API, CreditsLog::CREDIT_GROUP_MEMBER);
        }
        // 加入统计表
        Statistics::updateRegisterMember(date("Y-m-d"));

        return Yii::$app->services->apiAccessToken->getAccessToken($member, $model->group);
    }

    /**
     * 登录
     * @return array|mixed
     * @throws \yii\base\Exception
     * @author "原创脉冲"
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        $model->attributes = Yii::$app->request->post();
        if ($model->validate()) {
            return Yii::$app->services->apiAccessToken->getAccessToken($model->getUser(), $model->group);
        }
        // 返回数据验证失败
        return ResultDataHelper::api(422, $this->getError($model));
    }

    /**
     * 登出
     *
     * @return array|mixed
     */
    public function actionLogout()
    {
        if (Yii::$app->services->apiAccessToken->disableByAccessToken(Yii::$app->user->identity->access_token)) {
            return ResultDataHelper::api(200, '退出成功');
        }

        return ResultDataHelper::api(200, '退出失败');
    }

    /**
     * 重置令牌
     *
     * @return array|mixed
     * @throws \yii\base\Exception
     * @author "原创脉冲"
     */
    public function actionRefresh()
    {
        $model = new RefreshForm();
        $model->attributes = Yii::$app->request->post();
        if (!$model->validate()) {
            return ResultDataHelper::api(422, $this->getError($model));
        }

        return Yii::$app->services->apiAccessToken->getAccessToken($model->getUser(),
            $model->group);
    }

    //    /**
    //     * 手机验证码登录Demo
    //     * @return array|mixed
    //     * @throws \yii\base\Exception
    //     * @author "原创脉冲"
    //     */
    //    public function actionMobileLogin()
    //    {
    //        $model = new MobileLogin();
    //        $model->attributes = Yii::$app->request->post();
    //        if ($model->validate()) {
    //            return Yii::$app->services->apiAccessToken->getAccessToken($model->getUser(), $model->group);
    //        }
    //
    //        // 返回数据验证失败
    //        return ResultDataHelper::api(422, $this->getError($model));
    //    }

    /**
     * 获取验证码
     * @return mixed|null|string
     * @throws \yii\web\UnprocessableEntityHttpException
     * @author "原创脉冲"
     */
    public function actionSmsCode()
    {
        $model = new SmsCodeForm();
        $model->attributes = Yii::$app->request->post();
        if (!$model->validate()) {
            return ResultDataHelper::api(422, $this->getError($model));
        }

        return $model->send();
    }

    /**
     * 密码重置
     * @return array|mixed
     * @throws \yii\base\Exception
     * @author "原创脉冲"
     */
    public function actionUpPwd()
    {
        $model = new UpPwdForm();
        $model->attributes = Yii::$app->request->post();
        if (!$model->validate()) {
            return ResultDataHelper::api(422, $this->getError($model));
        }

        $member = $model->getUser();
        $member->password_hash = Yii::$app->security->generatePasswordHash($model->password);
        if (!$member->save()) {
            return ResultDataHelper::api(422, $this->getError($member));
        }

        return Yii::$app->services->apiAccessToken->getAccessToken($member, $model->group);
    }

    /**
     * 权限验证
     * @param  string $action 当前的方法
     * @param  null $model 当前的模型类
     * @param  array $params $_GET变量
     * @throws \yii\web\BadRequestHttpException
     * @author "原创脉冲"
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action,
            ['index', 'view', 'update', 'create', 'delete'])
        ) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}
