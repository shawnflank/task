<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\controllers;

use api\controllers\OnAuthController;

class TestController extends OnAuthController
{
    public $modelClass = '';
    // 不需要验证的方法
    protected $optional = ['index'];

    public function actionIndex()
    {
    }
}