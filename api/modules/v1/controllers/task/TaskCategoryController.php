<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\controllers\task;

use common\models\task\TaskCategory;
use Yii;

use api\controllers\OnAuthController;

class TaskCategoryController extends OnAuthController
{
    public $modelClass = TaskCategory::class;

    protected $optional = ['index'];


    public function actionIndex()
    {
        $task_category = Yii::$app->cache->get('task_category');
        if (empty($task_category)) {
            $task_category = TaskCategory::getAllCategory();
            // 缓存 7天
            Yii::$app->cache->set('task_category', $task_category, 60 * 60 * 24 * 7);
        }
        return $task_category;
    }


    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}