<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\controllers\task;


use api\controllers\OnAuthController;
use api\modules\v1\forms\task\SubmitForm;
use api\modules\v1\forms\task\TaskBillForm;
use api\modules\v1\forms\task\TaskForm;
use backend\modules\member\forms\RechargeForm;
use common\enums\AppEnum;
use common\helpers\ArrayHelper;
use common\helpers\DateHelper;
use common\helpers\ResultDataHelper;
use common\models\common\Statistics;
use common\models\member\Account;
use common\models\member\BindingAccount;
use common\models\member\CreditsLog;
use common\models\member\Member;
use common\models\member\QualificationAuditLists;
use common\models\task\Task;
use common\models\task\TaskBill;
use common\models\task\TaskCategory;
use Yii;

class TaskController extends OnAuthController
{
    public $modelClass = Task::class;

    /**
     * 任务列表
     * @return array|mixed|\yii\data\ActiveDataProvider|\yii\db\ActiveRecord[]
     */
    public function actionIndex()
    {
        $form = new TaskForm();
        $form->attributes = Yii::$app->request->get();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }
        // 先拿到用户等级
        $level_id = Member::findOne($this->memberId)['level_id'];
        return Task::getLists($form->c_id, $form->page, $level_id);
    }

    /**
     * 最新任务
     * @return array|\yii\db\ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionNewLists()
    {
        // 先拿到用户等级
        $page = Yii::$app->request->get('page', 1);
        $level_id = Member::findOne($this->memberId)['level_id'];
        return Task::getNewLists($level_id, $page);
    }

    /**
     * 任务详情
     * @return array|mixed|\yii\db\ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionDetail()
    {
        $id = Yii::$app->request->get('id');
        if (empty($id)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "没有找到对应的任务");
        }
        return Task::getModelById($id, $this->memberId);
    }


    /**
     * 领取任务
     * @return mixed
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionReceive()
    {
        $t_id = Yii::$app->request->post('t_id');
        if (empty($t_id)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '任务ID必传');
        }

        // redis判断并发
        $redis = Yii::$app->redis;
        $member_redis_key = 'member_id_' . $this->memberId . '_task_number_' . $t_id;
        $redis_result = $redis->get($member_redis_key);
        if (empty($redis_result)) {
            $redis->set($member_redis_key, 1);
            $redis->expire($member_redis_key, 5);
        } else {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '请勿重复操作！');
        }

        // 判断资质
        $qualificationAuditModel = QualificationAuditLists::find()
            ->select(['status'])
            ->where(['member_id' => $this->memberId])
            ->orderBy(['created_at' => SORT_DESC])
            ->one();
        if (empty($qualificationAuditModel)) {
            return ResultDataHelper::api(401, '尚未通过资质认证,通过后方可领取任务');
        }
        if ($qualificationAuditModel['status'] == 2) {
            return ResultDataHelper::api(401, '资质申请被驳回,请重新提交');
        }
        if ($qualificationAuditModel['status'] == 0) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '资质处于审核中,通过后方可领取任务');
        }

        // 判断新手任务完成次数 如果接的不是新手任务
        $this_task = Task::findOne($t_id);
        if ($this_task->c_id != 4) {
            $novice_task_number = Yii::$app->debris->config('novice_task_number');
            // 拿到该用户已经完成的新手任务次数
            $noviceTaskOverNumber = TaskBill::getNoviceTaskOverNumber($this->memberId);
            if ($noviceTaskOverNumber < $novice_task_number) {
                return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '您还剩下' . ($novice_task_number - $noviceTaskOverNumber) . "次新手任务未完成..");
            }
        }

        // 判断领取该任务的任务周期 如果大于0
        if ($this_task->receive_Intervals > 0) {
            //判断上次领取该任务的时间
            $last_get_this_task = TaskBill::lastGetThisTaskBill($this->memberId, $t_id);
            if (!empty($last_get_this_task) && time() < $last_get_this_task['created_at'] + ($this_task->receive_Intervals * 3600)) {
                return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '请在' . date('Y-m-d H:i:s', $last_get_this_task['created_at'] + ($this_task->receive_Intervals * 3600)) . '后再来领取该任务！');
            }
        }

        // 判断现在接的这个任务是否存在以往未完成状态
        if (TaskBill::getThisTaskBillNotOver($this->memberId, $t_id)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '该任务您还未完成，请完成后再来领取！');
        }

        // 判断是否允许多个任务同时进行
        $different_task_switch = intval(Yii::$app->debris->config('different_task_switch'));
        // //0不能,1能 如果不能
        if ($different_task_switch == 0) {
            // 判断用户现在是否有未完成的任务
            $not_over_task_bill = TaskBill::getNotOverTaskBillByMemberId($this->memberId);
            if ($not_over_task_bill) {
                return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '你还存在其它未完成的任务，请完成后再来领取');
            }
        }

        // 查询该任务能接次数,0不限制
        if ($this_task->is_repeat_receive > 0) {
            // 查询用户已经接了该任务的次数
            $get_this_task_bill_count = TaskBill::getThisTaskBillCount($this->memberId, $t_id);
            if ($get_this_task_bill_count >= $this_task->is_repeat_receive) {
                return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '该任务只能领取' . $this_task->is_repeat_receive . '次！');
            }
        }

        // 查询任务数量是否足够
        $redis_key = 'task_number_' . $t_id;
        $redis_result = $redis->get($redis_key);
        if ($redis_result < 1) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '当前任务已被抢光啦');
        }

        // 判断领取任务时间段
        if ($this_task->receive_time != "") {
            $receive_time = explode('~', $this_task->receive_time);
            $between_result = Task::checkIsBetweenTime($receive_time[0], $receive_time[1]);
            if ($between_result == 0) {
                return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '该时段任务不属于发放时期');
            }
        }

        // 判断当前完成任务是否有完成时间限制
        // 如果有完成到领取的时间限制 先取出上次完成过该任务的时间
        $lastOverTaskBill = TaskBill::getLastOverTaskBillByMemberId($this->memberId);
        // 如果有该任务过完的数据
        if (!empty($lastOverTaskBill) && $lastOverTaskBill->task->over_to_recevice_time > 0) {
            $over_to_recevice_time = $lastOverTaskBill->review_time + $lastOverTaskBill->task->over_to_recevice_time * 60;
            if ($over_to_recevice_time > time()) {
                return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '请在' . date("Y-m-d H:i:s", $over_to_recevice_time) . "后再来领取该任务");
            }
        }



        //正式领取任务 开启事务
        $transaction = Yii::$app->db->beginTransaction();
        try {
            // 减去任务数量
            $redis->decr($redis_key);
            $memberInfo = Member::findOne($this->memberId);
            // 扣除任务押金
            if ($this_task->deposit_money > 0) {
                $member_account_info = Account::findOne(['member_id' => $this->memberId]);
                if ($member_account_info->user_money < $this_task->deposit_money) {
                    return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '当前余额不足,领取该任务需要押金' . $this_task->deposit_money . '元');
                }
                $remark = "领取任务成功，扣除押金" . $this_task->deposit_money . "元";
                $type = RechargeForm::TYPE_MONEY;
                $rechargeForm = new RechargeForm([
                    'type' => $type,
                    'money' => $this_task->deposit_money,
                    'int' => 0,
                    'remark' => $remark,
                    'change' => RechargeForm::CHANGE_DECR
                ]);
                $rechargeForm->save($memberInfo, CreditsLog::DEPOSIT_TYPE, AppEnum::API, CreditsLog::CREDIT_GROUP_MEMBER);
            }

            // 消费积分
            if ($this_task->consume_integral > 0) {
                $member_account_info = Account::findOne(['member_id' => $this->memberId]);
                if ($member_account_info->user_integral < $this_task->consume_integral) {
                    return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '当前积分不足,领取该任务需要' . $this_task->consume_integral . '点积分');
                }
                $remark = "领取任务成功，扣除" . $this_task->consume_integral . "点积分";
                $type = RechargeForm::TYPE_INT;
                $rechargeForm = new RechargeForm([
                    'type' => $type,
                    'money' => 0,
                    'int' => $this_task->consume_integral,
                    'remark' => $remark,
                    'change' => RechargeForm::CHANGE_DECR
                ]);
                $rechargeForm->save($memberInfo, CreditsLog::DEPOSIT_TYPE, AppEnum::API, CreditsLog::CREDIT_GROUP_MEMBER);
            }

            $today = DateHelper::today();
            if (!TaskBill::find()->where(['member_id' => $this->memberId])->andWhere(['between', 'created_at', $today['start'], $today['end']])->exists()) {
                // 加入统计
                Statistics::updateGetTaskMember(date("Y-m-d"));
            }
            // 生成订单
            $task_bill = new TaskBill();
            $task_bill->t_id = $t_id;
            $task_bill->member_id = $this->memberId;
            $task_bill->c_id = $this_task->c_id;
            //判断如果用户有绑定微信号
            if (!empty($memberInfo->w_id)) {
                $task_bill->w_id = $memberInfo->w_id;
            }
            $task_bill->save();
            $transaction->commit();
            return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "任务领取成功,请按照要求完成");
        } catch (\Exception $e) {
            $transaction->rollBack();
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "领取失败,请联系客服处理");
        }
    }


    /**
     * 任务进度
     * @return array|mixed|\yii\db\ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionTaskSchedule()
    {
        $form = new TaskBillForm();
        $form->attributes = Yii::$app->request->get();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }
        return TaskBill::getTaskBillByStatus($this->memberId, $form->page, $form->type);
    }


    /**
     * 提交任务
     * @return mixed
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionSubmit()
    {
        $form = new SubmitForm();
        $form->attributes = Yii::$app->request->post();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }

        // 判断提交任务时间段
        $now_time = time();
        $submit_task_start_time = Yii::$app->debris->config('submit_task_start_time');
        $submit_task_end_time = Yii::$app->debris->config('submit_task_end_time');
        if ($now_time < strtotime($submit_task_start_time) || $now_time > strtotime($submit_task_end_time)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, '请您在每天' . $submit_task_start_time . '到' . $submit_task_end_time . '时间段内提交任务！');
        }

        $model = TaskBill::findOne($form->id);
        $taskBill = ArrayHelper::toArray($form);
        $taskBill['task_content'] = serialize($taskBill['task_content']);
        $taskBill['status'] = 2;
        $taskBill['updated_at'] = time();
        $taskBill['task_submit_number'] = $model->task_submit_number + 1;
        $model->attributes = $taskBill;
        $model->save();
        Yii::$app->services->webSocket->sendMessageToGroup($this->memberId, '任务订单', '您有新的任务审核，请及时处理！', '/backend/task/task-bill/index');
        return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "提交成功,请等待管理员审核");
    }


    /**
     * 统计任务正在进行中的任务及预计收益
     * @return array
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionTaskCount()
    {
        return TaskBill::getNotOverTaskBillAllByMemberId($this->memberId);
    }


    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'delete', 'create'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}