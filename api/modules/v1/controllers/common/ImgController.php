<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/24
 * Time: 2:08
 */
namespace api\modules\v1\controllers\common;

use api\controllers\OnAuthController;
use api\modules\v1\forms\common\ImgForm;
use common\helpers\ResultDataHelper;
use common\models\common\ImgDetails;
use Yii;

class ImgController extends OnAuthController
{
    public $modelClass = ImgDetails::class;

    protected $optional = ['index'];

    public function actionIndex()
    {
        $form = new ImgForm();
        $form->attributes = Yii::$app->request->get();
        if (!$form->validate()) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, $this->getError($form));
        }
        return Yii::$app->debris->imgLists($form->key);
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }
}