<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/10
 * Time: 21:54
 */

namespace api\modules\v1\controllers\common;


use api\controllers\OnAuthController;
use common\helpers\InviteCodeHelper;
use common\helpers\ResultDataHelper;
use common\models\member\Member;
use Yii;

class SettingController extends OnAuthController
{

    public $modelClass = '';

    protected $optional = ['index'];

    /**
     * 配置接口
     * @return array|\yii\data\ActiveDataProvider
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionIndex()
    {
        $setting_datas = array(
            // 网站开关
            'web_switch' => intval(Yii::$app->debris->config('web_switch')),
            // 短信开关
            'sms_switch' => intval(Yii::$app->debris->config('sms_switch')),
            // 注册开关
            'register_switch' => intval(Yii::$app->debris->config('register_switch')),
            // 邀请码开关
            'invitation_code_switch' => intval(Yii::$app->debris->config('invitation_code_switch')),
            // 签到开关
            'check_in_switch' => intval(Yii::$app->debris->config('check_in_switch')),
            // 抽奖开关
            'lottery_switch' => intval(Yii::$app->debris->config('lottery_switch')),
            // 微信注册开关
            'wechat_register_switch' => intval(Yii::$app->debris->config('wechat_register_switch')),
            // 网站名称
            'web_site_title' => Yii::$app->debris->config('web_site_title'),
            // 版权所有
            'web_copyright' => Yii::$app->debris->config('web_copyright'),
            // 备案号
            'web_site_icp' => Yii::$app->debris->config('web_site_icp'),
            // 网站logo
            'web_logo' => Yii::$app->debris->config('web_logo'),
            // 最低充值金额
            'minimum_recharge_amount' => intval(Yii::$app->debris->config('minimum_recharge_amount')),
            // 能提现的金额
            'can_withdraw_amount' => explode('/', Yii::$app->debris->config('can_withdraw_amount')),

            // 资质审核内容
            // 1.标题
            'qualification_audit_title' => Yii::$app->debris->config('qualification_audit_title'),
            // 2.示例图
            'qualification_audit_images' => unserialize(Yii::$app->debris->config('qualification_audit_images')),
            // 3.上传提示
            'qualification_audit_upload_prompt' => Yii::$app->debris->config('qualification_audit_upload_prompt'),
            // 4.上传张数
            'qualification_audit_upload_number' => intval(Yii::$app->debris->config('qualification_audit_upload_number')),
            // 5.账号输入提示
            'qualification_audit_input_prompt' => Yii::$app->debris->config('qualification_audit_input_prompt'),

            // 客服连接
            'customer_service_link' => Yii::$app->debris->config('customer_service_link'),
            // 当前版本号
            'present_version_number' => Yii::$app->debris->config('present_version_number'),
            // APP下载地址
            'app_download_link' => Yii::$app->debris->config('app_download_link'),
            // APP更新内容
            'app_update_content' => Yii::$app->debris->config('app_update_content'),
            //资质审核微信
            'wechat_number' => Yii::$app->debris->config('wechat_number'),

            // 跑马灯消息
            'marquee_placard' => Yii::$app->debris->config('marquee_placard'),
            // 弹窗广告
            'pop_ups_placard' => Yii::$app->debris->config('pop_ups_placard'),
            // 提现方式
            'withdraw_method' => unserialize(Yii::$app->debris->config('withdraw_method')),
        );
        return $setting_datas;
    }

    /**
     * 资质审核微信号
     * @return array
     * @author 哈哈
     */
    public function actionWechatNumber()
    {
        $wechat_number = Yii::$app->debris->config('wechat_number');
        if (empty($wechat_number)) {
            $member = Member::find()
                ->where(['id' => $this->memberId])
                ->select(['id', 'w_id'])
                ->with(['wechatNumber' => function ($query) {
                    $query->select(['id', 'wechat_number']);
                }])
                ->asArray()
                ->one();
            if (!empty($member['wechatNumber']['wechat_number'])) {
                return ['wechat_number' => "官方审核微信：" . $member['wechatNumber']['wechat_number']];
            } else {
                return ['wechat_number' => ""];
            }
        } else {
            return ['wechat_number' => $wechat_number];
        }
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }

}