<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\controllers\common;


use api\controllers\OnAuthController;
use common\helpers\ResultDataHelper;
use Yii;

class RichTextController extends OnAuthController
{
    public $modelClass = '';

    protected $optional = ['index'];

    /**
     * 富文本页面
     * @return bool|string|\yii\data\ActiveDataProvider
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionIndex()
    {
        $key = Yii::$app->request->get('key');
        if (empty($key)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "关键字必传");
        }
        $setting_datas = Yii::$app->debris->config($key);
        return $setting_datas;
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'delete', 'create'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }

}