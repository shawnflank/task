<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/12/8
 * Time: 19:02
 */

namespace api\modules\v1\controllers\common;


use api\controllers\OnAuthController;
use common\helpers\ResultDataHelper;
use common\models\common\Config;
use Yii;

class AppUploadController extends OnAuthController
{
    public $modelClass = "";

    protected $optional = ['index', 'update-plist'];

    /**
     * APP下载信息
     */
    public function actionIndex()
    {
        // 安卓 app大小
        $android_app_url = $this->checkUrl(Yii::$app->debris->config('android_app'));
        $android_file_size = round(get_headers($android_app_url, true)['Content-Length'] / 1024 / 1024, 2);
        // IOS app大小
        $ios_app = $this->checkUrl(Yii::$app->debris->config('ios_app'));
        $ios_file_size = round(get_headers($ios_app, true)['Content-Length'] / 1024 / 1024, 2);
        // IOS plist文件
        $ios_app_url = $this->checkUrl(Yii::$app->debris->config('ios_plist'));
        // 安装更新时间
        $android_app_time = date("Y-m-d H:i:s", Config::find()->where(['name' => 'android_app'])->select(['updated_at'])->column()[0]);
        // ios更新时间
        $ios_app_time = date("Y-m-d H:i:s", Config::find()->where(['name' => 'ios_app'])->select(['updated_at'])->column()[0]);
        $return_data = array(
            'app_name' => Yii::$app->debris->config('app_name'),
            'web_logo' => $this->checkUrl(Yii::$app->debris->config('web_logo')),
            'present_version_number' => Yii::$app->debris->config('present_version_number'),
            'app_download_link' => Yii::$app->debris->config('app_download_link'),
            'app_upload_url' => $this->checkUrl(Yii::$app->debris->config('app_upload_url')),
            'android_app_url' => $android_app_url,
            'android_file_size' => $android_file_size,
            'ios_app_url' => $ios_app_url,
            'ios_file_size' => $ios_file_size,
            'android_app_time' => $android_app_time,
            'ios_app_time' => $ios_app_time,
        );
        return $return_data;
    }

    public function actionUpdatePlist()
    {
        $ios_url = Yii::$app->request->post('ios_url');
        $bundle_identifier = Yii::$app->request->post('bundle_identifier');
        $app_name = Yii::$app->request->post('app_name');
        $present_version_number = Yii::$app->request->post('present_version_number');
        if (empty($ios_url) || empty($bundle_identifier) || empty($app_name) || empty($present_version_number)) {
            return ResultDataHelper::api(ResultDataHelper::ERROR_CODE, "更新失败");
        }
        $this->updatePlist($this->checkUrl($ios_url), $bundle_identifier, $app_name, $present_version_number);
        return ResultDataHelper::api(ResultDataHelper::SUCCESS_CODE, "更新成功");
    }

    /**
     * 权限验证
     * @param string $action
     * @param null $model
     * @param array $params
     * @throws \yii\web\BadRequestHttpException
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        // 方法名称
        if (in_array($action, ['view', 'update', 'create', 'delete'])) {
            throw new \yii\web\BadRequestHttpException('权限不足');
        }
    }

    public function checkUrl($url)
    {
        $first = mb_substr($url, 0, 1);
        if ($first == "/") {
            $url = $this->get_domain() . $url;
        }
        return $url;
    }

    public function get_domain()
    {
        /* 协议 */
        $protocol = (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) != 'off')) ? 'https://' : 'http://';
        /* 域名或IP地址 */
        if (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
            $host = $_SERVER['HTTP_X_FORWARDED_HOST'];
        } elseif (isset($_SERVER['HTTP_HOST'])) {
            $host = $_SERVER['HTTP_HOST'];
        } else {
            /* 端口 */
            if (isset($_SERVER['SERVER_PORT'])) {
                $port = ':' . $_SERVER['SERVER_PORT'];
                if ((':80' == $port && 'http://' == $protocol) || (':443' == $port && 'https://' == $protocol)) {
                    $port = '';
                }
            } else {
                $port = '';
            }
            if (isset($_SERVER['SERVER_NAME'])) {
                $host = $_SERVER['SERVER_NAME'] . $port;
            } elseif (isset($_SERVER['SERVER_ADDR'])) {
                $host = $_SERVER['SERVER_ADDR'] . $port;
            }
        }
        return $protocol . $host;
    }

    public function updatePlist($ios_url, $bundle_identifier, $app_name, $present_version_number)
    {
        $web_logo = $this->checkUrl(Yii::$app->debris->config('web_logo'));
        $myfile = fopen("task.plist", "w") or die("Unable to open file!");
        $content = '<?xml version="' . $present_version_number . '" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="' . $present_version_number . '">
<dict>
    <key>items</key>
    <array>
        <dict>
            <key>assets</key>
            <array>
                <dict>
                    <key>kind</key>
                    <string>software-package</string>
                    <key>url</key>
                    <string>' . $ios_url . '</string> 
                </dict>
				<dict>
                   <key>kind</key>
                   <string>display-image</string>
                   <key>needs-shine</key>
                   <true/>
                   <key>url</key>
                   <string>' . $web_logo . '</string>
               </dict>
            </array>
            <key>metadata</key>
            <dict>
                <key>bundle-identifier</key>
                <string>' . $bundle_identifier . '</string>
                <key>bundle-version</key>
                <string>' . $present_version_number . '</string>
                <key>kind</key>
                <string>software</string>
                <key>title</key>
                <string>' . $app_name . '</string>
            </dict>
        </dict>
    </array>
</dict>
</plist>';
        fwrite($myfile, $content);
        fclose($myfile);
    }
}