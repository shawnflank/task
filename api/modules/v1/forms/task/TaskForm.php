<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------


namespace api\modules\v1\forms\task;

use common\models\task\TaskCategory;
use yii\base\Model;

class TaskForm extends Model
{
    public $page;
    public $c_id;

    public function rules()
    {
        return [
            [['c_id'], 'required'],
            [['page', 'c_id' ], 'integer'],
            ['c_id', 'in', 'range' => TaskCategory::find()->select(['id'])->where(['status'=>1])->column()]
        ];
    }

    public function attributeLabels()
    {
        return [
            'page'=>'页码',
            'c_id'=>'类型',
        ];
    }
}