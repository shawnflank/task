<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\forms\task;


use common\models\task\Task;
use common\models\task\TaskBill;
use yii\base\Model;
use Yii;

class SubmitForm extends Model
{
    public $id;
    public $task_content;

    public function rules()
    {
        return [
            [['id', 'task_content'], 'required'],
            ['task_content', 'validateContent'],
        ];
    }

    /**
     * 验证提交内容
     * @param $attribute
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function validateContent($attribute)
    {
        if (!$this->hasErrors()) {
            if(!is_array($this->task_content)){
                $this->addError($attribute, "请上传数组形式的图片");
                return;
            }
            // 获取任务
            $task_bill = TaskBill::find()
                ->where(['id' => $this->id, 'status' => 1])
                ->orWhere(['id' => $this->id, 'status' => 3])
                ->with('task')
                ->one();
            if (empty($task_bill)) { // 未审核和审核不通过的任务才能提交
                $this->addError($attribute, "当前任务不存在或已完成");
                return;
            } elseif (count($this->task_content) != $task_bill->task->upload_img_number) { // 判断图片张数
                $this->addError($attribute, "必须上传" . $task_bill->task->upload_img_number . "张图片");
                return;
            } elseif ($task_bill->task_submit_number >= $task_bill->task->reject_submit_switch) { // 任务已提交次数大于等于允许提交次数
                $this->addError($attribute, "当前任务提交次数已超过该任务提交限制");
                return;
            } elseif ($task_bill->task->receive_to_submit_time != 0 && time() < $receive_to_submit_time = $task_bill->created_at+ $task_bill->task->receive_to_submit_time * 60) { //判断提交时间
                $this->addError($attribute, '请在'.date('Y-m-d H:i:s',$receive_to_submit_time)."后再来提交任务");
                return;
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => '任务ID',
            'task_content' => '提交内容',
        ];
    }
}