<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/24
 * Time: 2:17
 */

namespace api\modules\v1\forms\common;

use common\models\common\ImgDetails;
use yii\base\Model;

class ImgForm extends Model
{
    public $key;

    public function rules()
    {
        return [
            [['key'], 'required'],
            ['key', 'in', 'range' => ImgDetails::getImgKeyArray()]
        ];
    }

    public function attributeLabels()
    {
        return [
            'key' => '标识符',
        ];
    }
}