<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/24
 * Time: 19:48
 */

namespace api\modules\v1\forms\member;


use common\helpers\RegularHelper;
use common\models\common\SmsLog;
use common\models\member\Member;
use common\models\validators\SmsCodeValidator;
use yii\base\Model;

class UpdateMemberForm extends Model
{
    public $scenario_type;
    public $nickname;
    public $head_portrait;
    public $username;
    public $code;

    public $scenario_type_array = [
        'nickname', 'head_portrait', 'username'
    ];

    public function rules()
    {
        return [
            [['scenario_type'], 'required'],
            [['code', 'username'], 'required', 'on' => 'username'],
            [['nickname'], 'required', 'on' => 'nickname'],
            [['head_portrait'], 'required', 'on' => 'head_portrait'],
            ['code', SmsCodeValidator::class, 'usage' => SmsLog::UPDATE_USERNAME],
            [
                ['username'],
                'unique',
                'targetClass' => Member::class,
                'targetAttribute' => 'username',
                'message' => '此{attribute}已存在。'
            ],
            ['username', 'match', 'pattern' => RegularHelper::mobile(), 'message' => '请输入正确的手机号码'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'scenario_type' => '类型',
            'nickname' => '昵称',
            'head_portrait' => '头像',
            'code' => '验证码',
            'username' => '手机号',
        ];
    }
}