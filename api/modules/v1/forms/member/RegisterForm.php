<?php

namespace api\modules\v1\forms\member;

use common\models\validators\InvitationCodeValidator;
use yii\base\Model;
use common\helpers\RegularHelper;
use common\models\member\Member;
use common\models\api\AccessToken;
use common\models\common\SmsLog;
use common\models\validators\SmsCodeValidator;
use Yii;

/**
 * Class RegisterForm
 * @package api\modules\v1\forms
 * @author 原创脉冲 <QQ：2790684490>
 */
class RegisterForm extends Model
{
    public $username;
    public $password;
    public $password_repetition;
    public $code;
    public $group;
    public $invitation_code;
    public $recommend_id;
    public $phone_identifier;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        // 定义必填字段
        $register_array = ['username', 'group', 'password', 'password_repetition'];
        //如果短信验证码开启
        if (Yii::$app->debris->config('sms_switch')) {
            $register_array[] = 'code';
            $code_array = ['code', SmsCodeValidator::class, 'usage' => SmsLog::USAGE_REGISTER];
        }
        // 如果邀请码开启
        if (Yii::$app->debris->config('invitation_code_switch')) {
            $register_array[] = 'invitation_code';
        }
        //如果手机唯一标识符开关开启
        if(Yii::$app->debris->config('phone_identifier_switch')){
            $register_array[] = 'phone_identifier';
            $phone_identifier = [
                ['phone_identifier'],
                'unique',
                'targetClass' => Member::class,
                'targetAttribute' => 'phone_identifier',
                'message' => '您当前的手机型号只能注册一个账号！'
            ];
        }

        $return_array = [
            [$register_array, 'required'],
            [['password'], 'string', 'min' => 6],
            [['password'], 'string', 'max' => 20],
            [['phone_identifier'], 'string', 'max' => 255],
            [
                ['username'],
                'unique',
                'targetClass' => Member::class,
                'targetAttribute' => 'username',
                'message' => '此{attribute}已存在。'
            ],
            ['username', 'match', 'pattern' => RegularHelper::mobile(), 'message' => '请输入正确的手机号码'],
            [['password_repetition'], 'compare', 'compareAttribute' => 'password'],// 验证新密码和重复密码是否相等
            ['group', 'in', 'range' => AccessToken::$ruleGroupRnage],
            ['invitation_code', InvitationCodeValidator::class],
        ];
        if (!empty($code_array)) {
            $return_array[] = $code_array;
        }
        if (!empty($invitation_code)) {
            $return_array[] = $invitation_code;
        }
        //如果手机唯一标识符开关开启
        if(!empty($phone_identifier)){
            $return_array[] = $phone_identifier;
        }
        return $return_array;
    }

    public function attributeLabels()
    {
        return [
            'username'=>'手机号码',
            'password' => '密码',
            'password_repetition' => '重复密码',
            'group' => '类型',
            'code' => '验证码',
            'invitation_code' => '邀请码',
            'phone_identifier' => '手机唯一标识符',
        ];
    }
}