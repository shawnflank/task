<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\forms\member;


use common\models\common\SmsLog;
use common\models\member\BindingAccount;
use common\models\validators\SmsCodeValidator;
use yii\base\Model;
use Yii;

class UpdateBindingAccountForm extends Model
{
    public $alipay_account_url;
    public $alipay_account;
    public $alipay_user_name;
    public $wechat_account;
    public $wechat_account_url;
    public $scenario_type;//1微信 2支付宝
    public $code;//验证码
    public $username;//手机号


    public $scenario_type_array = ['wechat', 'alipay', 'wechat_url','alipay_url'];


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['scenario_type'], 'required'],
            [['code'], 'required', 'on' => ['wechat_url', 'alipay','alipay_url']],
            ['code', SmsCodeValidator::class, 'usage' => SmsLog::BINDING_ACCOUNT],
            [['alipay_account', 'alipay_user_name'], 'required', 'on' => 'alipay'],
            [['wechat_account'], 'required', 'on' => 'wechat'],
            [['wechat_account_url'], 'required', 'on' => 'wechat_url'],
            [['alipay_account_url'], 'required', 'on' => 'alipay_url'],
            [['alipay_account', 'alipay_user_name', 'wechat_account'], 'string', 'max' => 50],
            [['wechat_account_url','alipay_account_url'], 'string', 'max' => 255],
            [
                ['alipay_account'],
                'unique',
                'targetClass' => BindingAccount::class,
                'targetAttribute' => 'alipay_account',
                'message' => '此{attribute}已存在。',
                'on' => 'alipay',
            ],
            [
                ['alipay_user_name'],
                'unique',
                'targetClass' => BindingAccount::class,
                'targetAttribute' => 'alipay_user_name',
                'message' => '此{attribute}已存在。',
                'on' => 'alipay'
            ],
            [
                ['wechat_account'],
                'unique',
                'targetClass' => BindingAccount::class,
                'targetAttribute' => 'wechat_account',
                'message' => '该微信号已被绑定',
                'on' => 'wechat'
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'alipay_account_url' => '支付宝收款码',
            'alipay_account' => '支付宝账号',
            'alipay_user_name' => '支付宝账户名',
            'wechat_account' => '微信账号',
            'wechat_account_url' => '微信收款码',
            'scenario_type' => '类型',
            'code' => '验证码',
        ];
    }
}