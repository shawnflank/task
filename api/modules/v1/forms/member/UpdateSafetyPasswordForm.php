<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\forms\member;


use common\models\member\BindingAccount;
use yii\base\Model;
use Yii;

class UpdateSafetyPasswordForm extends Model
{

    public $old_safety_password;
    public $new_safety_password;
    public $old_safety_password_repetition;
    public $_bindingAccount;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['old_safety_password', 'new_safety_password', 'old_safety_password_repetition'], 'required'],
            [['old_safety_password', 'new_safety_password', 'old_safety_password_repetition'], 'string', 'min' => 6, 'max' => 6],
            [['old_safety_password_repetition'], 'compare', 'compareAttribute' => 'new_safety_password', 'message' => '您输入的俩次安全码不一致'],// 验证新密码和重复密码是否相等
            ['new_safety_password', 'validateNewSafetyPassword'],
        ];
    }

    public function validateNewSafetyPassword($attribute)
    {
        if (!$this->hasErrors()) {
            $bindingAccount = $this->getBindingAccount();
            if (!$bindingAccount || !$bindingAccount->validateSafetyPassword($this->old_safety_password)) {
                $this->addError($attribute, '旧安全码错误!');
            }
        }
    }

    /**
     * 获取信息
     * @return BindingAccount|null
     * @author 原创脉冲 <QQ：2790684490>
     */
    protected function getBindingAccount()
    {
        if ($this->_bindingAccount === null) {
            $this->_bindingAccount = BindingAccount::findOne(['member_id' => Yii::$app->user->identity['member_id']]);
        }
        return $this->_bindingAccount;
    }


    public function attributeLabels()
    {
        return [
            'old_safety_password' => '旧安全码',
            'new_safety_password' => '新安全码',
            'old_safety_password_repetition' => '确定新安全码',
        ];
    }
}