<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/21
 * Time: 4:14
 */

namespace api\modules\v1\forms\member;


use common\models\member\CreditsLog;
use yii\base\Model;

class CreditsLogForm extends Model
{
    public $page;
    public $type;
    public $select_type;

    // 余额或积分
    public static $credit_type_array = ['user_money', 'user_integral'];

    public function rules()
    {
        return [
            [['page', 'type'], 'integer'],
            ['type', 'in', 'range' => CreditsLog::$TypeArray],
            ['select_type', 'in', 'range' => self::$credit_type_array]
        ];
    }

    public function attributeLabels()
    {
        return [
            'page' => '页码',
            'type' => '类型',
            'select_type' => '查询类型',
        ];
    }
}