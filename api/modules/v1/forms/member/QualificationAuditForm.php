<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace api\modules\v1\forms\member;


use common\models\member\Member;
use yii\base\Model;
use Yii;

class QualificationAuditForm extends Model
{
    public $verification_img_data;
    public $verification_text_data;

    public function rules()
    {
        $qualification_audit_input_prompt = Yii::$app->debris->config('qualification_audit_input_prompt');
        if (!empty($qualification_audit_input_prompt)) {
            $required = ['verification_img_data', 'verification_text_data'];
        } else {
            $required = ['verification_img_data'];
        }
        return [
            [$required, 'required'],
            ['verification_img_data', 'validateImg'],
        ];
    }

    /**
     * 验证图片数量
     * @param $attribute
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function validateImg($attribute)
    {
        if (!$this->hasErrors()) {
            if(!is_array($this->verification_img_data)){
                $this->addError($attribute, "请上传数组形式的图片");
                return;
            }
            $qualification_audit_upload_number = intval(Yii::$app->debris->config('qualification_audit_upload_number'));
            if (count($this->verification_img_data) != $qualification_audit_upload_number) {
                $this->addError($attribute, "图片数量不足,需上传" . $qualification_audit_upload_number . "张");
                return;
            }
        }
    }


    public function attributeLabels()
    {
        return [
            'verification_img_data' => '资质资料',
            'verification_text_data' => '平台账号',
        ];
    }

}