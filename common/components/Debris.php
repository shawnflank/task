<?php

namespace common\components;

use common\helpers\ArrayHelper;
use Yii;
use yii\web\UnprocessableEntityHttpException;
use common\enums\CacheKeyEnum;

/**
 * Class Debris
 * @package common\components
 * @author 原创脉冲 <QQ：2790684490>
 */
class Debris
{
    /**
     * 返回配置名称
     *
     * @param string $name 字段名称
     * @param bool $noCache true 不从缓存读取 false 从缓存读取
     * @return bool|string
     */
    public function config($name, $noCache = false)
    {
        // 获取缓存信息
        $info = $this->getConfigInfo($noCache);
        return isset($info[$name]) ? trim($info[$name]) : null;
    }

    /**
     * 获取图片
     * @param $name
     * @param bool $noCache
     * @return mixed|null
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function imgLists($name, $noCache = false)
    {
        // 获取缓存信息
        $info = $this->getImgListsInfo($noCache);
        return isset($info[$name]) ? json_decode(trim($info[$name])) : null;
    }

    /**
     * 返回配置名称
     *
     * @param bool $noCache true 不从缓存读取 false 从缓存读取
     * @return array|bool|mixed
     */
    public function configAll($noCache = false)
    {
        $info = $this->getConfigInfo($noCache);
        return $info ? $info : [];
    }


    /**
     * 返回配置名称
     *
     * @param bool $noCache true 不从缓存读取 false 从缓存读取
     * @return array|bool|mixed
     */
    public function imgListsAll($noCache = false)
    {
        $info = $this->getImgListsInfo($noCache);
        return $info ? $info : [];
    }

    /**
     * 获取全部配置信息
     *
     * @param bool $noCache true 不从缓存读取 false 从缓存读取
     * @return array|mixed
     */
    protected function getConfigInfo($noCache)
    {
        // 获取缓存信息
        $cacheKey = CacheKeyEnum::SYS_CONFIG;
        if (!($info = Yii::$app->cache->get($cacheKey)) || $noCache == true) {
            $config = Yii::$app->services->config->getListWithValue();
            $info = [];
            foreach ($config as $row) {
                $info[$row['name']] = $row['value']['data'] ?? $row['default_value'];
            }

            // 设置缓存 2天
            Yii::$app->cache->set($cacheKey, $info, 60 * 60 * 24 * 2);
        }
        return $info;
    }

    /**
     * 获取图片配置信息
     * @param $noCache
     * @return array|mixed
     */
    protected function getImgListsInfo($noCache)
    {
        // 获取缓存信息
        $cacheKey = CacheKeyEnum::IMG_LISTS_INFO;
        if (!($info = Yii::$app->cache->get($cacheKey)) || $noCache == true) {
            $config = Yii::$app->services->imgLists->getListWithValue();
            $info = [];
            foreach ($config as $row) {
                $info[$row['key']] = json_encode($row['imgDetails']) ?? "";
            }
            // 设置缓存 2天
            Yii::$app->cache->set($cacheKey, $info, 60 * 60 * 24 * 2);
        }
        return $info;
    }

    /**
     * 获取设备客户端信息
     *
     * @return mixed|string
     */
    public function detectVersion()
    {
        /** @var \Detection\MobileDetect $detect */
        $detect = Yii::$app->mobileDetect;
        if ($detect->isMobile()) {
            $devices = $detect->getOperatingSystems();
            $device = '';

            foreach ($devices as $key => $valaue) {
                if ($detect->is($key)) {
                    $device = $key . $detect->version($key);
                    break;
                }
            }

            return $device;
        }

        return $detect->getUserAgent();
    }

    /**
     * 打印
     *
     * @param mixed ...$array
     */
    public function p(...$array)
    {
        echo "<pre>";

        if (count($array) == 1) {
            print_r($array[0]);
        } else {
            print_r($array);
        }

        echo '</pre>';
    }

    /**
     * 解析微信是否报错
     *
     * @param array $message 微信回调数据
     * @param bool $direct 是否直接报错
     * @return bool
     * @throws UnprocessableEntityHttpException
     * @throws \EasyWeChat\Kernel\Exceptions\HttpException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyWeChat\Kernel\Exceptions\RuntimeException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getWechatError($message, $direct = true)
    {
        if (isset($message['errcode']) && $message['errcode'] != 0) {
            // token过期 强制重新从微信服务器获取 token.
            if ($message['errcode'] == 40001) {
                Yii::$app->wechat->app->access_token->getToken(true);
            }

            if ($direct) {
                throw new UnprocessableEntityHttpException($message['errmsg']);
            }

            return $message['errmsg'];
        }

        return false;
    }

    /**
     * 解析错误
     *
     * @param $fistErrors
     * @return string
     */
    public function analyErr($firstErrors)
    {
        if (!is_array($firstErrors) || empty($firstErrors)) {
            return false;
        }

        $errors = array_values($firstErrors)[0];
        return $errors ?? '未捕获到错误信息';
    }
}