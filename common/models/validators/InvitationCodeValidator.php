<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/11
 * Time: 14:00
 */

namespace common\models\validators;


use common\models\member\Member;
use yii\validators\Validator;

class InvitationCodeValidator extends Validator
{
    /**
     * Model或者form中提交的手机号字段名称
     *
     * @var string
     */
    public $invitationCodeAttribute = 'invitation_code';

    public function validateAttribute($model, $attribute)
    {
        $fieldName = $this->invitationCodeAttribute;
        $cellCode = $model->$fieldName;
        // 查找数据库是否有该邀请码
        $memberInfo = Member::find()->where(['invitation_code' => $cellCode])->one();
        if (is_null($memberInfo)) {
            $this->addError($model, $attribute, '您输入的邀请码不正确!');
        } else {
            // 如果正确,保存id
            $model['recommend_id'] = $memberInfo['id'];

        }
    }
}