<?php

namespace common\models\task;

use common\helpers\BcMathHelper;
use common\models\common\ReviewsWechat;
use common\models\member\CreditsLog;
use common\models\member\Member;
use common\models\member\QualificationAuditLists;
use common\models\sys\Manager;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task_task_bill".
 *
 * @property int $id
 * @property int $member_id 领取人ID
 * @property int $t_id 任务ID
 * @property int $task_submit_number 任务提交次数
 * @property array $task_content 提交任务内容
 * @property string $remark 任务备注
 * @property int $status 任务状态，1：进行中，2：审核中，3：审核不通过，4：已完成，5：已失效
 * @property int $created_by 审核人
 * @property int $created_at 任务领取时间
 * @property int $updated_at 提交时间
 * @property int $review_time 审核时间
 * @property int $c_id 任务类型
 * @property int $w_id 绑定微信ID
 */
class TaskBill extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public $notice;
    private $_user;


    const TaskBillStatus_1 = 1; //进行中
    const TaskBillStatus_2 = 2; //审核中
    const TaskBillStatus_3 = 3; //审核不通过
    const TaskBillStatus_4 = 4; //已完成
    const TaskBillStatus_5 = 5; //已失效

    public static $taskBillStatusArray = [
        self::TaskBillStatus_1 => "进行中",
        self::TaskBillStatus_2 => "审核中",
        self::TaskBillStatus_3 => "审核未通过",
        self::TaskBillStatus_4 => "已完成",
        self::TaskBillStatus_5 => "已失效",
    ];

    public static $taskBill_status_color = [
        1 => "primary",
        2 => "info",
        3 => "warning",
        4 => "success",
        5 => "danger",
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_task_bill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 't_id', 'status', 'task_submit_number', 'created_by', 'created_at', 'updated_at', 'review_time', 'c_id', 'w_id'], 'integer'],
            [['task_content'], 'safe'],
            [['remark','notice'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '领取人ID',
            't_id' => '任务ID',
            'task_submit_number' => '提交次数',
            'task_content' => '提交内容',
            'notice' => '会员备注',
            'remark' => '任务备注',
            'status' => '任务状态',
            'created_by' => '审核人',
            'created_at' => '领取时间',
            'updated_at' => '提交时间',
            'review_time' => '审核时间',
            'c_id' => '任务类型',
            'w_id' => '审核信息',
        ];
    }


    /**
     * @return Member|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Member::findOne(Yii::$app->user->identity['member_id']);
        }
        return $this->_user;
    }


    /**
     *  用户任务进度
     * @param $memberId
     * @param $page
     * @param $type
     * @return array|ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getTaskBillByStatus($memberId, $page, $type)
    {
        $pagessize = Yii::$app->params['pagessize']['task_lists'];
        $page = !empty($page) ? $page : 1;
        $pages = ($page - 1) * $pagessize;
        $inner = "(SELECT `id` FROM " . self::tableName() . " b WHERE (`status` = " . $type . " AND `member_id` = $memberId" . ") ORDER BY `created_at` DESC LIMIT " . $pages . " , " . "$pagessize" . ") b using (`id`)";
        $model = self::find()
            ->select(['id', 't_id', 'task_submit_number', 'task_content', 'created_at', 'updated_at'])
            ->innerJoin($inner)
            ->with(['task' => function ($query) {
                $query->select(['id', 'c_id', 'title', 'banner', 'commission', 'deposit_money', 'status', 'release_title', 'remark']);
            }])
            ->asArray()
            ->all();
        return $model;
    }

    /**
     * 关联会员
     */
    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id']);
    }

    /**
     * 关联任务
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 't_id']);
    }

    /**
     * 关联管理员
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'created_by']);
    }

    /**
     * 关联收益表
     */
    public function getCreditsLog()
    {
        return $this->hasOne(CreditsLog::class, ['map_id' => 'id'])->where(['member_id' => Yii::$app->user->identity['member_id']]);
    }


    /**
     * 根据会员ID查看已完成订单及其收益
     * @param $member_id
     * @return array|ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getTaskBillByMemberId($member_id)
    {
        $model = self::find()
            ->select(['id', 't_id', 'member_id'])
            ->where(['member_id' => $member_id, 'status' => 4])
            ->with(['task' => function ($query) {
                $query->select(['id', 'title', 'commission']);
            }])
            ->with(['creditsLog' => function ($query) {
                $query->select(['map_id', 'num', 'FROM_UNIXTIME(`created_at`,\'%Y-%m-%d %H:%i:%s\') as created_at']);
            }])
            ->with(['member' => function ($query) {
                $query->select(['id', 'head_portrait']);
            }])
            ->asArray()
            ->all();
        foreach ($model as $k1 => $v1) {
            if (empty($v1['creditsLog'])) {
                unset($model[$k1]);
            }
        }
        return array_values($model);
    }

    /**
     * 根据会员ID查看是否存在未完成的任务
     * @param $member_id
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getNotOverTaskBillByMemberId($member_id)
    {
        return self::find()
            ->where(['member_id' => $member_id])
            ->andWhere(['<', 'status', TaskBill::TaskBillStatus_4])
            ->exists();
    }

    /**
     * 根据会员ID查看该任务是否有未完成的订单
     * @param $member_id
     * @param $t_id
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getThisTaskBillNotOver($member_id, $t_id)
    {
        return self::find()
            ->where(['member_id' => $member_id, 't_id' => $t_id])
            ->andWhere(['<', 'status', TaskBill::TaskBillStatus_4])
            ->exists();
    }

    /**
     * 根据会员ID和任务ID查看当前是否存在未完成
     * @param $member_id
     * @param $t_id
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getThisTaskBillCount($member_id, $t_id)
    {
        return self::find()
            ->select(['id'])
            ->where(['member_id' => $member_id, 't_id' => $t_id])
            ->count();

    }

    /**
     * 根据会员ID查看上次完成该任务的详情
     * @param $member_id
     * @return array|null|ActiveRecord
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getLastOverTaskBillByMemberId($member_id)
    {
        return self::find()
            ->where([
                'member_id' => $member_id,
                'status' => TaskBill::TaskBillStatus_4
            ])
            ->with('task')
            ->orderBy(['review_time' => SORT_DESC])
            ->one();
    }

    /**
     * 统计任务正在进行中的任务及预计收益
     * @param $member_id
     * @return array
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getNotOverTaskBillAllByMemberId($member_id)
    {
        $model = self::find()
            ->where(['member_id' => $member_id])
            ->with(['task' => function ($query) {
                $query->select(['id', 'commission']);
            }])
            ->andWhere(['<', 'status', TaskBill::TaskBillStatus_4])
            ->asArray()
            ->all();
        $commission_array = array();
        foreach ($model as $k1 => $v1) {
            $commission_array[] = $v1['task']['commission'];
        }
        $reture_data = array(
            'is_doing_task_bill_count' => self::find()->where(['member_id' => $member_id, 'status' => 1])->count(),
            'expected_income' => array_sum($commission_array),
        );
        return $reture_data;
    }

    /**
     * 获取完成新手任务次数
     * @param $member_id
     * @return int|string
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getNoviceTaskOverNumber($member_id)
    {
        return self::find()
            ->where([
                'member_id' => $member_id,
                'c_id' => 4,
                'status' => self::TaskBillStatus_4
            ])
            ->count();
    }

    /**
     * 关联资质资料
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getQualificationAudit()
    {
        return $this->hasOne(QualificationAuditLists::class, ["member_id" => "member_id"]);
    }

    /**
     * 上次领取该任务
     * @param $member_id
     * @param $t_id
     * @return array|null|ActiveRecord
     * @author 哈哈
     */
    public static function lastGetThisTaskBill($member_id, $t_id)
    {
        return self::find()
            ->select(['created_at'])
            ->where(['member_id' => $member_id, 't_id' => $t_id])
            ->orderBy(['created_at' => SORT_DESC])
            ->asArray()
            ->one();
    }

    /**
     * 关联微信号
     * @return \yii\db\ActiveQuery
     * @author 哈哈
     */
    public function getWechatNumber()
    {
        return $this->hasOne(ReviewsWechat::class, ['id' => 'w_id']);
    }
}
