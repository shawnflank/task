<?php

namespace common\models\task;

use Yii;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "task_task_label".
 *
 * @property int $id
 * @property string $label 标签
 * @property string $remark 标签说明
 * @property int $created_at 添加时间
 * @property int $created_by 添加人
 * @property int $status 状态
 */
class TaskLabel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_task_label';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ],
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['label','status','remark'], 'required'],
            [['created_at', 'created_by','status'], 'integer'],
            [['label'], 'string', 'max' => 100],
            [['remark'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => '标签',
            'created_at' => '添加时间',
            'created_by' => '添加人',
            'status' => '状态',
            'remark' => '说明',
        ];
    }

    /**
     * 关联管理员
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getManager()
    {
        return $this->hasOne(\common\models\sys\Manager::class, ['id' => 'created_by']);
    }
}
