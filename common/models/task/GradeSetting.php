<?php

namespace common\models\task;

use Yii;

/**
 * This is the model class for table "task_grade_setting".
 *
 * @property int $id
 * @property int $title 等级
 * @property int $experience 经验值
 * @property int $created_at 添加时间
 * @property int $updated_at 更新时间
 * @property int $created_by 更新人
 * @property int $updated_by 修改人
 */
class GradeSetting extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            \yii\behaviors\BlameableBehavior::className(),
            \yii\behaviors\TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_grade_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'experience'], 'required'],
            [['title', 'experience', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '等级',
            'experience' => '经验值',
            'created_at' => '添加时间',
            'updated_at' => '更新时间',
            'created_by' => '更新人',
            'updated_by' => '修改人',
        ];
    }

    /**
     * 关联管理员
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getManager()
    {
        return $this->hasOne(\common\models\sys\Manager::class, ['id' => 'created_by']);
    }

    /**
     * 根据会员经验值,获取当前用户等级
     * @param $experience
     * @return mixed|string
     */
    public static function getGradeByExperience($experience)
    {
        $model = self::find()
            ->select(['title'])
            ->where(['<', 'experience', $experience])
            ->orderBy(['title' => SORT_DESC])
            ->asArray()
            ->one();
        if (!empty($model)) {
            return $model['title'];
        } else {
            return "0";
        }
    }
}
