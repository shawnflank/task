<?php

namespace common\models\task;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "task_task_category".
 *
 * @property int $id
 * @property string $title 分类名
 * @property int $created_at 添加时间
 * @property int $updated_at 修改时间
 * @property int $created_by 添加人
 * @property int $status 状态
 * @property int $sort 排序
 */
class TaskCategory extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_task_category';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ],
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'sort'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'status', 'sort'], 'integer'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '分类名',
            'created_at' => '添加时间',
            'updated_at' => '修改时间',
            'created_by' => '添加人',
            'status' => '状态',
            'sort' => '排序',
        ];
    }

    /**
     * 关联管理员
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getManager()
    {
        return $this->hasOne(\common\models\sys\Manager::class, ['id' => 'created_by']);
    }

    /**
     * 获取所有类型
     * @return array|ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getAllCategory()
    {
        return self::find()
            ->select(['id', 'title'])
            ->where(['status' => 1])
            ->orderBy([
                'sort' => SORT_ASC,
                'created_at' => SORT_DESC,
            ])
            ->asArray()
            ->all();
    }
}
