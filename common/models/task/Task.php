<?php

namespace common\models\task;

use common\enums\StatusEnum;
use common\models\member\Member;
use Yii;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "task_task".
 *
 * @property int $id
 * @property int $c_id 任务分类ID
 * @property string $title 任务名称
 * @property string $commission 任务佣金
 * @property int $task_number 任务数量（0，无限制）
 * @property int $consume_integral 消费积分（0，无限制）
 * @property string $deposit_money 押金金额（0时无押金）
 * @property array $receive_time 领取时间段，为空是则无时间限制
 * @property int $receive_to_submit_time 领取到提交时间间隔（0，无限制）秒
 * @property int $over_to_recevice_time 完成到再次领取时间间隔（0，无限制）秒
 * @property int $deposit_return 过期/驳回 押金是否退还0，退还，1不退还
 * @property int $reject_submit_switch 驳回后是否允许提交次数
 * @property string $remark 注意事项
 * @property string $release_title 发布内容
 * @property string $release_body 发布主体
 * @property string $release_comment 发布评论
 * @property string $receive_Intervals 该任务领取时间周期
 * @property int $created_at 添加时间
 * @property int $updated_at 修改时间
 * @property int $created_by 创建人
 * @property int $status 状态
 * @property int $upload_img_number 提交任务上传图片张数
 * @property int $l_id 标签
 * @property int $sort 排序
 * @property int $is_repeat_receive 是否可以重复领取
 * @property string $receive_hint 领取任务提示语
 * @property string $banner 任务封面图
 * @property string $wechat_number 审核微信
 * @property int $experience 经验
 * @property int $grade 等级
 * @property int $is_open 是否能所有用户可见
 */
class Task extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ],
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['c_id', 'title', 'commission', 'remark', 'release_body', 'task_number', 'deposit_money', 'receive_to_submit_time', 'over_to_recevice_time', 'reject_submit_switch', 'upload_img_number', 'l_id', 'receive_hint', 'sort', 'is_repeat_receive', 'grade', 'experience', 'is_open'], 'required'],
            [['c_id', 'task_number', 'consume_integral','receive_to_submit_time', 'over_to_recevice_time', 'deposit_return', 'reject_submit_switch', 'created_at', 'updated_at', 'created_by', 'status', 'upload_img_number', 'l_id', 'sort', 'grade', 'experience', 'is_open', 'receive_Intervals'], 'integer'],
            [['receive_time', 'release_comment'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['wechat_number'], 'string', 'max' => 50],
            [['remark', 'release_title', 'receive_hint'], 'string', 'max' => 255],
            [['commission', 'deposit_money'], 'number', 'numberPattern' => '/^\d+(.\d{1,2})?$/', 'message' => '小数点后位数不能大于俩位'],
            [['banner'], 'file', 'extensions' => 'png,jpg,jpeg,gif', 'mimeTypes' => 'image/jpeg, image/png, image/gif', 'maxSize' => 1024 * 1024 * 10, 'maxFiles' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'c_id' => '任务分类',
            'title' => '任务名称',
            'banner' => '任务封面图(1920x1024)',
            'commission' => '任务获得佣金',
            'integral' => '任务获得积分',
            'deposit_money' => '押金金额',
            'consume_integral' => '消费积分',
            'task_number' => '任务数量',
            'receive_time' => '领取时间段',
            'receive_to_submit_time' => '领取到提交时间间隔(分)',
            'over_to_recevice_time' => '完成此任务到领取下一个任务时间间隔(分)',
            'deposit_return' => '驳回任务押金是否退还',
            'reject_submit_switch' => '允许任务提交次数',
            'remark' => '注意事项',
            'release_title' => '发布内容',
            'release_body' => '发布主体',
            'release_comment' => '发布评论',
            'created_at' => '添加时间',
            'updated_at' => '修改时间',
            'created_by' => '创建人',
            'status' => '状态',
            'upload_img_number' => '提交任务上传图片张数',
            'l_id' => '标签',
            'receive_hint' => '领取任务提示语',
            'sort' => '排序',
            'wechat_number' => '审核微信',
            'is_repeat_receive' => '领取任务次数',
            'grade' => "会员达到对应等级可见",
            'experience' => "任务获得经验值",
            'is_open' => "是否对所有会员可见",
            'receive_Intervals' => "该任务领取时间周期",
        ];
    }

    /**
     * 关联管理员
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getManager()
    {
        return $this->hasOne(\common\models\sys\Manager::class, ['id' => 'created_by']);
    }

    /**
     * 关联分类
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getCategory()
    {
        return $this->hasOne(TaskCategory::class, ['id' => 'c_id']);
    }

    /**
     * 关联标签
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getLabel()
    {
        return $this->hasOne(TaskLabel::class, ['id' => 'l_id']);
    }


    /**
     * 根据类型获取任务列表
     * @param $c_id
     * @param $page
     * @param $level_id
     * @return array|ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getLists($c_id, $page, $level_id)
    {
        $pagessize = Yii::$app->params['pagessize']['task_lists'];
        $page = !empty($page) ? $page : 1;
        $pages = ($page - 1) * $pagessize;
        $inner = "(SELECT `id` FROM " . self::tableName() . " b WHERE (`status` = " . StatusEnum::ENABLED . " AND `c_id` = $c_id" . " AND((`is_open` = 1) OR (`grade` = $level_id))) ORDER BY `sort` DESC , `grade` DESC , `created_at` DESC  LIMIT " . $pages . " , " . "$pagessize" . ") b using (`id`)";
        $model = self::find()
            ->select(['id', 'c_id', 'l_id', 'title', 'banner', 'commission', 'task_number', 'remark', 'deposit_money'])
            ->innerJoin($inner)
            ->with(['category' => function ($query) {
                $query->select(['id', 'title']);
            }])
            ->with(['label' => function ($query) {
                $query->select(['id', 'label', 'remark']);
            }])
            ->asArray()
            ->all();
        $redis = Yii::$app->redis;
        foreach ($model as $k1 => $v1) {
            $task_key = 'task_number_' . $v1['id'];
            $model[$k1]['remaining_amount'] = $redis->get($task_key) ?? '0';
        }
        return $model;
    }

    /**
     * 根据id查看详情
     * @param $id
     * @param $member_id
     * @return array|null|ActiveRecord
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getModelById($id, $member_id)
    {
        $model = self::find()
            ->where(['id' => $id])
            ->with(['category' => function ($query) {
                $query->select(['id', 'title']);
            }])
            ->with(['label' => function ($query) {
                $query->select(['id', 'label', 'remark']);
            }])
            ->asArray()
            ->one();
        // 判断该任务是否有统一审核微信号
        if (empty($model['wechat_number'])) {
            // 若没有
            $member = Member::find()
                ->where(['id' => $member_id])
                ->select(['id', 'w_id'])
                ->with(['wechatNumber' => function ($query) {
                    $query->select(['id', 'wechat_number']);
                }])
                ->asArray()
                ->one();
            if (!empty($member['wechatNumber']['wechat_number'])) {
                $model['wechat_number'] = "官方审核微信：" . $member['wechatNumber']['wechat_number'];
            } else {
                $model['wechat_number'] = "";
            }
        }
        //处理可接任务时间
        if (empty($model['receive_time'])) {
            $model['whether_to_receive_the_time_again'] = 1;
        } else {
            $receive_time_deal = explode('~', $model['receive_time']);
            $model['whether_to_receive_the_time_again'] = self::checkIsBetweenTime($receive_time_deal[0], $receive_time_deal[1]);
        }

        $redis = Yii::$app->redis;
        $task_key = 'task_number_' . $id;
        $model['remaining_amount'] = $redis->get($task_key) ?? '0';

        $model['user_task_info'] = null;
        $user_is_task_Doing = TaskBill::find()->where(['t_id' => $id, 'member_id' => $member_id])->orderBy('id DESC')->one();
        if ($user_is_task_Doing) {
            $model['user_task_info'] = $user_is_task_Doing;
        }
        if (!empty($model)) {
            if ($model['c_id'] == 1 || $model['c_id'] == 4) {// 1，4为图文任务
                $model['release_body'] = unserialize($model['release_body']);
            }
        }
        // 还可领取任务次数
        if ($model['is_repeat_receive'] > 0) {
            $model['is_repeat_receive'] -= TaskBill::getThisTaskBillCount($member_id, $id);
            if ($model['is_repeat_receive'] == 0) {
                $model['is_repeat_receive'] = "已领完";
            } else {
                $model['is_repeat_receive'] = $model['is_repeat_receive'] . "次";
            }
        } else {
            $model['is_repeat_receive'] = "无限制";
        }
        return $model;
    }


    /**当前时间是否在该时间段中
     * @param $start
     * @param $end
     * @return int
     */
    public static function checkIsBetweenTime($start, $end)
    {
        $curTime = time();//当前时分
        $assignTime1 = strtotime($start);//获得指定分钟时间戳，00:00
        $assignTime2 = strtotime($end);//获得指定分钟时间戳，01:00
        $result = 0;
        if ($curTime > $assignTime1 && $curTime < $assignTime2) {
            $result = 1;
        }
        return $result;
    }


    /**
     * 最新任务列表
     * @param $level_id
     * @param $page
     * @return array|ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getNewLists($level_id, $page)
    {
        $pages_size = Yii::$app->params['pagessize']['task_lists'];
        $pages = ($page - 1) * $pages_size;
        $model = self::find()
            ->select(['id', 'c_id', 'l_id', 'title', 'banner', 'commission', 'task_number', 'remark'])
            ->where(['>', 'task_number', 0])
            ->andWhere([
                'OR', ['is_open' => 1], ['grade' => $level_id]
            ])
            ->andWhere(['status' => StatusEnum::ENABLED])
            ->with(['category' => function ($query) {
                $query->select(['id', 'title']);
            }])
            ->orderBy(['sort' => SORT_DESC, 'grade' => SORT_DESC, 'created_at' => SORT_DESC])
            ->offset($pages)
            ->limit($pages_size)
            ->asArray()
            ->all();
        $redis = Yii::$app->redis;
        foreach ($model as $k1 => $v1) {
            $task_key = 'task_number_' . $v1['id'];
            $model[$k1]['remaining_amount'] = $redis->get($task_key) ?? '0';
        }
        return $model;
    }
}
