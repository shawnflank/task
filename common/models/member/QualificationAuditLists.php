<?php

namespace common\models\member;

use common\models\common\ReviewsWechat;
use common\models\sys\Manager;
use Yii;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "task_qualification_audit_lists".
 *
 * @property int $id
 * @property int $member_id
 * @property string $verification_img_data 用户提交资质资料
 * @property string $verification_text_data 用户提交文本资料
 * @property int $status 状态0未审核,1审核通过,2审核失败
 * @property int $created_at 提交时间
 * @property int $updated_at 审核时间
 * @property int $updated_by 审核人
 * @property int $w_id 绑定微信
 */
class QualificationAuditLists extends \yii\db\ActiveRecord
{
    public $notice;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_qualification_audit_lists';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ],

        ];
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->member_id = Yii::$app->user->identity['member_id'];
        }
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'status', 'created_at', 'updated_at', 'updated_by','w_id'], 'integer'],
            [['verification_img_data','notice'], 'string'],
            [['verification_text_data'], 'string', 'max' => 255],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '会员',
            'verification_img_data' => '资质资料',
            'verification_text_data' => '资质账号',
            'status' => '状态',
            'notice' => '会员备注',
            'created_at' => '提交时间',
            'updated_at' => '审核时间',
            'updated_by' => '审核人',
            'w_id' => '审核信息',
        ];
    }

    public static $statusArray = [
        0=>'未审核',
        1=>'已通过',
        2=>'已拒绝'
    ];

    public static $qualification_status_color=  [0 => "info", 1 => "success", 2 => "warning"];


    /**
     * 关联会员
     */
    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id']);
    }


    /**
     * 关联管理员
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getManager()
    {
        return $this->hasOne(Manager::class, ['id' => 'updated_by']);
    }

    /**
     * 关联微信号
     * @return \yii\db\ActiveQuery
     * @author 哈哈
     */
    public function getWechatNumber()
    {
        return $this->hasOne(ReviewsWechat::class, ['id' => 'w_id']);
    }
}
