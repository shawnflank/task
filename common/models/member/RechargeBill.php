<?php

namespace common\models\member;

use common\helpers\CommonPluginHelper;
use Yii;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "task_recharge_bill".
 *
 * @property int $id
 * @property int $member_id 会员ID
 * @property string $sn 订单号
 * @property string $recharge_money 充值金额
 * @property string $username 充值人姓名
 * @property int $type 充值类型(1支付宝扫码,2微信扫码,3支付宝H5,4微信H5)
 * @property int $created_at 充值时间
 * @property int $updated_at 审核时间
 * @property int $status 0未支付,1未审核,2通过,3拒绝
 */
class RechargeBill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public $recharge_type = [0, 1, 2, 3, 4];
    public static $recharge_type_array = [0 => "线下充值", 1 => "支付宝扫码", 2 => "微信扫码", 3 => "支付宝H5", 4 => "微信H5"];
    public static $recharge_status_array = [0 => "未审核", 1 => "通过", 2 => "拒绝", 3 => "未支付"];


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_recharge_bill';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['recharge_money', 'username', 'type'], 'required'],
            [['member_id', 'type', 'created_at', 'updated_at', 'status'], 'integer'],
            [['recharge_money'], 'number', 'numberPattern' => '/^\d+(.\d{1,2})?$/', 'message' => '小数点后位数不能大于俩位'],
            [['sn'], 'string', 'max' => 50],
            [['username'], 'string', 'max' => 100],
            [['type'], 'in', 'range' => $this->recharge_type],
            [['recharge_money'], 'verifyRechargeMoney'],
        ];
    }

    /**
     * 验证提现金额
     * @param $attribute
     * @param $params
     */
    public function verifyRechargeMoney($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $minimum_recharge_amount = intval(Yii::$app->debris->config('minimum_recharge_amount'));
            if ($this->recharge_money < $minimum_recharge_amount) {
                return $this->addError($attribute, '充值金额不能低于' . $minimum_recharge_amount . '元！');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '会员',
            'sn' => '订单号',
            'recharge_money' => '充值金额',
            'username' => '充值人姓名',
            'type' => '类型',
            'created_at' => '充值时间',
            'updated_at' => '审核时间',
            'status' => '状态',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->member_id = Yii::$app->user->identity['member_id'];
            $this->sn = CommonPluginHelper::getSn($this->member_id);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id']);
    }
}
