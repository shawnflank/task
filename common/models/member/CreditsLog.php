<?php

namespace common\models\member;

use common\enums\StatusEnum;
use common\models\sys\Manager;
use Yii;
use common\behaviors\MerchantBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%member_credits_log}}".
 *
 * @property int $id
 * @property string $merchant_id 商户id
 * @property string $member_id 用户id
 * @property int $pay_type 支付类型
 * @property string $credit_type 变动类型[integral:积分;money:余额]
 * @property string $credit_group 变动的组别
 * @property string $credit_group_detail 变动的详细组别
 * @property double $old_num 之前的数据
 * @property double $new_num 变动后的数据
 * @property double $num 变动的数据
 * @property string $remark 备注
 * @property string $ip ip地址
 * @property string $map_id 关联id
 * @property int $status 状态[-1:删除;0:禁用;1启用]
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 * @property int $type 类型
 */
class CreditsLog extends \common\models\base\BaseModel
{
    use MerchantBehavior;

    const CREDIT_TYPE_USER_MONEY = 'user_money';
    const CREDIT_TYPE_USER_INTEGRAL = 'user_integral';


    // 完成任务1
    const OVER_TASK_TYPE = 1;
    // 充值2
    const RECHARGE_TYPE = 2;
    // 提现3
    const WITHDRAW_TYPE = 3;
    // 签到4
    const SIGN_TYPE = 4;
    // 抽奖5
    const LOTTERY_TYPE = 5;
    // 注册
    const REGISTER_TYPE = 6;
    // 押金
    const DEPOSIT_TYPE = 7;
    // 推荐佣金
    const RECOMMEND_COMMISSION = 8;
    // 类别数组
    public static $TypeArray = [1, 2, 3, 4, 5, 6, 7, 8];

    /**
     * 类别
     */
    public static $TypeExplain = [
        self::OVER_TASK_TYPE => '完成任务',
        self::RECHARGE_TYPE => '充值',
        self::WITHDRAW_TYPE => '提现',
        self::SIGN_TYPE => '签到',
        self::LOTTERY_TYPE => '抽奖',
        self::REGISTER_TYPE => '注册',
        self::DEPOSIT_TYPE => '押金',
        self::RECOMMEND_COMMISSION => '推荐佣金',
    ];


    /**
     * 变动类型
     *
     * @var array
     */
    public static $creditTypeExplain = [
        self::CREDIT_TYPE_USER_MONEY => '余额日志',
        self::CREDIT_TYPE_USER_INTEGRAL => '积分日志',
    ];


    const CREDIT_GROUP_MANAGER = 'manager';
    const CREDIT_GROUP_MEMBER = 'member';


    /**
     * 变动组别
     *
     * @var array
     */
    public static $creditGroupExplain = [
        self::CREDIT_GROUP_MANAGER => '管理员',
        self::CREDIT_GROUP_MEMBER => '会员',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%member_credits_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pay_type', 'merchant_id', 'member_id', 'map_id', 'status', 'created_at', 'updated_at', 'type'], 'integer'],
            [['old_num', 'new_num', 'num'], 'number'],
            [['credit_type', 'credit_group', 'credit_group_detail', 'ip'], 'string', 'max' => 30],
            [['remark'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'merchant_id' => '商户',
            'member_id' => '用户',
            'pay_type' => '支付类型',
            'map_id' => '关联id',
            'ip' => 'ip地址',
            'credit_type' => '变动类型',
            'credit_group' => '操作类型',
            'credit_group_detail' => '操作人',
            'old_num' => '变更之前',
            'new_num' => '变更后',
            'num' => '变更数量',
            'remark' => '备注',
            'status' => '状态',
            'created_at' => '创建时间',
            'updated_at' => '修改时间',
            'type' => '类型',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id']);
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->ip = Yii::$app->request->userIP;
        }

        return parent::beforeSave($insert);
    }

    /**
     * 获取当天签到次数
     * @param $member_id
     * @return int|string
     */
    public static function getTodaySignCountByMemberId($member_id)
    {
        // 首先拿到当天的时间戳
        $today_first = strtotime(date('Y-m-d', time()));
        $today_last = $today_first + 86399;
        return self::find()
            ->where(['member_id' => $member_id, 'type' => self::SIGN_TYPE])
            ->andWhere(['between', 'created_at', $today_first, $today_last])
            ->count();
    }

    /**
     * 查看列表
     * @param $member_id
     * @param $page
     * @param $type
     * @return array|ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     * @param $select_type
     */
    public static function getLists($member_id, $page, $type, $select_type)
    {
        $pagessize = Yii::$app->params['pagessize']['credits_log_lists'];
        $page = !empty($page) ? $page : 1;
        $pages = ($page - 1) * $pagessize;
        if (!empty($type)) {
            $type_sql = " AND `type` = $type";
        } else {
            $type_sql = "";
        }
        if (!empty($select_type)) {
            $selevt_type_sql = ' AND `credit_type` = ' . '"' . $select_type . '"';
        } else {
            $selevt_type_sql = "";
        }
        $inner = "(SELECT `id` FROM " . self::tableName() . " b WHERE (`member_id` = $member_id" . $type_sql . $selevt_type_sql . " AND `status` = " . StatusEnum::ENABLED . ") ORDER BY `id` DESC LIMIT " . $pages . " , " . "$pagessize" . ") b using (`id`)";

        return self::find()
            ->select(['id', 'credit_type', 'num', 'remark', 'FROM_UNIXTIME(`created_at`,\'%Y-%m-%d %H:%i:%s\') as created_at', 'type'])
            ->innerJoin($inner)
            ->asArray()
            ->all();
    }

    /**
     * 获取当天收入记录
     * @param $member_id
     * @param $page
     * @param $type
     * @return array|CreditsLog[]|ActiveRecord[]
     */
    public static function DateGetTodayList($member_id, $page, $type)
    {
        $pagessize = Yii::$app->params['pagessize']['credits_log_lists'];
        $page = !empty($page) ? $page : 1;
        $pages = ($page - 1) * $pagessize;
        if (!empty($type)) {
            $type_sql = " AND `type` = $type";
        } else {
            $type_sql = "";
        }
        //当天开始时间
        $todayStartTime = strtotime(date('Y-m-d', time()));
        $todayEndTime = strtotime(date('Y-m-d', time())) + 86399;
        $date_sql = ' AND created_at BETWEEN ' . $todayStartTime . ' AND ' . $todayEndTime;
        $inner = "(SELECT `id` FROM " . self::tableName() . " b WHERE (`member_id` = $member_id" . $type_sql . " AND `status` = " . StatusEnum::ENABLED . "  $date_sql  AND `credit_type` = 'user_money' ) ORDER BY `id` DESC LIMIT " . $pages . " , " . "$pagessize" . ") b using (`id`)";
        return self::find()
            ->select(['id', 'credit_type', 'num', 'remark', 'FROM_UNIXTIME(`created_at`,\'%Y-%m-%d %H:%i:%s\') as created_at', 'type'])
            ->innerJoin($inner)
            ->asArray()
            ->all();
    }
}
