<?php

namespace common\models\member;

use Yii;

/**
 * This is the model class for table "mi_user".
 *
 * @property int $id
 * @property string $username 账号
 * @property string $password_hash hash密码
 * @property string $cr_time 创建时间
 * @property string $access_token token
 * @property int $type 账号类型，10会员，20管理员，30初始管理员
 * @property string $avatar 头像
 * @property string $login_time 上次登录时间
 * @property string $ip 登录ip
 * @property int $wallet 钱包
 * @property string $nickname 昵称
 * @property int $complete_count 完成任务次数
 * @property string $receipt_qrcode 收款二维码
 * @property int $get_mid 当前分配任务ID
 * @property string $bank_name 银行名称
 * @property string $bank_number 银行卡号
 * @property string $bank_mobile 银行预留手机
 * @property string $bank_user_name 真实姓名
 * @property int $status 1未认证，2待认证，3已认证，4认证未通过
 * @property string $wechat 微信号
 * @property int $p_id 上级id
 * @property string $referrer_code 推荐码
 * @property string $notice 备注
 * @property int $official_wechat_number  绑定官方微信
 */
class MiUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mi_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'wallet', 'complete_count', 'get_mid', 'status', 'p_id', 'official_wechat_number'], 'integer'],
            [['username', 'nickname'], 'string', 'max' => 50],
            [['password_hash', 'avatar', 'ip', 'receipt_qrcode', 'wechat'], 'string', 'max' => 255],
            [['cr_time', 'login_time'], 'string', 'max' => 10],
            [['access_token'], 'string', 'max' => 32],
            [['bank_name'], 'string', 'max' => 40],
            [['bank_number', 'notice'], 'string', 'max' => 30],
            [['bank_mobile'], 'string', 'max' => 15],
            [['bank_user_name'], 'string', 'max' => 20],
            [['referrer_code'], 'string', 'max' => 8],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => '账号',
            'password_hash' => 'hash密码',
            'cr_time' => '创建时间',
            'access_token' => 'token',
            'type' => '账号类型，10会员，20管理员，30初始管理员',
            'avatar' => '头像',
            'login_time' => '上次登录时间',
            'ip' => '登录ip',
            'wallet' => '钱包',
            'nickname' => '昵称',
            'complete_count' => '完成任务次数',
            'receipt_qrcode' => '收款二维码',
            'get_mid' => '当前分配任务ID',
            'bank_name' => '银行名称',
            'bank_number' => '银行卡号',
            'bank_mobile' => '银行预留手机',
            'bank_user_name' => '真实姓名',
            'status' => '1未认证，2待认证，3已认证，4认证未通过',
            'wechat' => '微信号',
            'p_id' => '上级id',
            'referrer_code' => '推荐码',
            'notice' => '备注',
            'official_wechat_number' => ' 绑定官方微信',
        ];
    }
}
