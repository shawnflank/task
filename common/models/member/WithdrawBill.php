<?php

namespace common\models\member;

use backend\modules\member\forms\RechargeForm;
use common\enums\AppEnum;
use common\helpers\CommonPluginHelper;
use common\helpers\DateHelper;
use Yii;

use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "task_withdraw_bill".
 *
 * @property int $id
 * @property int $member_id 会员ID
 * @property string $sn 订单号
 * @property string $withdraw_money 提现金额
 * @property string $type 提现类型(wechat_account_url,转账微信收款码,alipay_account,转账支付宝,wechat_account转账微信红包)
 * @property int $created_at 提现时间
 * @property int $updated_at 审核时间
 * @property int $status 0未审核,1通过,2拒绝,3取消
 * @property int $updated_by 审核人
 */
class WithdrawBill extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_withdraw_bill';
    }

    private $_user;

    public static $withdraw_type = ['wechat_account', 'wechat_account_url', 'alipay_account', 'alipay_account_url'];
    public static $withdraw_type_array = ['wechat_account_url' => "微信收款码", 'alipay_account' => "转账支付宝", 'wechat_account' => "微信自动提现", "alipay_account_url" => "支付宝收款码"];
    public static $withdraw_status_array = [0 => "未审核", 1 => "通过", 2 => "拒绝", 3 => "取消",4 =>"失败"];
    public static $withdraw_status_color = [0 => "info", 1 => "success", 2 => "primary", 3 => "warning",4 => "warning"];


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ],

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['withdraw_money', 'type'], 'required'],
            [['member_id', 'created_at', 'updated_at', 'status', 'updated_by'], 'integer'],
            [['withdraw_money'], 'number', 'numberPattern' => '/^\d+(.\d{1,2})?$/', 'message' => '小数点后位数不能大于俩位'],
            [['sn'], 'string', 'max' => 50],
            [['type'], 'string', 'max' => 30],
            [['type'], 'in', 'range' => self::$withdraw_type],
            [['withdraw_money'], 'in', 'range' => explode('/', Yii::$app->debris->config('can_withdraw_amount')), 'message' => '提现金额错误！'],
            [['withdraw_money'], 'verifyWithdrawMoney'],
            [['type'], 'verifyType'],
        ];
    }

    /**
     * 验证提现类型
     * @param $attribute
     * @param $params
     */
    public function verifyType($attribute, $params)
    {
        if (!$this->hasErrors()) {
            // 判断提现类型
            $withdraw_method = unserialize(Yii::$app->debris->config('withdraw_method'));
            if (!in_array($this->type, $withdraw_method)) {
                return $this->addError($attribute, '该提现方式已暂停使用，请选择其他的提现方式！');
            }
          
            if (empty(BindingAccount::find()->where(['member_id' => Yii::$app->user->identity['member_id']])->andWhere(['<>', $this->type, ''])->exists())) {
                return $this->addError($attribute, '您所提交的提现类型暂未绑定,请绑定后再试！');
            }
        }
    }

    /**
     * 验证提现金额
     * @param $attribute
     * @param $params
     */
    public function verifyWithdrawMoney($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) { // 判断是否有该用户
                return $this->addError($attribute, '用户信息错误,请稍后再试！');
            } elseif ($this->withdraw_money > $user->account->user_money) {
                return $this->addError($attribute, '余额不足！');
            }
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\db\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
     
        if ($insert) {
            $remark = "提交提现申请，当前余额减少" . $this->withdraw_money . "元";
            $type = RechargeForm::TYPE_MONEY;
            $rechargeForm = new RechargeForm([
                'type' => $type,
                'money' => $this->withdraw_money,
                'int' => 0,
                'remark' => $remark,
                'change' => RechargeForm::CHANGE_DECR
            ]);
            $rechargeForm->save($this->getUser(), CreditsLog::WITHDRAW_TYPE, AppEnum::API, CreditsLog::CREDIT_GROUP_MEMBER);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return Member|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Member::findOne(Yii::$app->user->identity['member_id']);
        }
        return $this->_user;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->member_id = Yii::$app->user->identity['member_id'];
            $this->sn = CommonPluginHelper::getSn($this->member_id);
        }

        return parent::beforeSave($insert);
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '会员',
            'sn' => '订单号',
            'withdraw_money' => '提现金额',
            'type' => '类型',
            'created_at' => '提现时间',
            'updated_at' => '审核时间',
            'status' => '状态',
            'updated_by' => '审核人',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id']);
    }

    /**
     * 关联绑定账号
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getBindingAccount()
    {
        return $this->hasOne(BindingAccount::class, ['member_id' => 'member_id']);
    }

    /**
     * 关联账户信息
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getAccount()
    {
        return $this->hasOne(Account::class, ['member_id' => 'member_id']);
    }

    /**
     * 根据member_id获取该用户所有订单
     * @param $member_id
     * @param $page
     * @return array|ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getWithDrawBillByMemberId($member_id, $page)
    {
        $pages_size = Yii::$app->params['pagessize']['withdraw_lists'];
        $page = !empty($page) ? $page : 1;
        $pages = ($page - 1) * $pages_size;
        $inner = "(SELECT `id` FROM " . self::tableName() . " b WHERE (`member_id` = $member_id) ORDER BY `id` DESC LIMIT " . $pages . " , " . "$pages_size" . ") b using (`id`)";
        return self::find()
            ->select(['sn', 'withdraw_money', 'type', 'FROM_UNIXTIME(`created_at`,\'%Y-%m-%d %H:%i:%s\') as created_at', 'status'])
            ->innerJoin($inner)
            ->asArray()
            ->all();
    }


    /**
     * 获取今日已提现次数
     * @param $member_id
     * @return int|string
     * @author 哈哈
     */
    public static function getTodaySubmitWithdrawTimes($member_id)
    {
        $today = DateHelper::today();
        return self::find()
            ->where(['member_id' => $member_id])
            ->andWhere(['between', 'created_at', $today['start'], $today['end']])
            ->select(['id'])
            ->count();
    }


    /**
     * 获取已提现总次数
     * @param $member_id
     * @return int|string
     * @author 哈哈
     */
    public static function getTotalSubmitWithdrawTimes($member_id)
    {
        return self::find()
            ->where(['member_id' => $member_id])
            ->select(['id'])
            ->count();
    }
}
