<?php

namespace common\models\member;

use Yii;

/**
 * This is the model class for table "task_binding_account".
 *
 * @property int $id
 * @property int $member_id 用户ID
 * @property string $alipay_account 支付宝账号
 * @property string $alipay_account_url 支付宝收款码
 * @property string $wechat_account 微信账号
 * @property string $safety_password 安全密码
 * @property string $alipay_user_name 支付宝账户名
 * @property string $safety_password_hash 安全密码
 * @property string $wechat_account_url 微信收款二维码
 */
class BindingAccount extends \yii\db\ActiveRecord
{
    public $safety_password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_binding_account';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id'], 'integer'],
            [['safety_password'], 'string', 'min' => 6, 'max' => 6],
            [['alipay_account', 'alipay_user_name', 'wechat_account'], 'string', 'max' => 50],
            [['safety_password_hash', 'wechat_account_url','alipay_account_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '用户ID',
            'alipay_account' => '支付宝账号',
            'wechat_account' => '微信账号',
            'safety_password' => '安全密码',
            'safety_password_hash' => '安全密码',
            'alipay_user_name' => '支付宝账户名',
            'wechat_account_url' => '微信收款码',
            'alipay_account_url' => '支付宝收款码',
        ];
    }

    /**
     * 验证旧安全密码
     * @param $new_safety_password
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function validateSafetyPassword($new_safety_password)
    {
        return Yii::$app->security->validatePassword($new_safety_password, $this->safety_password_hash);
    }

    /**
     * 设置安全密码
     * @param $safety_password
     * @throws \yii\base\Exception
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function setPassword($safety_password)
    {
        $this->safety_password_hash = Yii::$app->security->generatePasswordHash($safety_password);
    }

    public function getMember()
    {
        return $this->hasOne(Member::class, ['id' => 'member_id']);
    }
}
