<?php

namespace common\models\member;

use backend\modules\member\forms\RechargeForm;
use common\enums\AppEnum;
use common\helpers\DateHelper;
use common\helpers\InviteCodeHelper;
use common\models\common\ReviewsWechat;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use common\enums\StatusEnum;
use common\models\base\User;
use common\helpers\RegularHelper;

/**
 * This is the model class for table "{{%member}}".
 *
 * @property int $id 主键
 * @property string $merchant_id 商户id
 * @property string $username 帐号
 * @property string $password_hash 密码
 * @property string $auth_key 授权令牌
 * @property string $password_reset_token 密码重置令牌
 * @property int $type 类别[1:普通会员;10管理员]
 * @property string $nickname 昵称
 * @property string $realname 真实姓名
 * @property string $head_portrait 头像
 * @property int $gender 性别[0:未知;1:男;2:女]
 * @property string $invitation_code 邀请码
 * @property int $role 权限
 * @property int $last_time 最后一次登录时间
 * @property string $last_ip 最后一次登录ip
 * @property string $register_ip 注册ip
 * @property int $last_device 最后一次登录设备，1、web，2、ios，3、android
 * @property string $pid 上级id
 * @property int $status 状态[-1:删除;0:禁用;1启用]
 * @property string $created_at 创建时间
 * @property string $updated_at 修改时间
 * @property string $notice 备注
 * @property string $phone_identifier 手机唯一标识符
 * @property int $level_id 等级id
 * @property int $sign_days 连续签到天数
 * @property int $created_by 创建人id，0为自己注册
 * @property int $visit_count 登录次数
 * @property int $recommend_id 推荐人ID
 * @property int $today_sign_count 签到
 * @property int $experience 经验值
 * @property int $w_id 审核微信号
 * @property int $recommend_number 推荐人数
 */
class Member extends User
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%member}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password_hash'], 'required', 'on' => ['backendCreate']],
            [['password_hash'], 'string', 'min' => 6, 'on' => ['backendCreate']],
            [['username'], 'unique', 'on' => ['backendCreate']],
            [['merchant_id', 'type', 'gender', 'visit_count', 'role', 'last_time', 'level_id', 'sign_days', 'status', 'created_at', 'updated_at', 'last_device', 'recommend_id', 'today_sign_count', 'experience', 'recommend_number'], 'integer'],
            [['username'], 'string', 'max' => 20],
            [['password_hash', 'password_reset_token', 'head_portrait'], 'string', 'max' => 150],
            [['phone_identifier'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['notice'], 'string', 'max' => 50],
            [['nickname', 'realname', 'invitation_code'], 'string', 'max' => 50],
            [['last_ip', 'register_ip'], 'string', 'max' => 16],
            ['username', 'match', 'pattern' => RegularHelper::mobile(), 'message' => '不是一个有效的手机号码'],
            [['invitation_code'], 'unique', 'on' => 'backendIndex'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => '账号',
            'password_hash' => '密码',
            'auth_key' => '授权登录key',
            'password_reset_token' => '密码重置token',
            'type' => '类型',
            'nickname' => '昵称',
            'head_portrait' => '头像',
            'gender' => '性别',
            'visit_count' => '登录总次数',
            'role' => '权限',
            'last_time' => '最后一次登录时间',
            'last_ip' => '最后一次登录ip',
            'register_ip' => '注册ip',
            'status' => '状态',
            'created_at' => '创建时间',
            'updated_at' => '修改时间',
            'level_id' => '等级',
            'realname' => '真实姓名',
            'recommend_id' => '推荐人',
            'today_sign_count' => '签到', // 0未签到 1 已签到
            'notice' => '备注',
            'experience' => '经验值',
            'invitation_code' => '邀请码',
            'w_id' => '审核微信号',
            'phone_identifier' => '手机标识符',
            'recommend_number' => '推荐人数',
        ];
    }

    /**
     * 场景
     *
     * @return array
     */
    public function scenarios()
    {
        return [
            'backendCreate' => ['username', 'password_hash'],
            'default' => array_keys($this->attributeLabels()),
            'backendIndex' => ['invitation_code', 'notice', 'username', 'nickname', 'w_id']
        ];
    }

    /**
     * 关联账户
     */
    public function getAccount()
    {
        return $this->hasOne(Account::class, ['member_id' => 'id']);
    }

    /**
     * 关联上级推荐人
     */
    public function getRecommendMember()
    {
        return $this->hasOne(Member::class, ['id' => 'recommend_id']);
    }

    /**
     * 关联下级注册人
     */
    public function getRegisterMember()
    {
        return $this->hasMany(Member::class, ['recommend_id' => 'id']);
    }

    /**
     * 关联绑定账号
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getBindingAccount()
    {
        return $this->hasOne(BindingAccount::class, ['member_id' => 'id']);
    }

    /**
     * 关联当天完成任务所获取的金额
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getTodayAccount()
    {
        // 首先拿到当天的时间戳
        $today = DateHelper::today();
        return $this->hasMany(CreditsLog::class, ['member_id' => 'id'])
            ->where(['between', 'created_at', $today['start'], $today['end']])
            ->andWhere(['type' => CreditsLog::OVER_TASK_TYPE, 'credit_type' => 'user_money']);
    }

    /**
     * 关联昨天的签到记录
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getYesterdaySign()
    {
        // 首先拿到昨天的时间戳
        $yesterday = DateHelper::yesterday();
        return $this->hasOne(CreditsLog::class, ['member_id' => 'id'])
            ->where(['between', 'created_at', $yesterday['start'], $yesterday['end']])
            ->andWhere(['type' => CreditsLog::SIGN_TYPE]);
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->auth_key = Yii::$app->security->generateRandomString();
            $inviteCode = new InviteCodeHelper();
            $this->invitation_code = $inviteCode->id2Code($this->id);
            // 昵称 (若迁移老数据,这里要关闭)
            $this->nickname = "Task" . time();
            // 注册IP (若迁移老数据,这里要关闭)
            $this->register_ip = Yii::$app->request->getUserIP();
            // 分配审核微信号
            // 首先拿到的微信号
            $wechat_number = ReviewsWechat::find()->where(['status' => 1])->orderBy(['allocation_times' => SORT_ASC])->one();
            if (!empty($wechat_number)) {
                $this->w_id = $wechat_number['id'];
                $wechat_number->allocation_times += 1;
                $wechat_number->save(false);
            }

        }

        return parent::beforeSave($insert);
    }

    /**
     * 添加账户信息  添加绑定信息  (若迁移老数据,这里要关闭)
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\base\Exception
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function afterSave($insert, $changedAttributes)
    {
        // 若迁移老数据,关闭这里
        if ($insert) {
            // 添加账户信息
            $account = new Account();
            $account->member_id = $this->id;
            $account->save();
            // 添加绑定账户信息
            $binding_account = new BindingAccount();
            $binding_account->member_id = $this->id;
            //初始化二级密码(默认手机号后6位)
            $binding_account->safety_password_hash = Yii::$app->security->generatePasswordHash(mb_substr($this->username, -6));
            $binding_account->save();
        }
        if (isset($changedAttributes['status']) && $this->status == StatusEnum::DISABLED) {
            // 禁用账号后直接驳回提现
            $un_deal_bills = WithdrawBill::find()->where(['member_id' => $this->id, 'status' => 0])->all();
            foreach ($un_deal_bills as $un_deal_bill) {
                $remark = "提现申请已被拒绝，当前余额增加" . $un_deal_bill->withdraw_money . "元";
                $type = RechargeForm::TYPE_MONEY;
                $rechargeForm = new RechargeForm([
                    'type' => $type,
                    'money' => $un_deal_bill->withdraw_money,
                    'int' => 0,
                    'remark' => $remark,
                ]);
                $rechargeForm->save($un_deal_bill->member, CreditsLog::WITHDRAW_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, 0, 0);
                $un_deal_bill->status = 2;
                $un_deal_bill->save(false);
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function getQualificationAudit()
    {
        return $this->hasOne(QualificationAuditLists::class, ['member_id' => 'id'])->orderBy(['created_at' => SORT_DESC]);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ],
            ]
        ];
    }

    /**
     * 获取用户信息
     * @param $memberId
     * @return array|null|ActiveRecord
     */
    public static function getUserInfoByMemberId($memberId)
    {
        $model = self::find()
            ->where(['id' => $memberId])
            ->select([
                'id',
                'username',
                'nickname',
                'head_portrait',
                'invitation_code',
                'today_sign_count',
            ])
            ->with(['account' => function ($query) {
                $query->select(['user_money', 'withdraw_money', 'accumulate_money', 'user_integral']);
            }])
            ->with(['bindingAccount' => function ($query) {
                $query->select(['alipay_account', 'alipay_user_name', 'wechat_account', 'wechat_account_url', 'alipay_account_url']);
            }])
            ->with(['todayAccount' => function ($query) {
                $query->select(['member_id', 'sum(num) as today_money']);
            }])
            ->with(['qualificationAudit' => function ($query) {
                $query->select(['member_id', 'status']);
            }])
            ->asArray()
            ->one();
        $model['todayAccount'] = isset($model['todayAccount'][0]['today_money']) ? $model['todayAccount'][0]['today_money'] : "0.00";
        // 资质认证状态
        if (!empty($model['qualificationAudit'])) {
            if ($model['qualificationAudit']['status'] == 0) {
                $model['qualificationAuditStatus'] = 2;
            } elseif ($model['qualificationAudit']['status'] == 1) {
                $model['qualificationAuditStatus'] = 1;
            } else {
                $model['qualificationAuditStatus'] = 0;
            }
        } else {
            $model['qualificationAuditStatus'] = 0;
        }
        unset($model['qualificationAudit']);
        return $model;
    }

    /**
     * 关联账变列表
     */
    public function getTypeAmount()
    {
        return $this->hasMany(CreditsLog::class, ['member_id' => 'id']);
    }

    /**
     * 根据ID和类型查找累积金额
     * @param $member_id
     * @param $type
     * @return array|null|ActiveRecord
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getTypeAmountByMemberId($member_id, $type)
    {
        $model = Member::find()
            ->where(['id' => $member_id, 'status' => StatusEnum::ENABLED])
            ->select(['id', 'sign_days'])
            ->with(['typeAmount' => function ($query) use ($type) {
                $query->select(['member_id', 'credit_type', 'sum(num) as numbers'])
                    ->where(['type' => $type])
                    ->groupBy('credit_type');
            }])
            ->asArray()
            ->one();
        // 判断类型
        if (!empty($model['typeAmount'])) {
            if (count($model['typeAmount']) == 2) {
                if ($model['typeAmount'][0]['credit_type'] == "user_money") {
                    $moneys = $model['typeAmount'][0]['numbers'];
                    $integrals = $model['typeAmount'][1]['numbers'];
                } else {
                    $moneys = $model['typeAmount'][1]['numbers'];
                    $integrals = $model['typeAmount'][0]['numbers'];
                }
            } else {
                if ($model['typeAmount'][0]['credit_type'] == "user_money") {
                    $moneys = $model['typeAmount'][0]['numbers'];
                    $integrals = "0.00";
                } else {
                    $moneys = "0.00";
                    $integrals = $model['typeAmount'][0]['numbers'];
                }
            }
        } else {
            $moneys = "0.00";
            $integrals = "0.00";
        }
        $model['moneys'] = $moneys;
        $model['integrals'] = $integrals;
        unset($model['typeAmount']);
        return $model;
    }

    /**
     * 获取推荐人列表
     * @param $recommend_id
     * @param $page
     * @return array|ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function getRecommendListsByRecommendId($recommend_id, $page)
    {
        $pagessize = Yii::$app->params['pagessize']['recommend_lists'];
        $page = !empty($page) ? $page : 1;
        $pages = ($page - 1) * $pagessize;
        $inner = "(SELECT `id` FROM " . self::tableName() . " b WHERE (`recommend_id` = $recommend_id " . "AND `status` = " . StatusEnum::ENABLED . ") ORDER BY `created_at` DESC LIMIT " . $pages . " , " . "$pagessize" . ") b using (`id`)";
        return self::find()
            ->select(['id', 'nickname', 'head_portrait', 'FROM_UNIXTIME(`created_at`,\'%Y-%m-%d\') as created_at'])
            ->innerJoin($inner)
            ->asArray()
            ->all();
    }

    /**
     *  获取上级
     * @return array
     */
    public static function getParents($member)
    {
        $parents[] = $member;
        if ($member['pid']) {
            self::findParent($member['pid'], $parents);
        }
        return $parents;
    }

    /**
     *  递归获取上级
     * @param $id
     * @param $parents
     */
    private static function findParent($id, &$parents)
    {
        $parent = self::find()->with(['account' => function ($query) {
            return $query->select('user_money');
        }])->select('id,username as title,recommend_id as pid')->where(['id' => $id])->asArray()->one();
        if ($parent) {
            $parent['title'] .= '(' . $parent['account']['user_money'] . ')';
            $parents[] = $parent;
            if ($parent['pid']) {
                self::findParent($parent['pid'], $parents);
            }
        }
    }

    /**
     * 关联微信号
     * @return \yii\db\ActiveQuery
     * @author 哈哈
     */
    public function getWechatNumber()
    {
        return $this->hasOne(ReviewsWechat::class, ['id' => 'w_id']);
    }

}
