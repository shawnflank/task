<?php

namespace common\models\forms;

use yii\base\Model;
use common\models\member\Member;

/**
 * Class CreditsLogForm
 * @package common\models\forms
 * @author 原创脉冲 <QQ：2790684490>
 */
class CreditsLogForm extends Model
{
    /**
     * @var Member
     */
    public $member;
    public $num = 0;
    public $credit_group;
    public $credit_group_detail = '';
    public $remark = '';
    public $map_id = 0;
    // 默认账单类型
    public $type;

    // 默认1增加累积金额 0不增加
    public $increase;
    /**
     * 支付类型
     *
     * @var int
     */
    public $pay_type = 0;
}