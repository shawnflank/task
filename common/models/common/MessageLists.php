<?php

namespace common\models\common;

use Yii;

/**
 * This is the model class for table "task_message_lists".
 *
 * @property int $id
 * @property string $title 标题
 * @property string $content 消息内容
 * @property string $jump_url 跳转链接
 * @property int $member_id 用户id
 * @property int $status 0未读,1已读
 * @property int $created_at 发送时间
 * @property int $updated_at 阅读时间
 * @property int $type 类型(0,用户发给管理员,1管理员发给用户)
 * @property int $jump_type 跳转类型(0,站内,1站外)
 */
class MessageLists extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_message_lists';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'jump_url', 'member_id'], 'required'],
            [['member_id', 'status', 'created_at', 'updated_at', 'type', 'jump_type'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['content', 'jump_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '标题',
            'content' => '消息内容',
            'jump_url' => '跳转链接',
            'member_id' => '用户id',
            'status' => '0未读,1已读',
            'created_at' => '发送时间',
            'updated_at' => '阅读时间',
            'type' => '类型(0,用户发给管理员,1管理员发给用户)',
            'jump_type' => '跳转类别',
        ];
    }


    /**
     * 新增模型
     * @param $member_id
     * @param $title
     * @param $content
     * @param $jump_url
     * @param int $type "0用户发送给管理员,1管理员发送给用户"
     * @param int $jump_type "0站内,1站外"
     * @return bool|MessageLists
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function createdNewModel($member_id, $title, $content, $jump_url, $type = 0, $jump_type = 0)
    {
        $model = new self;
        $model->member_id = $member_id;
        $model->title = $title;
        $model->content = $content;
        $model->jump_url = $jump_url;
        $model->created_at = time();
        $model->type = $type;
        $model->jump_type = $jump_type;
        if ($model->save(false)) {
            return $model;
        } else {
            return false;
        }
    }
}
