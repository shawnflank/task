<?php

namespace common\models\common;

use common\helpers\BcMathHelper;
use Yii;

/**
 * This is the model class for table "task_statistics".
 *
 * @property int $id
 * @property int $register_member 今日注册人数
 * @property int $sign_member 今日签到人数
 * @property int $get_task_member 今日领取任务人数
 * @property int $over_task_member 今日完成任务人数
 * @property int $over_task 今日完成任务数
 * @property string $withdraw_money 今日提现金额
 * @property string $date 日期
 * @property int $created_at 截止统计时间
 */
class Statistics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['register_member', 'sign_member', 'over_task', 'created_at', 'get_task_member', 'over_task_member'], 'integer'],
            [['withdraw_money'], 'number'],
            [['date'], 'required'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'register_member' => '今日注册人数',
            'sign_member' => '今日签到人数',
            'get_task_member' => '今日领取任务人数',
            'over_task_member' => '今日完成任务人数',
            'over_task' => '今日完成任务数',
            'withdraw_money' => '今日提现金额',
            'date' => '日期',
            'created_at' => '截止统计时间',
        ];
    }


    /**
     * 更新今日注册人数
     * @param $date
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function updateRegisterMember($date)
    {
        $model = self::find()->where(['date' => $date])->one();
        if (empty($model)) {
            $model = new self();
            $model->date = date("Y-m-d");
        }
        $model->register_member += 1;
        $model->created_at = time();
        $model->save(false);
        return true;
    }

    /**
     * 更新今日签到人数
     * @param $date
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function updateSignMember($date)
    {
        $model = self::find()->where(['date' => $date])->one();
        if (empty($model)) {
            $model = new self();
            $model->date = date("Y-m-d");
        }
        $model->sign_member += 1;
        $model->created_at = time();
        $model->save(false);
        return true;
    }

    /**
     * 更新今日完成任务数
     * @param $date
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function updateOverTask($date)
    {
        $model = self::find()->where(['date' => $date])->one();
        if (empty($model)) {
            $model = new self();
            $model->date = date("Y-m-d");
        }
        $model->over_task += 1;
        $model->created_at = time();
        $model->save(false);
        return true;
    }

    /**
     * 更新今日提现金额
     * @param $date
     * @param $withdraw_money
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function updateWithdrawMoney($date, $withdraw_money)
    {
        $model = self::find()->where(['date' => $date])->one();
        if (empty($model)) {
            $model = new self();
            $model->date = date("Y-m-d");
        }
        $model->withdraw_money = BcMathHelper::add($model->withdraw_money, $withdraw_money);
        $model->created_at = time();
        $model->save(false);
        return true;
    }

    /**
     * 更新领取任务人数
     * @param $date
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function updateGetTaskMember($date)
    {
        $model = self::find()->where(['date' => $date])->one();
        if (empty($model)) {
            $model = new self();
            $model->date = date("Y-m-d");
        }
        $model->get_task_member += 1;
        $model->created_at = time();
        $model->save(false);
        return true;
    }

    /**
     * 更新完成任务人数
     * @param $date
     * @return bool
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function updateOverTaskMember($date)
    {
        $model = self::find()->where(['date' => $date])->one();
        if (empty($model)) {
            $model = new self();
            $model->date = date("Y-m-d");
        }
        $model->over_task_member += 1;
        $model->created_at = time();
        $model->save(false);
        return true;
    }

    /**
     * @param $from_date
     * @param $to_date
     * @return array|\yii\db\ActiveRecord[]
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function findBetweenByCreatedAt($from_date, $to_date)
    {
        return self::find()
            ->where(['between', 'created_at', strtotime($from_date), strtotime($to_date) + 86399])
            ->orderBy('created_at asc')
            ->asArray()
            ->all();
    }

    /**
     * @param $date
     * @return array|null|\yii\db\ActiveRecord
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function findByDate($date)
    {
        return self::find()
            ->where(['date' => $date])
            ->asArray()
            ->one();
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     * @author 原创脉冲 <QQ：2790684490>
     */
    public static function findByDateAll()
    {
        return self::find()
            ->select(['sum(`register_member`) as register_member', 'sum(`sign_member`) as sign_member', 'sum(`over_task`) as over_task', 'sum(`withdraw_money`) as withdraw_money', 'sum(`get_task_member`) as get_task_member', 'sum(`over_task_member`) as over_task_member',])
            ->asArray()
            ->one();
    }
}
