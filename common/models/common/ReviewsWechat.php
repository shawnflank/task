<?php

namespace common\models\common;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task_reviews_wechat".
 *
 * @property int $id
 * @property string $wechat_number 审核微信号
 * @property int $allocation_times 已分配次数
 * @property int $created_at 添加人
 * @property int $created_by 添加时间
 * @property int $status 状态(1正常,0禁用)
 */
class ReviewsWechat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_reviews_wechat';
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by'],
                ],
            ]
        ];
    }

    /**
     * 关联管理员
     * @return \yii\db\ActiveQuery
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function getManager()
    {
        return $this->hasOne(\common\models\sys\Manager::class, ['id' => 'created_by']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wechat_number'], 'required'],
            [['allocation_times', 'created_at', 'created_by', 'status'], 'integer'],
            [['wechat_number'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wechat_number' => '审核微信号',
            'allocation_times' => '已分配次数',
            'created_at' => '添加人',
            'created_by' => '添加时间',
            'status' => '状态',
        ];
    }
}
