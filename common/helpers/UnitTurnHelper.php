<?php

namespace common\helpers;

final class UnitTurnHelper
{
    /**
     *  高转低，默认除以100保留2位小数
     * @param int $value
     * @param int $second_number
     * @param string $scale
     * @return float
     */
    public final static function h2l($value, $second_number = 100, $scale = '2'): float
    {
        if ($value == 0 || $value == ''){
            return 0;
        }
        return bcdiv($value, $second_number, $scale);
    }

    /**
     *  低转高，默认乘以100保留2位小数
     * @param float $value
     * @param int $second_number
     * @param string $scale
     * @return int
     */
    public final static function l2h($value, $second_number = 100, $scale = '2'): int
    {
        if ($value == 0 || $value == ''){
            return 0;
        }
        return bcmul($value,$second_number, $scale);
    }
}