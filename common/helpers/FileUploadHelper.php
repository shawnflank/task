<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/1
 * Time: 13:08
 */

namespace common\helpers;

use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use Yii;
use Qcloud\Cos\Client;

/**
 * 文件上传常用方法
 * Class FileUploadHelper
 * @package common\helpers
 */
class FileUploadHelper
{
    /**
     * API POST上传本地文件
     * @param $fileName '字段名'
     * @param string $filePath '自定义文件夹名称'
     * @param bool $is_upload '是否上传图片(默认必须上传)'
     * @return array
     * @throws \yii\base\Exception
     *
     * **************************
     * 使用方式
     * 如需上传的字段名为avatar
     * 上传用户头像
     * $avatar = UploadFile::add('avatar', 'avatar', false);
     * if ($avatar['code'] == 500) {
     *     return $this->jsonResponse(500, $avatar['message']);
     * } else {
     *     $avatar = $avatar['img_url'];
     * }
     * **************************
     */
    public static function LocalUploadFileApi($fileName, $filePath = 'default', $is_upload = true)
    {
        $res = ['code' => 500, 'message' => '',];
        $file = UploadedFile::getInstanceByName($fileName);
        if ($file) {
            if ($file->hasError) {
//                $res['message'] = $file->error;
                $res['message'] = "请上传正确的图片";
                return $res;
            }
            if (!$file->size) {
                $res['message'] = '文件内容是空的';
                return $res;
            }
            $allowTypes = ['image/jpeg', 'image/png', 'image/gif', 'image/bmp', 'image/heic'];
            if (!in_array($file->type, $allowTypes)) {
                $res['message'] = '图片格式不正确';
                return $res;
            }
            // 验证文件格式
            if (!in_array($file->extension, ['jpg', 'gif', 'png', 'jpeg', 'bmp', 'heic'])) {
                $res['message'] = '图片格式不正确';
                return $res;
            }
            // 验证文件大小
            if ($file->size >= 1024 * 1024 * 3) {
                $res['message'] = '图片大小超限小于3M';
                return $res;
            }
            $randomNumber = microtime() . mt_rand(111111, 999999);
            $filename = md5($randomNumber) . '.' . $file->extension;
            $randomFolder = date('Ymd');
            $savePath = '/uploads/' . $filePath . '/' . $randomFolder . '/' . $filename;
            $trueSavePath = Yii::getAlias('@webroot') .'/../attachment'. $savePath;
            if (!FileHelper::createDirectory(dirname($trueSavePath))) {
                $res['message'] = '目录权限错误';
                return $res;
            }
            if ($file->saveAs($trueSavePath)) {
                $res['code'] = 200;
                $res['img_url'] = Yii::$app->request->hostInfo . '/attachment'.$savePath;
            } else {
                $res['message'] = $file->error;
            }
            return $res;
        } else {
            // 如果图片必须上传
            if ($is_upload) {
                $res['message'] = '图片必须上传';
            } else {
                $res['code'] = 200;
                $res['img_url'] = '';
            }
            return $res;
        }
    }

    /**
     * COS上传文件
     * @param $file "文件"
     * @param $cos_file_path "上传COS文件路径"
     * @return mixed
     * @throws \Exception
     */
    public static function CosUploadFile($file, $cos_file_path)
    {
        $cosClient = new Client(array(
            'region' => 'ap-guangzhou', #地域，如ap-guangzhou,ap-beijing-1
            'credentials' => array(
                'secretId' => 'AKID8nIZaPBLRgkdGejhMl15xay83kBTRT8Y',
                'secretKey' => 'JNCmCRwEH5ey2iCUZOzLRHxRjlY7RWL1',
            ),
        ));
        $file_type = explode("/", $file->type);
        $allowed_types = array('jpg', 'gif', 'png', 'jpeg', 'svg', 'peg');
        if (!in_array($file_type[1], $allowed_types)) {
            throw new \Exception('不被允许的文件');
        }
        $body = file_get_contents($file->tempName);
        $result = $cosClient->putObject(array(
            'Bucket' => 'tuku-1259005996',
            'Key' => '/' . $cos_file_path . '.' . $file_type[1],
            'Body' => $body
        ));
        return $result['ObjectURL'];
    }

    /**
     * COS删除文件
     * @param $cos_path "图片路径"
     */
    public static function CosDeleteFile($cos_path)
    {
        $cosClient = new Client(array(
            'region' => 'ap-guangzhou', #地域，如ap-guangzhou,ap-beijing-1
            'credentials' => array(
                'secretId' => 'AKID8nIZaPBLRgkdGejhMl15xay83kBTRT8Y',
                'secretKey' => 'JNCmCRwEH5ey2iCUZOzLRHxRjlY7RWL1',
            ),
        ));
        $result = $cosClient->deleteObject(array(
            'Bucket' => 'tuku-1259005996',
            'Key' => $cos_path,
        ));
    }

    /**
     * BASE64上传文件到COS
     * @param $base64_img "base64图片"
     * @param $uplopd_cos_file_path "上传COS文件路径"
     * @return bool 正确返回图片路径
     */
    public static function CosUploadsBase64($base64_img, $uplopd_cos_file_path)
    {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_img, $result)) {
            //文件重命名
            $type = $result[2];
            if (in_array($type, array('jpeg', 'jpg', 'bmp', 'png'))) {
                $img_name = date('YmdHis') . mt_rand(100, 1000);
                $dir_path = "./weiban/";
                if (!file_exists($dir_path)) {
                    mkdir($dir_path, 0777, true);
                }
                $file_Path_vend_banner = $dir_path . 'renwu' . $img_name . '.' . $type;
                if (file_put_contents($file_Path_vend_banner, base64_decode(str_replace($result[1], '', $base64_img)))) {
                    //上传cos 文件路径
                    $cos_file_path = "./weiban/" . 'renwu' . $img_name . '.' . $type;       //拼接存储路径和文件名称，上传第一个参数
                    $cosClient = new Client(array(
                        'region' => 'ap-guangzhou', #地域，如ap-guangzhou,ap-beijing-1
                        'credentials' => array(
                            'secretId' => 'AKID8nIZaPBLRgkdGejhMl15xay83kBTRT8Y',
                            'secretKey' => 'JNCmCRwEH5ey2iCUZOzLRHxRjlY7RWL1',
                        ),
                    ));
                    $body = file_get_contents($cos_file_path);
                    $result = $cosClient->putObject(array(
                        'Bucket' => 'tuku-1259005996',
                        'Key' => $uplopd_cos_file_path . '.' . $type,
                        'Body' => $body
                    ));
                    if (file_exists($file_Path_vend_banner)) {
                        unlink($file_Path_vend_banner);
                    }
                    return $result['ObjectURL'];
                }
            } else {
                return false;
            }
        } else {
            //文件类型错误
            return false;
        }
    }

    /**
     * api接口上传图片
     * @param $file "文件"
     * @param $cos_file_path "上传COS文件路径"
     * @return mixed
     * @throws \Exception
     */
    public static function CosUploadFileApi($file, $cos_file_path)
    {
        $cosClient = new Client(array(
            'region' => 'ap-guangzhou', #地域，如ap-guangzhou,ap-beijing-1
            'credentials' => array(
                'secretId' => 'AKID8nIZaPBLRgkdGejhMl15xay83kBTRT8Y',
                'secretKey' => 'JNCmCRwEH5ey2iCUZOzLRHxRjlY7RWL1',
            ),
        ));
        $file_type = explode("/", $file['type']);
        $allowed_types = array('jpg', 'gif', 'png', 'jpeg', 'svg', 'peg');
        if (!in_array($file_type[1], $allowed_types)) {
            throw new \Exception('不被允许的文件');
        }
        $body = file_get_contents($file['tmp_name']);
        $result = $cosClient->putObject(array(
            'Bucket' => 'tuku-1259005996',
            'Key' => '/' . $cos_file_path . '.' . $file_type[1],
            'Body' => $body
        ));
        return $result['ObjectURL'];
    }


}