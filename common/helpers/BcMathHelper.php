<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/7/24
 * Time: 13:04
 */

namespace common\helpers;


/**
 * 精确计算PHP加减乘除
 * $firstNumber 第一个数字
 * $lastNumber 第二个数字
 * $scale 保留几位小数点
 * Class BcmathHelper
 * @package common\helpers
 */
final class BcMathHelper
{


    /**
     * 比较
     * @param $firstNumber
     * @param $lastNumber
     * @param int $scale
     * @return float 返回值 相等0 大于1 小于-1
     */
    final public static function comparison($firstNumber, $lastNumber, $scale = 2): float
    {
        return bccomp($firstNumber, $lastNumber, $scale);
    }

    /**
     * 加法
     * @param $firstNumber
     * @param $lastNumber
     * @param int $scale
     * @return float
     */
    final public static function add($firstNumber, $lastNumber, $scale = 2): float
    {
        return bcadd($firstNumber, $lastNumber, $scale);
    }

    /**
     * 减法
     * @param $firstNumber
     * @param $lastNumber
     * @param int $scale
     * @return float
     */
    final public static function sub($firstNumber, $lastNumber, $scale = 2): float
    {
        return bcsub($firstNumber, $lastNumber, $scale);
    }

    /**
     * 乘法
     * @param $firstNumber
     * @param int $lastNumber
     * @param int $scale
     * @return float
     */
    final public static function mul($firstNumber, $lastNumber = 100, $scale = 2): float
    {
        return bcmul($firstNumber, $lastNumber, $scale);
    }

    /**
     * 除法
     * @param $firstNumber
     * @param int $lastNumber
     * @param int $scale
     * @return float
     */
    final public static function div($firstNumber, $lastNumber = 100, $scale = 2): float
    {
        if ($lastNumber == 0) {
            return 0;
        }
        return bcdiv($firstNumber, $lastNumber, $scale);
    }

    /**
     * 余数
     * @param $firstNumber
     * @param $lastNumber
     * @return float 返回余数
     */
    final public static function mod($firstNumber, $lastNumber): float
    {
        return bcmod($firstNumber, $lastNumber);
    }


}