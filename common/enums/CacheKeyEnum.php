<?php

namespace common\enums;

/**
 * Class CacheKeyEnum
 * @package common\enums
 * @author 原创脉冲 <QQ：2790684490>
 */
class CacheKeyEnum
{
    const API_MINI_PROGRAM_LOGIN = 'api:mini-program:auth:'; // 小程序授权
    const API_ACCESS_TOKEN = 'api:access-token:'; // 用户信息记录
    const SYS_CONFIG = 'sys:config'; // 公用参数
    const WECHAT_FANS_STAT = 'wechat:fans:stat:'; // 粉丝统计缓存
    const COMMON_ADDONS = 'common:addons:'; // 插件
    const COMMON_ADDONS_CONFIG = 'common:addons-config:'; // 插件配置
    const COMMON_PROVINCES = 'common:provinces:'; // 省市区
    const COMMON_IP_BLACKLIST = 'common:ip-blacklist:'; // ip黑名单
    const COMMON_ACTION_BEHAVIOR = 'common:action-behavior'; // 需要被记录的行为
    const IMG_LISTS_INFO = 'common:img-lists'; // 图片列表
}