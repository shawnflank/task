<?php

namespace common\enums;

/**
 * Class WhetherEnum
 * @package common\enums
 * @author 原创脉冲 <QQ：2790684490>
 */
class WhetherEnum
{
    const ENABLED = 1;
    const DISABLED = 0;

    /**
     * @var array
     */
    public static $listExplain = [
        self::ENABLED => '是',
        self::DISABLED => '否',
    ];
}