<?php


namespace common\queues;


use yii\base\BaseObject;

class TestJob extends BaseObject implements \yii\queue\JobInterface
{
    public $data;
    public function execute($queue)
    {
        file_put_contents('11.txt', $this->data);
    }
}