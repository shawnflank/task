<?php

namespace addons\RfArticle\backend\controllers;

use Yii;
use common\components\Curd;
use addons\RfArticle\common\models\ArticleSingle;

/**
 * 单页管理
 *
 * Class ArticleSingleController
 * @package addons\RfArticle\backend\controllers
 * @author 原创脉冲 <QQ：2790684490>
 */
class ArticleSingleController extends BaseController
{
    use Curd;

    /**
     * @var ArticleSingle
     */
    public $modelClass = ArticleSingle::class;
}