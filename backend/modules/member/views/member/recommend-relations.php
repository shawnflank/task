<?php

?>

<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title">代理关系</h4>
    </div>
    <div class="modal-body">
        <div class="col-sm-5">
            上级(<?= count($parents)?>人)
            <?= \backend\widgets\jstree\JsTree::widget([
                'name' => "parents",
                'defaultData' => $parents,
                'selectIds' => [],
                'autoOpen' => 'true',
                'theme' => 'relations'
            ])?>
        </div>
        <div class="col-sm-5">
            下级
            <?= \backend\widgets\jstree\JsTree::widget([
                'name' => "children",
                'defaultData' => $children,
                'selectIds' => [],
                'autoOpen' => 'false',
                'url' => 'get-children',
                'cid' => $cid,
                'theme' => 'relations'
            ])?>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
    </div>
</div>