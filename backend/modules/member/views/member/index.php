<?php


use common\helpers\Html;
use common\helpers\ImageHelper;
use common\helpers\DateHelper;
use kartik\grid\GridView;

$this->title = '会员信息';
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title; ?></h3>
                <div class="box-tools">
                    <?= Html::create(['ajax-edit'], '创建', [
                        'data-toggle' => 'modal',
                        'data-target' => '#ajaxModal',
                    ]) ?>
                </div>
            </div>
            <div class="box-body table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    //重新定义分页样式
                    'tableOptions' => ['class' => 'table table-hover'],
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'visible' => false, // 不显示#
                        ],
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['width' => '70px'],
                        ],
                        [
                            'attribute' => 'created_at',
                            'label' => '注册时间',
                            'filter' => \kartik\daterange\DateRangePicker::widget([
                                'model' => $searchModel,
                                'convertFormat' => true,
                                'name' => 'created_at',
                                'attribute' => 'created_at',
                                'hideInput' => true,
                                'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                'pluginOptions' => [
                                    'timePicker' => true,
                                    'locale' => [
                                        'format' => 'Y-m-d',
                                        'separator' => '~'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                ]
                            ]),
                            'value' => function ($model) {
                                return DateHelper::dateTime($model->created_at);
                            },
                            'headerOptions' => ['width' => '102px'],
                        ],
                        [
                            'attribute' => 'head_portrait',
                            'value' => function ($model) {
                                return Html::img(ImageHelper::defaultHeaderPortrait(Html::encode($model->head_portrait)),
                                    [
                                        'class' => 'img-circle rf-img-md img-bordered-sm',
                                    ]);
                            },
                            'filter' => false,
                            'format' => 'raw',
                        ],
                        [
                            'class' => '\kartik\grid\EditableColumn',
                            'attribute' => 'username',
                            'headerOptions' => ['width' => '118px'],
                        ],

                        [
                            'class' => '\kartik\grid\EditableColumn',
                            'attribute' => 'nickname',
                            'headerOptions' => ['width' => '105px'],
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'notice',
                            'editableOptions' => [
                                'asPopover' => false,
                                'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,//只需添加如下代码
                                'options' => [
                                    'rows' => 4,
                                ],
                            ],
                            'value' => function ($model) {
                                if (!empty($model->notice) && mb_strlen($model->notice) > 5) {
                                    return mb_substr($model->notice, 0, 5, 'utf-8') . "..";
                                } else {
                                    return $model->notice;
                                }

                            },
                            'headerOptions' => ['width' => '105px'],
                        ],
                        [
                            'class' => '\kartik\grid\EditableColumn',
                            'attribute' => 'invitation_code',
                            'headerOptions' => ['width' => '100px'],
                        ],

                        [
                            'label' => '账户金额',
                            'filter' => false, //不显示搜索框
                            'value' => function ($model) {
                                return "余额：" . $model->account->user_money . '<br>' .
                                    "累积金额：" . $model->account->accumulate_money . '<br>' .
                                    "积分：" . $model->account->user_integral . '<br>' .
                                    "累计积分：" . $model->account->accumulate_integral . '<br>' .
                                    "已提现金额：" . $model->account->withdraw_money;
                            },
                            'format' => 'raw',
                        ],
                        [
                            'label' => '账号详情',
                            'filter' => false, //不显示搜索框
                            'value' => function ($model) {
                                $recommend_name = $model->recommendMember->username ?? "(未设置)";
                                return
                                    "连签天数：" . $model->sign_days . '<br>' .
                                    "推荐人：" . $recommend_name . '<br>' .
                                    "推荐人数：" . $model->recommend_number . '<br>' .
                                    "最后访问IP：" . $model->last_ip . '<br>' .
                                    "最后登录：" . DateHelper::dateTime($model->last_time) . '<br>';
                            },
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'experience',
                            'value' => function ($model) {
                                return $model->experience;
                            },
                            'headerOptions' => ['width' => '60px'],
                        ],
                        [
                            'attribute' => 'level_id',
                            'value' => function ($model) {
                                return $model->level_id . "级";
                            },
                            'headerOptions' => ['width' => '50px'],
                        ],

                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'w_id',
                            'filterType' => GridView::FILTER_SELECT2,
                            'editableOptions' => [
                                'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                                'asPopover' => false,
                                'data' => $reviewsWechat,
                            ],
                            'filterWidgetOptions' => [
                                'data' => $reviewsWechat,
                                'options' => [
                                    'prompt' => "请选择",
                                ],
                                'hideSearch' => true,
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ],
                            'filter' => $reviewsWechat,
                            'value' => function ($model) use ($reviewsWechat) {
                                if ($model->w_id != 0) {
                                    return isset($reviewsWechat[$model->w_id]) ? $reviewsWechat[$model->w_id] : '失效';
                                } else {
                                    return "未绑定";
                                }
                            },
                        ],

                        [
                            'header' => "操作",
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '180px'],
                            'template' => '{ajax-edit} {recharge} {edit} {status} {destroy} {recommend}',
                            'buttons' => [
                                'ajax-edit' => function ($url, $model, $key) {
                                    return Html::linkButton(['ajax-edit', 'id' => $model->id], '账号', [
                                        'data-toggle' => 'modal',
                                        'data-target' => '#ajaxModal',
                                    ]);
                                },
                                'recharge' => function ($url, $model, $key) {
                                    return Html::linkButton(['recharge', 'id' => $model->id], '充值', [
                                        'data-toggle' => 'modal',
                                        'data-target' => '#ajaxModal',
                                    ]);
                                },
                                'edit' => function ($url, $model, $key) {
                                    return Html::edit(['edit', 'id' => $model->id]);
                                },
                                'status' => function ($url, $model, $key) {
                                    return Html::status($model->status);
                                },
                                'destroy' => function ($url, $model, $key) {
                                    return Html::delete(['destroy', 'id' => $model->id]);
                                },
                                'recommend' => function ($url, $model, $key) {
                                    return Html::linkButton(['recommend-relations', 'id' => $model->id], '代理', [
                                        'data-toggle' => 'modal',
                                        'data-target' => '#ajaxModalLg',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

