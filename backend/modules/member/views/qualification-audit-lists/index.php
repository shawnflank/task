<?php

use common\helpers\Html;
use common\helpers\Url;
use kartik\grid\GridView;
use common\models\member\QualificationAuditLists;
use common\helpers\DateHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '申请列表';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                <div class="box-tools">
                </div>
            </div>
            <div class="box-body table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'hover' => true,
                    "options" => ["class" => "grid-view", "style" => "overflow:auto", "id" => "grid"],
                    'tableOptions' => ['class' => 'table table-hover'],
                    'columns' => [
//                        [
//                            'class' => 'yii\grid\SerialColumn',
//                            'visible' => false,
//                        ],
                        // 若要全选则关闭上面打开下面的代码
                        [
                            'class' => '\kartik\grid\CheckboxColumn',
                            'rowSelectedClass' => GridView::TYPE_INFO,
                            'visible' => true,
                        ],

                        'id',
                        [
                            'label' => '会员信息',
                            'attribute' => 'member_id',
                            'filter' => Html::activeTextInput($searchModel, 'member_id', [
                                    'class' => 'form-control',
                                    'placeholder' => '会员账号'
                                ]
                            ),
                            'value' => function ($model) {
                                return "账号：" . $model->member->username . '<br>' .
                                    "昵称：" . $model->member->nickname . '<br>';
                            },
                            'format' => 'raw',
                        ],

                        [
                            'class' => '\kartik\grid\EditableColumn',
                            'attribute' => 'notice',
                            'value' => function ($model) {
                                if (!empty($model->member->notice)) {
                                    if (mb_strlen($model->member->notice) > 10) {
                                        $notice = mb_substr($model->member->notice, 0, 10, 'utf-8') . "..";
                                    } else {
                                        $notice = $model->member->notice;
                                    }
                                } else {
                                    $notice = "(无)";
                                }

                                return $notice;
                            },
                        ],
                        [
                            'attribute' => 'verification_img_data',
                            'filter' => false, //不显示搜索框
                            'value' => function ($model) {

                                return \common\helpers\ImageHelper::fancyBoxs(unserialize($model->verification_img_data));
                            },
                            'format' => 'raw'
                        ],
                        [
                            'class' => '\kartik\grid\EditableColumn',
                            'attribute' => 'verification_text_data',
                        ],
                        [
                            'attribute' => 'w_id',
                            'format' => 'raw',
                            'filter' => Html::activeDropDownList($searchModel, 'w_id', $reviewsWechat, [
                                    'prompt' => '全部',
                                    'class' => 'form-control'
                                ]
                            ),
                            'value' => function ($model) {
                                $wechat_number = Yii::$app->debris->config('wechat_number');
                                if (!empty($wechat_number)) {
                                    return $wechat_number;
                                } else {
                                    if (!empty($model->wechatNumber->wechat_number)) {
                                        return "审核微信号:" . $model->wechatNumber->wechat_number;
                                    } else {
                                        return "暂无";
                                    }
                                }
                            }
                        ],

                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'filter' => Html::activeDropDownList($searchModel, 'status', QualificationAuditLists::$statusArray, [
                                    'prompt' => '全部',
                                    'class' => 'form-control'
                                ]
                            ),
                            'value' => function ($model) {
                                return '<span class="label label-' . QualificationAuditLists::$qualification_status_color[$model->status] . '">' . QualificationAuditLists::$statusArray[$model->status] . '</span>';

                            },
                        ],
                        [
                            'attribute' => 'updated_by',
                            'filter' => Html::activeDropDownList($searchModel, 'updated_by', $managerArray, [
                                    'prompt' => '全部',
                                    'class' => 'form-control'
                                ]
                            ),
                            'value' => function ($model) use ($managerArray) {
                                return $managerArray[$model->updated_by] ?? "(暂无)";
                            },
                        ],
                        [
                            'attribute' => 'created_at',
                            'filter' => \kartik\daterange\DateRangePicker::widget([
                                'model' => $searchModel,
                                'convertFormat' => true,
                                'name' => 'created_at',
                                'attribute' => 'created_at',
                                'hideInput' => true,
                                'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                'pluginOptions' => [
                                    'timePicker' => true,
                                    'locale' => [
                                        'format' => 'Y-m-d',
                                        'separator' => '~'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                ]
                            ]),
                            'value' => function ($model) {
                                return DateHelper::dateTime($model->created_at);
                            },
                            'headerOptions' => ['width' => '102px'],
                        ],
                        [
                            'attribute' => 'updated_at',
                            'filter' => \kartik\daterange\DateRangePicker::widget([
                                'model' => $searchModel,
                                'convertFormat' => true,
                                'name' => 'updated_at',
                                'attribute' => 'updated_at',
                                'hideInput' => true,
                                'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                'pluginOptions' => [
                                    'timePicker' => true,
                                    'locale' => [
                                        'format' => 'Y-m-d',
                                        'separator' => '~'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                ]
                            ]),
                            'value' => function ($model) {
                                if ($model->updated_at == 0) {
                                    return "未审核";
                                }
                                return DateHelper::dateTime($model->updated_at);
                            },
                            'headerOptions' => ['width' => '102px'],
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => '操作',
                            'template' => '{pass} {refuse}{delete}',
                            'buttons' => [
                                'pass' => function ($url, $model, $key) {
                                    if ($model->status == 0) {
                                        return Html::linkButton(['update-status', 'id' => $model->id, 'status' => 1], '通过', [
                                            'class' => 'btn btn-success btn-sm',
                                        ]);
                                    }
                                },
                                'refuse' => function ($url, $model, $key) {
                                    if ($model->status == 0) {
                                        return Html::linkButton(['update-status', 'id' => $model->id, 'status' => 2], '拒绝', [
                                            'class' => 'btn btn-warning btn-sm',
                                        ]);
                                    }
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::delete(['delete', 'id' => $model->id]);
                                },
                            ]
                        ]
                    ],
                    'panel' => [
                        'heading' => false,
                        'before' => '<div class="box-header pull-left"><i class="fa fa-fw fa-sun-o"></i><h3 class="box-title">数据管理</h3></div>',
                        'footer' => false,
                        'after' => '<div class="pull-left" style="margin-top: 8px">{summary}</div><div class="kv-panel-pager pull-right">{pager}</div><div class="clearfix"></div>',
                    ],
                    'panelFooterTemplate' => '{footer}<div class="clearfix"></div>',
                    'toolbar' => [
                        '<div class="pull-left">'
                        . Html::a('<i class="glyphicon  glyphicon-ok-circle"></i>批量通过', Url::to('pass-all'), ['class' => 'btn btn-success', 'id' => 'bulk_forbid'])
                        . '</div>',
                        '{toggleData}',
                        '{export}'
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('
$("#bulk_forbid").on("click", function (e) {
    e.preventDefault();
    var keys = $("#grid").yiiGridView("getSelectedRows");
    if(keys.length < 1) {
        return rfError("", "没有选中任何项");
    }
    var href = $(this).attr("href");
    window.location.href = href + "?ids=" + keys.join();
});
');
?>

