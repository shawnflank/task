<?php

use common\helpers\Url;
use common\enums\PayEnum;
use common\helpers\Html;
use common\enums\StatusEnum;
use common\models\member\QualificationAuditLists;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">资质信息</h4>
</div>
<div class="modal-body">
    <table class="table table-hover text-center">
        <tbody>
        <tr>
            <td>账号</td>
            <td>
                <?= !empty($model->member->username) ? $model->member->username : "暂无"; ?>
            </td>
        </tr>

        <tr>
            <td>昵称</td>
            <td>
                <?= !empty($model->member->nickname) ? $model->member->nickname : "暂无"; ?>
            </td>
        </tr>

        <tr>
            <td>资质账号</td>
            <td>
                <?= !empty($model->verification_text_data) ? $model->verification_text_data : "暂无"; ?>
            </td>
        </tr>
        <tr>
            <td>资质资料</td>
            <td>
                <?php if (!empty($model->verification_img_data)) {
                    $verification_img_data = unserialize($model->verification_img_data);
                    foreach ($verification_img_data as $v1) {
                        echo '<a href="' . $v1 . '" style="margin-right: 9px;" data-fancybox="gallery"><img src="' . $v1 . '" width="45" height="45" alt=""></a>';
                    }
                } else {
                    echo "暂无";
                } ?>
            </td>
        </tr>
        <tr>
            <td>审核状态</td>
            <td>
                <?= !empty($model->status) ? '<span class="label label-' . QualificationAuditLists::$qualification_status_color[$model->status] . '">' . QualificationAuditLists::$statusArray[$model->status] . '</span>' : "暂无"; ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
</div>