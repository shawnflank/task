<?php

use common\helpers\Url;
use common\enums\PayEnum;
use common\helpers\Html;
use common\enums\StatusEnum;

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <h4 class="modal-title">账户信息</h4>
</div>
<div class="modal-body">
    <table class="table table-hover text-center">
        <tbody>
        <tr>
            <td>会员</td>
            <td>
                <?= $model->member->username; ?>
            </td>
        </tr>
        <tr>
            <td>提现金额</td>
            <td>
                <?= $withdraw_money; ?>
            </td>
        </tr>
        <?php if ($type == "alipay_account") { ?>
            <tr>
                <td>支付宝账号</td>
                <td>
                    <?= $model->alipay_account; ?>
                </td>
            </tr>
            <tr>
                <td>支付宝用户名</td>
                <td>
                    <?= $model->alipay_user_name; ?>
                </td>
            </tr>
        <?php } else { ?>
            <tr>
                <td>银行卡号</td>
                <td>
                    <?= $model->bank_card; ?>
                </td>
            </tr>
            <tr>
                <td>开户行</td>
                <td>
                    <?= $model->bank_address; ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
</div>