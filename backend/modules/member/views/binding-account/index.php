<?php

use common\helpers\Html;
use common\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '绑定列表';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                <div class="box-tools">

                </div>
            </div>
            <div class="box-body table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover'],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'visible' => false,
            ],

            'id',
            [
                'label' => '会员信息',
                'attribute' => 'member_id',
                'filter' => Html::activeTextInput($searchModel, 'member_id', [
                        'class' => 'form-control',
                        'placeholder' => '会员账号'
                    ]
                ),
                'value' => function ($model) {
                    return "用户ID：" . $model->member->id . '<br>' .
                        "昵称：" . $model->member->nickname . '<br>' .
                        "账号：" . $model->member->username . '<br>';
                },
                'format' => 'raw',
            ],
            'alipay_account',
            'alipay_user_name',
            'wechat_account',
//            'safety_password',
            [
                'attribute' => 'wechat_account_url',
                'filter' => false, //不显示搜索框
                'value' => function ($model) {
                    return \common\helpers\ImageHelper::fancyBox($model->wechat_account_url);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'alipay_account_url',
                'filter' => false, //不显示搜索框
                'value' => function ($model) {
                    return \common\helpers\ImageHelper::fancyBox($model->alipay_account_url);
                },
                'format' => 'raw'
            ],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'header' => '操作',
//                'template' => ' {delete}',
//                'buttons' => [
//                'delete' => function($url, $model, $key){
//                        return Html::delete(['delete', 'id' => $model->id]);
//                },
//                ]
//            ]
    ]
    ]); ?>
            </div>
        </div>
    </div>
</div>
