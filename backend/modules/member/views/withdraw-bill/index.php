<?php

use common\helpers\Html;
use common\helpers\Url;
use kartik\grid\GridView;
use common\models\member\WithdrawBill;
use common\helpers\DateHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '提现记录';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                <div class="box-tools">
                </div>
            </div>
            <div class="box-body table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'hover' => true,
                    "options" => ["class" => "grid-view", "style" => "overflow:auto", "id" => "grid"],
                    'tableOptions' => ['class' => 'table table-hover'],
                    'columns' => [
//                        [
//                            'class' => 'yii\grid\SerialColumn',
//                            'visible' => false,
//                        ],
                        // 若要全选则关闭上面打开下面的代码
                        [
                            'class' => '\kartik\grid\CheckboxColumn',
                            'rowSelectedClass' => GridView::TYPE_INFO,
                            'visible' => true,
                        ],

                        'id',
                        'sn',
                        [
                            'label' => '会员信息',
                            'attribute' => 'member_id',
                            'filter' => Html::activeTextInput($searchModel, 'member_id', [
                                    'class' => 'form-control',
                                    'placeholder' => '会员账号'
                                ]
                            ),
                            'value' => function ($model) {
                                if (!empty($model->member->notice)) {
                                    if (mb_strlen($model->member->notice) > 10) {
                                        $notice = mb_substr($model->member->notice, 0, 10, 'utf-8') . "..";
                                    } else {
                                        $notice = $model->member->notice;
                                    }
                                } else {
                                    $notice = "(无)";
                                }
                                $str =  "账号：" . $model->member->username . '<br>' .
                                    "昵称：" . $model->member->nickname . '<br>' .
                                    "备注：" . $notice . '<br>';
                                if ($model->status == 0) {
                                    $exists = WithdrawBill::find()->where(['member_id' => $model->member_id, 'status' => 1])->andWhere(['<>', 'id', $model->id])->exists();
                                    $is_first_time =  $exists ? '否' : '是';
                                    $str .= "是否首次提现：" . $is_first_time;
                                }
                                return $str;
                            },
                            'format' => 'raw',
                        ],

                        [
                            'attribute' => 'withdraw_money',
                            'headerOptions' => ['width' => '150px'],
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'filter' => Html::activeDropDownList($searchModel, 'type', WithdrawBill::$withdraw_type_array, [
                                    'prompt' => '全部',
                                    'class' => 'form-control'
                                ]
                            ),
                            'value' => function ($model) {
                                $data = WithdrawBill::$withdraw_type_array;
                                if ($model->type == 'alipay_account') {
                                    $str = '<a href="/backend/member/binding-account/view?id=' . $model->member_id . '&amp;type=' . $model->type . '&amp;withdraw_money=' . $model->withdraw_money . '" data-toggle="modal" data-target="#ajaxModal">【' . $data[$model->type] . '】</a>';
                                } elseif ($model->type == 'wechat_account_url') {
                                    $str = '<a href="' . $model->bindingAccount->wechat_account_url . '" data-fancybox="gallery">【' . $data[$model->type] . '】</a>';
                                } elseif ($model->type == 'alipay_account_url') {
                                    $str = '<a href="' . $model->bindingAccount->alipay_account_url . '" data-fancybox="gallery">【' . $data[$model->type] . '】</a>';
                                } else {
                                    $str = '【'.$data[$model->type].'】';
                                }
                                return $str . '<br>' .
                                    '<a href="/backend/member/qualification-audit-lists/view?memberId=' . $model->member_id . '" data-toggle="modal" data-target="#ajaxModal">【资质信息】</a>';
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'filter' => Html::activeDropDownList($searchModel, 'status', WithdrawBill::$withdraw_status_array, [
                                    'prompt' => '全部',
                                    'class' => 'form-control'
                                ]
                            ),
                            'value' => function ($model) {
                                return '<span class="label label-' . WithdrawBill::$withdraw_status_color[$model->status] . '">' . WithdrawBill::$withdraw_status_array[$model->status] . '</span>';
                            },
                        ],
                        [
                            'attribute' => 'updated_by',
                            'filter' => Html::activeDropDownList($searchModel, 'updated_by', $managerArray, [
                                    'prompt' => '全部',
                                    'class' => 'form-control'
                                ]
                            ),
                            'value' => function ($model) use ($managerArray) {
                                return $managerArray[$model->updated_by] ?? "(暂无)";
                            },
                        ],
                        [
                            'attribute' => 'created_at',
                            'filter' => \kartik\daterange\DateRangePicker::widget([
                                'model' => $searchModel,
                                'convertFormat' => true,
                                'name' => 'created_at',
                                'attribute' => 'created_at',
                                'hideInput' => true,
                                'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                'pluginOptions' => [
                                    'timePicker' => true,
                                    'locale' => [
                                        'format' => 'Y-m-d',
                                        'separator' => '~'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                ]
                            ]),
                            'value' => function ($model) {
                                return DateHelper::dateTime($model->created_at);
                            },
                        ],
                        [
                            'attribute' => 'updated_at',
                            'filter' => \kartik\daterange\DateRangePicker::widget([
                                'model' => $searchModel,
                                'convertFormat' => true,
                                'name' => 'updated_at',
                                'attribute' => 'updated_at',
                                'hideInput' => true,
                                'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                'pluginOptions' => [
                                    'timePicker' => true,
                                    'locale' => [
                                        'format' => 'Y-m-d',
                                        'separator' => '~'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                ]
                            ]),
                            'value' => function ($model) {
                                if ($model->updated_at == 0) {
                                    return "未审核";
                                }
                                return DateHelper::dateTime($model->updated_at);
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => '操作',
                            'template' => '{pass} {refuse} {cancel} {delete}',
                            'buttons' => [
                                'pass' => function ($url, $model, $key) {
                                    if ($model->status == 0) {
                                        return Html::linkButton(['update-status', 'id' => $model->id, 'status' => 1], '通过', [
                                            'class' => 'btn btn-success btn-sm',
                                        ]);
                                    }
                                },
                                'refuse' => function ($url, $model, $key) {
                                    if ($model->status == 0) {
                                        return Html::linkButton(['update-status', 'id' => $model->id, 'status' => 2], '拒绝', [
                                            'class' => 'btn btn-primary btn-sm',
                                            'onclick' => "rfTwiceAffirm(this, '拒绝后提现金额将退还给用户,是否执行？', '请谨慎操作');return false;",
                                        ]);
                                    }
                                },
                                'cancel' => function ($url, $model, $key) {
                                    if ($model->status == 0) {
                                        return Html::linkButton(['update-status', 'id' => $model->id, 'status' => 3], '取消', [
                                            'class' => 'btn btn-warning btn-sm',
                                            'onclick' => "rfTwiceAffirm(this, '取消后提现金额将不会退还给用户,是否执行？', '请谨慎操作');return false;",
                                        ]);
                                    }
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::delete(['delete', 'id' => $model->id]);
                                },
                            ]
                        ],
                    ],
                    'panel' => [
                        'heading' => false,
                        'before' => '<div class="box-header pull-left"><i class="fa fa-fw fa-sun-o"></i><h3 class="box-title">数据管理</h3></div>',
                        'footer' => false,
                        'after' => '<div class="pull-left" style="margin-top: 8px">{summary}</div><div class="kv-panel-pager pull-right">{pager}</div><div class="clearfix"></div>',
                    ],
                    'panelFooterTemplate' => '{footer}<div class="clearfix"></div>',
                    'toolbar' => [
                        '<div class="pull-left btn-toolbar">' .
                        Html::a('支付导出', Url::to('pay-export'), ['class' => 'btn btn-dropbox bulk_forbid']) . Html::a('<i class="glyphicon  glyphicon-ok-circle"></i>批量通过', Url::to('pass-all'), ['class' => 'btn btn-success bulk_forbid']) . Html::a('<i class="glyphicon  glyphicon-remove-circle"></i>批量驳回', Url::to('reject-all'), ['class' => 'btn btn-danger bulk_forbid'])
                        . '</div>',
                        '{toggleData}',
                        '{export}'
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('
$(".bulk_forbid").on("click", function (e) {
    e.preventDefault();
    var keys = $("#grid").yiiGridView("getSelectedRows");
    if(keys.length < 1) {
        return rfError("", "没有选中任何项");
    }
    var href = $(this).attr("href");
    window.location.href = href + "?ids=" + keys.join();
});
');
?>
