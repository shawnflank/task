<?php

use common\helpers\Url;
use common\helpers\Html;
use kartik\grid\GridView;
use common\models\member\CreditsLog;
use common\helpers\DateHelper;

$this->title = '账变日志';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];

?>

<div class="row">
    <div class="col-sm-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <?php foreach ($creditType as $key => $item) { ?>
                    <li <?php if ($credit_type == $key) { ?>class="active"<?php } ?>><a
                                href="<?= Url::to(['index', 'credit_type' => $key]) ?>"> <?= $item; ?></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        //重新定义分页样式
                        'tableOptions' => ['class' => 'table table-hover'],
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                            ],
                            [
                                'label' => '会员信息',
                                'attribute' => 'member_id',
                                'filter' => Html::activeTextInput($searchModel, 'member_id', [
                                        'class' => 'form-control',
                                        'placeholder' => '会员账号'
                                    ]
                                ),
                                'value' => function ($model) {
                                    return "用户ID：" . $model->member->id . '<br>' .
                                        "昵称：" . $model->member->nickname . '<br>' .
                                        "账号：" . $model->member->username . '<br>';
                                },
                                'format' => 'raw',
                            ],
                            [
                                'label' => '变动记录',
                                'attribute' => 'new_num',
                                'filter' => Html::activeTextInput($searchModel, 'new_num', [
                                        'class' => 'form-control',
                                        'placeholder' => '变动数量'
                                    ]
                                ),
                                'value' => function ($model) {
                                    $operational = $model->num < 0 ? '-' : '+';
                                    return $model->old_num . $operational . abs($model->num) . '=' . $model->new_num;
                                },
                            ],
                            [
                                'attribute' => 'credit_group',
                                'format' => 'raw',
                                'filter' => Html::activeDropDownList($searchModel, 'credit_group',\common\enums\AppEnum::$listExplain, [
                                        'prompt' => '全部',
                                        'class' => 'form-control'
                                    ]
                                ),
                                'value' => function ($model) {
                                    return \common\enums\AppEnum::$listExplain[$model->credit_group];
                                }
                            ],
                            'remark',
//                            [
//                                'class' => '\kartik\grid\DataColumn',
//                                'attribute' => 'type',
//                                'filterType' => GridView::FILTER_SELECT2,
//                                'filterWidgetOptions' => [
//                                    'data' => CreditsLog::$TypeExplain,
//                                    'options' => [
//                                        'prompt' => "请选择...",
//                                    ],
//                                    'hideSearch' => true,
//                                    'pluginOptions' => [
//                                        'allowClear' => true
//                                    ],
//                                ],
//                                'value' => function ($model) {
//                                    $data = CreditsLog::$TypeExplain;
//                                    return $data[$model->type];
//                                },
//                            ],

                            [
                                'attribute' => 'type',
                                'format' => 'raw',
                                'filter' => Html::activeDropDownList($searchModel, 'type', CreditsLog::$TypeExplain, [
                                        'prompt' => '全部',
                                        'class' => 'form-control'
                                    ]
                                ),
                                'value' => function ($model) {
                                    $data = CreditsLog::$TypeExplain;
                                    return $data[$model->type];
                                },
                            ],
                            [
                                'attribute' => 'credit_group_detail',
                                'format' => 'raw',
                                'filter' => Html::activeDropDownList($searchModel, 'credit_group_detail', CreditsLog::$creditGroupExplain, [
                                        'prompt' => '全部',
                                        'class' => 'form-control'
                                    ]
                                ),
                                'value' => function ($model) {
                                    return CreditsLog::$creditGroupExplain[$model->credit_group_detail];
                                }
                            ],
                            [
                                'attribute' => 'created_at',
                                'filter' => \kartik\daterange\DateRangePicker::widget([
                                    'model' => $searchModel,
                                    'convertFormat' => true,
                                    'name' => 'created_at',
                                    'attribute' => 'created_at',
                                    'hideInput' => true,
                                    'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                    'pluginOptions' => [
                                        'timePicker' => true,
                                        'locale' => [
                                            'format' => 'Y-m-d',
                                            'separator' => '~'
                                        ],
                                        'opens' => 'left'
                                    ],
                                    'pluginEvents' => [
                                        "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                    ]
                                ]),
                                'value' => function ($model) {
                                    return DateHelper::dateTime($model->created_at);
                                },
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>