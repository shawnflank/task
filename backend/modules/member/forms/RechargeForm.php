<?php

namespace backend\modules\member\forms;

use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use common\enums\AppEnum;
use common\models\member\CreditsLog;
use common\models\forms\CreditsLogForm;

/**
 * Class RechargeForm
 * @package backend\modules\member\forms
 * @author 原创脉冲 <QQ：2790684490>
 */
class RechargeForm extends Model
{
    const TYPE_MONEY = 'Money'; // 余额
    const TYPE_INT = 'Int'; // 积分

    // 1增加 2 减少
    const CHANGE_INCR = 1;
    const CHANGE_DECR = 2;

    public $old_num = '';
    public $change = self::CHANGE_INCR;
    public $money;
    public $int;
    public $remark;
    public $type;

    protected $sercive;

    /**
     * @var array
     */
    public static $changeExplain = [
        self::CHANGE_INCR => '增加',
        self::CHANGE_DECR => '减少',
    ];

    public function rules()
    {
        return [
            [['change'], 'integer'],
            [['money'], 'number', 'min' => 0.01],
            [['int'], 'integer', 'min' => 1],
            [['remark', 'type'], 'string'],
            [['type'], 'verifyEmpty'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'old_num' => '当前',
            'change' => '变更',
            'money' => '数量',
            'int' => '数量',
            'remark' => '备注',
        ];
    }

    public function verifyEmpty()
    {
        if ($this->type == self::TYPE_MONEY && !$this->money) {
            $this->addError('money', '数量不能为空');
        }

        if ($this->type == self::TYPE_INT && !$this->int) {
            $this->addError('int', '数量不能为空');
        }
    }

    /**
     * @param $member
     * @param int $change_type 类型
     * @param string $credit_group 操作端
     * @param string $credit_group_detail 操作人员
     * @return bool
     * @throws \yii\db\Exception
     */

    /**
     * @param $member
     * @param int $change_type 类型
     * @param string $credit_group 操作端
     * @param string $credit_group_detail 操作人员
     * @param int $increase 1累加累加金额,0不累加
     * @param int $map_id 关联ID
     * @return bool
     * @throws \yii\db\Exception
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function save($member, $change_type = 2, $credit_group = AppEnum::BACKEND, $credit_group_detail = CreditsLog::CREDIT_GROUP_MANAGER, $map_id = 0, $increase = 1)
    {
        $action = 'decr' . $this->type;
        if ($this->change == self::CHANGE_INCR) {
            $action = 'incr' . $this->type;
        }

        $num = $this->money;
        if ($this->type == self::TYPE_INT) {
            $num = $this->int;
        }

        // 写入当前会员
        $transaction = Yii::$app->db->beginTransaction();
        try {
            Yii::$app->services->member->set($member);

            // 变动积分/余额
            $model = Yii::$app->services->memberCreditsLog->$action(new CreditsLogForm([
                'member' => Yii::$app->services->member->get($member->id),
                'num' => $num,
                'credit_group' => $credit_group,
                'credit_group_detail' => $credit_group_detail,
                'remark' => !empty($this->remark) ? $this->remark : '【后台】管理员操作',
                'type' => $change_type,
                'map_id' => $map_id,
                'increase' => $increase,
            ]));
            $transaction->commit();
            return $model;
        } catch (NotFoundHttpException $e) {
            $transaction->rollBack();
            $this->addError('remark', $e->getMessage());
            return false;
        }

        //return true;
    }
}