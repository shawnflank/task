<?php

namespace backend\modules\member\controllers;

use common\helpers\ResultDataHelper;
use common\models\member\BindingAccount;
use common\models\member\Member;
use common\models\sys\Manager;
use Yii;
use common\models\member\QualificationAuditLists;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;
use yii\web\Response;

/**
 * QualificationAuditLists
 *
 * Class QualificationAuditListsController
 * @package backend\modules\member\controllers
 */
class QualificationAuditListsController extends BaseController
{
    use Curd;

    /**
     * @var QualificationAuditLists
     */
    public $modelClass = QualificationAuditLists::class;


    /**
     *  首页
     * @return array|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');//获取ID
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = QualificationAuditLists::findOne($id);
            $output = '';
            $message = '';
            //由于传递的数据是二维数组，将其转为一维
            $attribute = Yii::$app->request->post('editableAttribute');//获取名称
            $posted = current(Yii::$app->request->post('QualificationAuditLists'));
            if ($attribute == 'notice') {
                $model = Member::findOne( $model->member_id);
                $model->notice = $posted['notice'];
                $model->save();
            }
            $post = ['QualificationAuditLists' => $posted];
            if ($model->load($post) && $model->save()) {
                isset($posted['verification_text_data']) && $output = $model->verification_text_data;
            } else {
                //由于本插件不会自动捕捉model的error，所以需要放在$message中展示出来
                $message = $model->getFirstError($attribute);
            }
            return ['output' => $output, 'message' => $message];
        } else {
            $searchModel = new SearchModel([
                'model' => $this->modelClass,
                'scenario' => 'default',
                'partialMatchAttributes' => [], // 模糊查询
                'defaultOrder' => [
                    'status' => SORT_ASC,
                ],
                'pageSize' => $this->pageSize
            ]);
            $dataProvider = $searchModel
                ->search(Yii::$app->request->queryParams);
            $dataProvider->query
                ->with([
                    'member',
                    'wechatNumber',
                ]);
            $managerArray = \yii\helpers\ArrayHelper::map(Manager::find()->select(['id', 'username'])->asArray()->all(), 'id', 'username');
            $reviewsWechat = \yii\helpers\ArrayHelper::map(\common\models\common\ReviewsWechat::find()->asArray()->all(), 'id', 'wechat_number');
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'managerArray' => $managerArray,
                'reviewsWechat' => $reviewsWechat,
            ]);
        }
    }

    /**
     * 资质审核
     * @param $id
     * @param $status
     * @return mixed
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionUpdateStatus($id, $status)
    {
        $model = $this->findModel($id);
        if ($model->status != 0) {
            return $this->message("该条记录已被操作！", $this->redirect(Yii::$app->request->referrer), 'error');
        }

        $model->status = $status;
        $model->save(false);
        return $this->message("审核成功！", $this->redirect(Yii::$app->request->referrer));
    }

    /**
     * 批量审核
     * @return array
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionPassAll()
    {
        $ids = explode(',', Yii::$app->request->get('ids'));
        if (!empty($ids)) {
            // 先判断所选订单内是否已经被别人处理过
            foreach ($ids as $v1) {
                $model = $this->findModel($v1);
                if ($model->status != 0) {
                    return $this->message('所选项包含已审核内容', $this->redirect(Yii::$app->request->referrer), 'error');
                }
            }
            //再进行操作
            foreach ($ids as $v1) {
                $model = $this->findModel($v1);
                $model->status = 1;
                $model->save(false);
            }
            return $this->message("审核成功！", $this->redirect(Yii::$app->request->referrer));
        } else {
            return $this->message("请选择订单", $this->redirect(Yii::$app->request->referrer), 'error');
        }
    }


    public function actionView($memberId)
    {
        $model = QualificationAuditLists::find()
            ->where(['member_id' => $memberId, 'status' => 1])
            ->with('member')
            ->one();
        return $this->renderAjax($this->action->id, [
            'model' => $model,
        ]);
    }


}
