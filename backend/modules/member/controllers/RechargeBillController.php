<?php

namespace backend\modules\member\controllers;

use backend\modules\member\forms\RechargeForm;
use common\enums\AppEnum;
use common\models\member\CreditsLog;
use common\models\member\Member;
use Yii;
use common\models\member\RechargeBill;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;

/**
* RechargeBill
*
* Class RechargeBillController
* @package backend\modules\member\controllers
*/
class RechargeBillController extends BaseController
{
    use Curd;

    /**
    * @var RechargeBill
    */
    public $modelClass = RechargeBill::class;


    /**
    * 首页
    *
    * @return string
    * @throws \yii\web\NotFoundHttpException
    */
    public function actionIndex()
    {
        $searchModel = new SearchModel([
            'model' => $this->modelClass,
            'scenario' => 'default',
            'partialMatchAttributes' => [], // 模糊查询
            'defaultOrder' => [
                'id' => SORT_DESC
            ],
            'pageSize' => $this->pageSize
        ]);

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->with('member');
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


    /**
     * 审核提现订单
     * @param $id
     * @param $status
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionUpdateStatus($id, $status)
    {
        $model = $this->findModel($id);
        if ($model->status != 0) {
            return $this->message("该条记录已被操作！", $this->redirect(['index']), 'error');
        }
        if($status==1){
            $remark = "充值申请已通过，当前余额增加" . $model->recharge_money . "元";
            $type = RechargeForm::TYPE_MONEY;
            $rechargeForm = new RechargeForm([
                'type' => $type,
                'money' => $model->recharge_money,
                'int' => 0,
                'remark' => $remark,
            ]);
            $rechargeForm->save(Member::findOne($model->member_id), CreditsLog::RECHARGE_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER);
        }
        $model->status = $status;
        $model->save(false);
        return $this->message("操作成功！", $this->redirect(['index']));
    }
}
