<?php

namespace backend\modules\member\controllers;

use common\helpers\ResultDataHelper;
use Yii;
use common\models\member\BindingAccount;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;

/**
 * BindingAccount
 *
 * Class BindingAccountController
 * @package backend\modules\member\controllers
 */
class BindingAccountController extends BaseController
{
    use Curd;

    /**
     * @var BindingAccount
     */
    public $modelClass = BindingAccount::class;


    /**
     * 首页
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $searchModel = new SearchModel([
            'model' => $this->modelClass,
            'scenario' => 'default',
            'partialMatchAttributes' => [], // 模糊查询
            'defaultOrder' => [
                'id' => SORT_DESC
            ],
            'pageSize' => $this->pageSize
        ]);

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->with('member');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * ajax编辑/创建
     *
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\ExitException
     */
    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        $type = Yii::$app->request->get('type');
        $withdraw_money = Yii::$app->request->get('withdraw_money');
        if (empty($id) || empty($type)) {
            return ResultDataHelper::json(ResultDataHelper::ERROR_CODE, '目前处于调试模式');
        }
        $model = BindingAccount::find()->where(['member_id' => $id])->with('member')->one();
        return $this->renderAjax($this->action->id, [
            'model' => $model,
            'type' => $type,
            'withdraw_money' => $withdraw_money,
        ]);
    }


}
