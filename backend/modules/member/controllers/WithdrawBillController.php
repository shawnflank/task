<?php

namespace backend\modules\member\controllers;

use backend\modules\member\forms\RechargeForm;
use common\enums\AppEnum;
use common\models\member\BindingAccount;
use common\helpers\ResultDataHelper;
use common\helpers\UnitTurnHelper;
use common\helpers\CommonPluginHelper;
use common\models\common\Statistics;
use common\models\member\Account;
use common\models\member\CreditsLog;
use common\models\member\Member;
use common\models\sys\Manager;
use Yii;
use common\models\member\WithdrawBill;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;

/**
 * WithdrawBill
 *
 * Class WithdrawBillController
 * @package backend\modules\member\controllers
 */
class WithdrawBillController extends BaseController
{
    use Curd;

    /**
     * @var WithdrawBill
     */
    public $modelClass = WithdrawBill::class;


    /**
     * 首页
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $searchModel = new SearchModel([
            'model' => $this->modelClass,
            'scenario' => 'default',
            'partialMatchAttributes' => [], // 模糊查询
            'defaultOrder' => [
                'status' => SORT_ASC,
                'created_at' => SORT_DESC
            ],
            'pageSize' => $this->pageSize
        ]);

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->with('bindingAccount')
            ->with('member');
        $managerArray = \yii\helpers\ArrayHelper::map(Manager::find()->select(['id','username'])->asArray()->all(), 'id', 'username');
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'managerArray' => $managerArray,
        ]);
    }

    /**
     * 审核提现订单
     * @param $id
     * @param $status
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionUpdateStatus($id, $status)
    {
        // 开启事务
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $withdraw_bill_sql = "select * from task_withdraw_bill where `id`={$id} and `status` = 0 for update";
            $withdraw_bill_lock = Yii::$app->db->createCommand($withdraw_bill_sql)->queryOne();
            $model = WithdrawBill::find()
                ->where(['id' => $withdraw_bill_lock['id'], 'status' => 0])
                ->with('member')
                ->with('account')
                ->one();
            if (empty($model)) {
                return $this->message("该条记录已被操作！", $this->redirect(Yii::$app->request->referrer), 'error');
            }
            switch ($status) {
                case 1: // 1成功
                    $account_model = $model->account;
                  
                    $result = $this->wallet_cash($model->member_id,$model->sn,$model->withdraw_money,$model);
                    if( $result===true){
                        if (intval(Yii::$app->debris->config('is_change_integral') != 0)) {
                            // 判断提现金额百分比转换积分
                            $amout_scale_change_integral = Yii::$app->debris->config('amout_scale_change_integral');
                            $conversion_ratio = Yii::$app->debris->config('conversion_ratio');
                            $updateWithdrawMoney = 0;
                            if ($amout_scale_change_integral > 0) {
                                $percentage = UnitTurnHelper::h2l($amout_scale_change_integral);
                                $change_withdraw_money = $model->withdraw_money * $percentage;
                                if ($conversion_ratio && strpos($conversion_ratio, ':') !== false) {
                                    $res = explode(':', $conversion_ratio);
                                    $change_integral = $change_withdraw_money / $res[0] * $res[1];
                                    $new_withdraw_money = $model->withdraw_money - $change_withdraw_money;
                                    $account_model->withdraw_money = $new_withdraw_money + $account_model->withdraw_money;
                                    $account_model->user_integral = $change_integral + $account_model->user_integral;
                                    $updateWithdrawMoney = $new_withdraw_money;
    
                                    $remark = "提现申请已成功，其中{$change_withdraw_money}元按照比例 $res[0]:$res[1] 已转换成{$change_integral}点积分" ;
                                    $type = RechargeForm::TYPE_INT;
                                    $rechargeForm = new RechargeForm([
                                        'type' => $type,
                                        'money' => 0,
                                        'int' => $change_integral,
                                        'remark' => $remark,
                                    ]);
                                    $rechargeForm->save($model->member, CreditsLog::WITHDRAW_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, 0, 0);
                                }
                            }
                        } else {
                            $account_model->withdraw_money = $account_model->withdraw_money + $model->withdraw_money;
                            $updateWithdrawMoney = $model->withdraw_money;
                        }
                        $account_model->save(false);
                        // 加入统计
                        Statistics::updateWithdrawMoney(date("Y-m-d"), $updateWithdrawMoney);
                    }else{
                        $account_model = $model->account;
                        $account_model->withdraw_money = $account_model->withdraw_money + $model->withdraw_money;
                        $updateWithdrawMoney = $model->withdraw_money;
                        $status = 4; 
                        $account_model->save(false);
                    }
                    break;
                    case 2: // 2拒绝
                    $remark = "提现申请已被拒绝，当前余额增加" . $model->withdraw_money . "元";
                    $type = RechargeForm::TYPE_MONEY;
                    $rechargeForm = new RechargeForm([
                        'type' => $type,
                        'money' => $model->withdraw_money,
                        'int' => 0,
                        'remark' => $remark,
                    ]);
                    $rechargeForm->save($model->member, CreditsLog::WITHDRAW_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, 0, 0);
                    break;
                case 3: // 取消
                    break;
                default:
                    break;
            }
            $model->status = $status;
            $model->save(false);
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            return $this->message("审核失败", $this->redirect(Yii::$app->request->referrer), 'error');
        }
        return $this->message("审核成功！", $this->redirect(Yii::$app->request->referrer));
    }


    /**
     * 批量通过提现审核
     * @return array
     * @throws \yii\db\Exception
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionPassAll()
    {
        $ids = explode(',', Yii::$app->request->get('ids'));
        if (!empty($ids)) {
            // 先判断所选订单内是否已经被别人处理过
            foreach ($ids as $v1) {
                if (empty(WithdrawBill::find()->where(['id' => $v1, 'status' => 0])->exists())) {
                    return $this->message('所选项包含已审核内容', $this->redirect(Yii::$app->request->referrer), 'error');
                }
            }
            //再进行操作
            foreach ($ids as $v1) {
                // 开启事务
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $withdraw_bill_sql = "select * from task_withdraw_bill where `id`={$v1} and `status` = 0 for update";
                    $withdraw_bill_lock = Yii::$app->db->createCommand($withdraw_bill_sql)->queryOne();
                    $model = WithdrawBill::find()
                        ->where(['id' => $withdraw_bill_lock['id'], 'status' => 0])
                        ->with('account')
                        ->one();
                    $account_model = $model->account;
                    if (intval(Yii::$app->debris->config('is_change_integral') != 0)) {
                        // 判断提现金额百分比转换积分
                        $amout_scale_change_integral = Yii::$app->debris->config('amout_scale_change_integral');
                        $conversion_ratio = Yii::$app->debris->config('conversion_ratio');
                        $updateWithdrawMoney = 0;
                        if ($amout_scale_change_integral > 0) {
                            $percentage = UnitTurnHelper::h2l($amout_scale_change_integral);
                            $change_withdraw_money = $model->withdraw_money * $percentage;
                            if ($conversion_ratio && strpos($conversion_ratio, ':') !== false) {
                                $res = explode(':', $conversion_ratio);
                                $change_integral = $change_withdraw_money / $res[0] * $res[1];
                                $new_withdraw_money = $model->withdraw_money - $change_withdraw_money;
                                $account_model->withdraw_money = $new_withdraw_money + $account_model->withdraw_money;
                                $account_model->user_integral = $change_integral + $account_model->user_integral;
                                $updateWithdrawMoney = $new_withdraw_money;

                                $remark = "提现申请已成功，其中{$change_withdraw_money}元按照比例 $res[0]:$res[1] 已转换成{$change_integral}点积分" ;
                                $type = RechargeForm::TYPE_INT;
                                $rechargeForm = new RechargeForm([
                                    'type' => $type,
                                    'money' => 0,
                                    'int' => $change_integral,
                                    'remark' => $remark,
                                ]);
                                $rechargeForm->save($model->member, CreditsLog::WITHDRAW_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, 0, 0);
                            }
                        }
                    } else {
                        $account_model->withdraw_money = $account_model->withdraw_money + $model->withdraw_money;
                        $updateWithdrawMoney = $model->withdraw_money;
                    }
                    $model->status = 1;
                    $account_model->save(false);
                    $model->save(false);
                    // 加入统计
                    Statistics::updateWithdrawMoney(date("Y-m-d"), $updateWithdrawMoney);
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    return $this->message("审核失败", $this->redirect(Yii::$app->request->referrer), 'error');
                }
            }
            return $this->message("审核成功！", $this->redirect(Yii::$app->request->referrer));
        } else {
            return $this->message("请选择订单", $this->redirect(Yii::$app->request->referrer), 'error');
        }
    }

    /**
     *  批量驳回
     * @return mixed
     */
    public function actionRejectAll()
    {
        $ids = explode(',', Yii::$app->request->get('ids'));
        if (!empty($ids)) {
            // 先判断所选订单内是否已经被别人处理过
            foreach ($ids as $v1) {
                if (empty(WithdrawBill::find()->where(['id' => $v1, 'status' => 0])->exists())) {
                    return $this->message('所选项包含已审核内容', $this->redirect(Yii::$app->request->referrer), 'error');
                }
            }
            //再进行操作
            foreach ($ids as $v1) {
                // 开启事务
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $withdraw_bill_sql = "select * from task_withdraw_bill where `id`={$v1} and `status` = 0 for update";
                    $withdraw_bill_lock = Yii::$app->db->createCommand($withdraw_bill_sql)->queryOne();
                    $model = WithdrawBill::find()
                        ->where(['id' => $withdraw_bill_lock['id'], 'status' => 0])
                        ->with('account')
                        ->one();
                    $remark = "提现申请已被拒绝，当前余额增加" . $model->withdraw_money . "元";
                    $type = RechargeForm::TYPE_MONEY;
                    $rechargeForm = new RechargeForm([
                        'type' => $type,
                        'money' => $model->withdraw_money,
                        'int' => 0,
                        'remark' => $remark,
                    ]);
                    $model->status = 2;
                    $model->save(false);
                    $rechargeForm->save($model->member, CreditsLog::WITHDRAW_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, 0, 0);
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    return $this->message("审核失败", $this->redirect(Yii::$app->request->referrer), 'error');
                }
            }
            return $this->message("审核成功！", $this->redirect(Yii::$app->request->referrer));
        } else {
            return $this->message("请选择订单", $this->redirect(Yii::$app->request->referrer), 'error');
        }
    }
    public function wallet_cash($user_id,$sn,$withdraw_money,$model){
        $config = require_once __DIR__."/Wechat/config.php";
        $appId=$config['appId'];
        $mchId =$config['mchId'];
        $key = $config['key'];
       
        //$openId='oCYlXv80TOjK9mkVYJk602KBvbe8';
        $openId= BindingAccount::findOne(['member_id'=>$user_id])->wechat_account;
        $url="https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers"; 
        $data=[
           "mch_appid"=>$appId,              //商户账号appid
           "mchid"=>$mchId,        //商户号
           "nonce_str"=>Yii::$app->getSecurity()->generateRandomString(10),    //随机字符串
           "partner_trade_no"=>$sn,   //商户订单号
           "openid"=>$openId,        //用户openid
           "check_name"=>"NO_CHECK",   //固定值，强校验真实姓名
           "re_user_name"=>'测试',   //用户真实姓名，必须与微信实名相同
           "amount"=>$withdraw_money*100,        //企业付款金额，单位为分
           "desc"=>"快赚猫提现",
           "spbill_create_ip"=>"127.0.0.1",   //该IP可传用户端或者服务端的IP
        ];
        $data["sign"] = CommonPluginHelper::getMD5sign($data, $key);
        $reqXml = CommonPluginHelper::arrayToXml($data);
        
        $sslCert = __DIR__."/Wechat/apiclient_cert.pem";
        $sslKey = __DIR__."/Wechat/apiclient_key.pem";
        $resp = CommonPluginHelper::curl_post_ssl($url, $reqXml, 30, [], $sslCert, $sslKey); 
        
        //记录支付请求响应日志
        $backarr = simplexml_load_string($resp['data'], 'SimpleXMLElement', LIBXML_NOCDATA);
        
        if ($backarr&&$backarr->return_code=='SUCCESS') {//通讯成功
            
            if ($backarr->result_code=='SUCCESS') {//提现成功
                return true;
            } else {//提现失败
                $remark = "微信提现失败，当前余额增加" . $model->withdraw_money . "元";
                $type = RechargeForm::TYPE_MONEY;
                $rechargeForm = new RechargeForm([
                    'type' => $type,
                    'money' => $model->withdraw_money,
                    'int' => 0,
                    'remark' => $remark,
                ]);
                $rechargeForm->save($model->member, CreditsLog::WITHDRAW_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, 0, 0);       
            }
        }else{
            $remark = "微信提现失败，当前余额增加" . $model->withdraw_money . "元";
            $type = RechargeForm::TYPE_MONEY;
            $rechargeForm = new RechargeForm([
                'type' => $type,
                'money' => $model->withdraw_money,
                'int' => 0,
                'remark' => $remark,
            ]);
            $rechargeForm->save($model->member, CreditsLog::WITHDRAW_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, 0, 0);
        } 
    }

    /**
     *  支付导出
     * @param $ids
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionPayExport($ids)
    {
        $ids = explode(',', $ids);
        $searchModel = new SearchModel([
            'model' => $this->modelClass,
            'scenario' => 'default',
            'partialMatchAttributes' => [], // 模糊查询
            'defaultOrder' => [
                'status' => SORT_ASC,
                'created_at' => SORT_DESC
            ],
            'pageSize' => count($ids)
        ]);

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->with('bindingAccount')
            ->where(['in', 'id', $ids])
            ->with('member');
        return $this->render('pay-export', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}
