<?php

namespace backend\modules\member\controllers;

use backend\controllers\BaseController;
use common\helpers\ArrayHelper;
use Yii;
use common\models\base\SearchModel;
use common\components\Curd;
use common\models\member\Member;
use common\enums\StatusEnum;
use backend\modules\member\forms\RechargeForm;
use yii\helpers\Json;
use yii\web\Response;

/**
 * 会员管理
 *
 * Class MemberController
 * @package backend\modules\member\controllers
 * @author 原创脉冲 <QQ：2790684490>
 */
class MemberController extends BaseController
{
    use Curd;

    /**
     * @var \yii\db\ActiveRecord
     */
    public $modelClass = Member::class;

    /**
     * 首页
     * @return array|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');//获取ID
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = Member::findOne($id);
            $model->scenario = 'backendIndex';
            $output = '';
            $message = '';
            //由于传递的数据是二维数组，将其转为一维
            $attribute = Yii::$app->request->post('editableAttribute');//获取名称
            $posted = current(Yii::$app->request->post('Member'));
            $post = ['Member' => $posted];
            if ($model->load($post) && $model->save()) {
                $output = $model->$attribute;
                if ($attribute == 'notice') {
                    if(!empty($model->$attribute)&&mb_strlen($model->$attribute) > 5){
                        $output = mb_substr($model->$attribute, 0, 5, 'utf-8') . "..";
                    }
                }
                if ($attribute == 'w_id') {
                    $datas = \yii\helpers\ArrayHelper::map(\common\models\common\ReviewsWechat::find()->asArray()->all(), 'id', 'wechat_number');
                    $output = $datas[$model->$attribute];
                }
            } else {
                //由于本插件不会自动捕捉model的error，所以需要放在$message中展示出来
                $message = $model->getFirstError($attribute);
            }
            return ['output' => $output, 'message' => $message];
        } else {
            $searchModel = new SearchModel([
                'model' => $this->modelClass,
                'scenario' => 'default',
                'partialMatchAttributes' => ['realname', 'username'], // 模糊查询
                'defaultOrder' => [
                    'id' => SORT_DESC
                ],
                'pageSize' => $this->pageSize
            ]);
            $dataProvider = $searchModel
                ->search(Yii::$app->request->queryParams);
            $dataProvider->query
                ->andFilterWhere(['merchant_id' => $this->getMerchantId()])
                ->andWhere(['>=', 'status', StatusEnum::DISABLED])
                ->with('account')
                ->with('recommendMember');
            $reviewsWechat = \yii\helpers\ArrayHelper::map(\common\models\common\ReviewsWechat::find()->asArray()->all(), 'id', 'wechat_number');
            return $this->render($this->action->id, [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'reviewsWechat' => $reviewsWechat,
            ]);
        }
    }


    /**
     * 编辑/创建
     *
     * @return mixed
     */
    public function actionEdit()
    {
        $id = Yii::$app->request->get('id', null);
        $model = $this->modelClass::find()->where(['id' => $id])->with('bindingAccount')->one();
        if ($model->load(Yii::$app->request->post()) && $model->bindingAccount->load(Yii::$app->request->post())) {
            //如果传递过来的密码为空,则不更新密码
            if (!empty($model->bindingAccount->safety_password)) {
                $model->bindingAccount->setPassword($model->bindingAccount->safety_password);
            }
            return $model->save() && $model->bindingAccount->save()
                ? $this->message("编辑成功", $this->redirect(['index']))
                : $this->message($this->getError($model), $this->redirect(['index']), 'error');

        }

        return $this->render($this->action->id, [
            'model' => $model,
        ]);
    }


    /**
     * 编辑/创建
     *
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAjaxEdit()
    {
        $id = Yii::$app->request->get('id');
        $model = $this->findModel($id);
        $model->scenario = 'backendCreate';
        $modelInfo = clone $model;

        // ajax 校验
        $this->activeFormValidate($model);
        if ($model->load(Yii::$app->request->post())) {
            // 验证密码
            if ($modelInfo['password_hash'] != $model->password_hash) {
                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
            }
            $model->merchant_id = Yii::$app->services->merchant->getId();
            return $model->save()
                ? $this->message("编辑成功", $this->redirect(['index']))
                : $this->message($this->getError($model), $this->redirect(['index']), 'error');
        }

        return $this->renderAjax($this->action->id, [
            'model' => $model,
        ]);
    }

    /**
     * 积分/余额变更
     *
     * @param $id
     * @return mixed|string
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionRecharge($id)
    {
        $rechargeForm = new RechargeForm();
        $member = $this->findModel($id);

        // ajax 校验
        $this->activeFormValidate($rechargeForm);
        if ($rechargeForm->load(Yii::$app->request->post())) {
            return $rechargeForm->save($member)
                ? $this->message("充值成功", $this->redirect(['index']))
                : $this->message($this->getError($rechargeForm), $this->redirect(['index']), 'error');
        }

        return $this->renderAjax($this->action->id, [
            'model' => $member,
            'rechargeForm' => $rechargeForm,
        ]);
    }

    /**
     *  代理关系
     * @param $id
     * @return string
     */
    public function actionRecommendRelations($id)
    {
        $member = Member::find()->with(['account' => function ($query) {
            return $query->select('user_money');
        }])->select('id,username as title,recommend_id as pid')->where(['id' => $id])->asArray()->one();
        $member['title'] .= '(' . $member['account']['user_money'] . ')';
        $parents = Member::getParents($member);
        $children = Member::find()->select('id,username as title,recommend_id as pid')->where(['recommend_id' => $id])->asArray()->all();
        $children[] = $member;
        return $this->renderAjax('recommend-relations', [
            'parents' => $parents,
            'children' => $children,
            'cid' => $id
        ]);
    }

    /**
     *  获取根节点
     * @param $id
     * @return string
     */
    public function actionGetRoot($id)
    {
        $member = Member::find()
            ->with(['account' => function ($query) {
                return $query->select('user_money');
            }])
            ->select('id,username as text,recommend_id as parent')
            ->where(['id' => $id])
            ->asArray()
            ->one();
        $member['text'] .= '(' . $member['account']['user_money'] . ')';
        $member['parent'] = '#';
        $member['children'] = true;
        return Json::encode($member);
    }

    /**
     *  获取下级
     * @param $id
     */
    public function actionGetChildren($id)
    {
        $children = Member::find()->with(['account' => function ($query) {
            return $query->select('member_id,user_money');
        }])->select('id,username as text,recommend_id as parent')->where(['recommend_id' => $id])->asArray()->all();
        foreach ($children as $k => $v) {
            $children[$k]['children'] = true;
            $children[$k]['text'] .= '(' . $children[$k]['account']['user_money'] . ')';
        }
        return Json::encode($children);
    }
}