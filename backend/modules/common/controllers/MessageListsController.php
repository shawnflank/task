<?php

namespace backend\modules\common\controllers;

use Yii;
use common\models\common\MessageLists;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;
use GatewayWorker\Lib\Gateway;
use common\helpers\ResultDataHelper;
/**
* MessageLists
*
* Class MessageListsController
* @package backend\modules\common\controllers
*/
class MessageListsController extends BaseController
{
    use Curd;

    /**
    * @var MessageLists
    */
    public $modelClass = MessageLists::class;


    /**
    * 首页
    *
    * @return string
    * @throws \yii\web\NotFoundHttpException
    */
    public function actionIndex()
    {
        $searchModel = new SearchModel([
            'model' => $this->modelClass,
            'scenario' => 'default',
            'partialMatchAttributes' => [], // 模糊查询
            'defaultOrder' => [
                'id' => SORT_DESC
            ],
            'pageSize' => $this->pageSize
        ]);

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * 绑定client_id
     * @return mixed
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionBinding()
    {
        $client_id = Yii::$app->request->post('client_id');
        if (empty($client_id)) {
            return ResultDataHelper::json(ResultDataHelper::ERROR_CODE, '目前处于调试模式');
        }
        $adminId = Yii::$app->user->identity['id'];
        if (empty($adminId)) {
            return ResultDataHelper::json(ResultDataHelper::ERROR_CODE, '暂未登录');
        }
        Yii::$app->services->webSocket->addClientIdToGroup($client_id,'admin');
        return ResultDataHelper::json(ResultDataHelper::SUCCESS_CODE, '绑定成功');
    }

    /**
     * 查看消息
     * @param $id
     * @return array
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        if (!empty($id)) {
            $model = MessageLists::find()->where(['id' => $id, 'status' => 0])->one();
            if (!empty($model)) {
                $model->status = 1;
                $model->save(false);
                return ResultDataHelper::json(ResultDataHelper::SUCCESS_CODE, '', $model);
            } else {
                return ResultDataHelper::json(ResultDataHelper::ERROR_CODE, '该消息已被查看');
            }
        } else {
            return ResultDataHelper::json(ResultDataHelper::ERROR_CODE, '目前处于调试模式');
        }
    }
}
