<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace backend\modules\common\controllers;


use backend\controllers\BaseController;
use common\helpers\ArrayHelper;
use common\models\common\Statistics;
use Yii;

class HomeController extends BaseController
{
    /**
     * 默认数据
     *
     * @var array
     */
    public $attention = [
        'register_member' => 0,
        'sign_member' => 0,
        'over_task' => 0,
        'withdraw_money' => 0,
        'get_task_member' => 0,
        'over_task_member' => 0
    ];
    /**
     * 统计
     *
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $from_date = $request->get('from_date', date('Y-m-d', strtotime("-6 day")));
        $to_date = $request->get('to_date', date('Y-m-d'));

        $models = Statistics::findBetweenByCreatedAt($from_date, $to_date);
        $stat = ArrayHelper::arrayKey($models, 'date');
        $fansStat = [];
        for ($i = strtotime($from_date); $i <= strtotime($to_date); $i += 86400) {
            $day = date('Y-m-d', $i);
            if (isset($stat[$day])) {
                $fansStat['register_member'][] = $stat[$day]['register_member'];
                $fansStat['sign_member'][] = $stat[$day]['sign_member'];
                $fansStat['over_task'][] = $stat[$day]['over_task'];
                $fansStat['withdraw_money'][] = $stat[$day]['withdraw_money'];
                $fansStat['get_task_member'][] = $stat[$day]['get_task_member'];
                $fansStat['over_task_member'][] = $stat[$day]['over_task_member'];
            } else {
                $fansStat['register_member'][] = 0;
                $fansStat['sign_member'][] = 0;
                $fansStat['over_task'][] = 0;
                $fansStat['withdraw_money'][] = 0;
                $fansStat['get_task_member'][] = 0;
                $fansStat['over_task_member'][] = 0;
            }

            $fansStat['chartTime'][] = $day;
        }

        // 所有日期累积统计
        $all_day = $this->attention;
        if ($allDayModel = Statistics::findByDateAll()) {
            $all_day = ArrayHelper::merge($this->attention, $allDayModel);
        }

        // 今日统计
        $today = $this->attention;
            if ($todayModel = Statistics::findByDate(date('Y-m-d'))) {
            $today = ArrayHelper::merge($this->attention, $todayModel);
        }

        return $this->render('index', [
            'models' => $models,
            'all_day' => $all_day,
            'today' => $today,
            'fansStat' => $fansStat,
            'from_date' => $from_date,
            'to_date' => $to_date,
        ]);
    }



}