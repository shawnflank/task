<?php

use common\helpers\Url;
use common\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

$addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;

$this->title = '统计';
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Html::jsFile('@web/resources/plugins/echarts/echarts-all.js') ?>

<div class="row">
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="ion ion-stats-bars green"></i> <?= $today['register_member']; ?></span>
                <span class="info-box-text">今日注册数(人)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-thumbsup green"></i> <?= $today['sign_member']; ?></span>
                <span class="info-box-text">今日签到数(人)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-arrow-graph-up-right green"></i> <?= $today['over_task']; ?></span>
                <span class="info-box-text">今日完成任务数(次)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-cash red"></i> <?= $today['withdraw_money']; ?></span>
                <span class="info-box-text">今日提现金额(元)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-android-contact green"></i> <?= $today['get_task_member']; ?></span>
                <span class="info-box-text">今日领取任务人数(个)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-android-checkbox-outline green"></i> <?= $today['over_task_member']; ?></span>
                <span class="info-box-text">今日完成任务人数(个)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
</div>

<div class="row">
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="ion ion-stats-bars green"></i> <?= $all_day['register_member']??0; ?></span>
                <span class="info-box-text">累积注册数(人)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-thumbsup green"></i> <?= $all_day['sign_member']??0; ?></span>
                <span class="info-box-text">累积签到数(人)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-arrow-graph-up-right green"></i> <?= $all_day['over_task']??0; ?></span>
                <span class="info-box-text">累积完成任务数(次)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-cash red"></i> <?= $all_day['withdraw_money']??0; ?></span>
                <span class="info-box-text">累积提现金额(元)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-android-contact green"></i> <?= $all_day['get_task_member']??0; ?></span>
                <span class="info-box-text">累积领取任务人数(个)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <div class="info-box">
            <div class="info-box-content p-md">
                <span class="info-box-number"><i
                            class="icon ion-android-checkbox-outline green"></i> <?= $all_day['over_task_member']??0; ?></span>
                <span class="info-box-text">累积完成人数(个)</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
</div>





<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= $this->title; ?></h3>
            </div>
            <div class="box-body table-responsive">
                <div class="col-sm-12 normalPaddingJustV">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to(['index']),
                        'method' => 'get'
                    ]); ?>
                    <div class="row">
                        <div class="col-sm-4 p-r-no-away">
                            <div class="input-group drp-container">
                                <?= DateRangePicker::widget([
                                    'name' => 'queryDate',
                                    'value' => $from_date . '-' . $to_date,
                                    'readonly' => 'readonly',
                                    'useWithAddon' => true,
                                    'convertFormat' => true,
                                    'startAttribute' => 'from_date',
                                    'endAttribute' => 'to_date',
                                    'startInputOptions' => ['value' => $from_date],
                                    'endInputOptions' => ['value' => $to_date],
                                    'pluginOptions' => [
                                        'locale' => ['format' => 'Y-m-d'],
                                    ]
                                ]) . $addon; ?>
                            </div>
                        </div>
                        <div class="col-sm-3 p-l-no-away">
                            <div class="input-group m-b">
                                <?= Html::tag('span',
                                    '<button class="btn btn-white"><i class="fa fa-search"></i> 搜索</button>',
                                    ['class' => 'input-group-btn']) ?>
                            </div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <table class="table">
                    <tr>
                        <td colspan="4">
                            <div id="main" style="width: 100%;height:400px;"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('main'));
    var register_member = <?= json_encode($fansStat['register_member'])?>;
    var sign_member = <?= json_encode($fansStat['sign_member'])?>;
    var over_task = <?= json_encode($fansStat['over_task'])?>;
    var get_task_member = <?= json_encode($fansStat['get_task_member'])?>;
    var over_task_member = <?= json_encode($fansStat['over_task_member'])?>;
    var withdraw_money = <?= json_encode($fansStat['withdraw_money'])?>;
    var chartTime = <?= json_encode($fansStat['chartTime'])?>;

    function chartOption() {
        var option = {
            title: {
                subtext: '单位'
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['注册数', '签到数', '完成任务数', '提现金额', '领取任务人数', '完成任务人数'],
                selected: {
                    '注册数': true,
                    '签到数': true,
                    '完成任务数': true,
                    '提现金额': false,
                    '领取任务人数': true,
                    '完成任务人数': true
                }
            },
            toolbox: {
                show: false,
                feature: {
                    mark: {show: true},
                    dataView: {show: true, readOnly: false},
                    magicType: {show: true, type: ['line', 'bar', 'stack', 'tiled']},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: chartTime
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: [
                {
                    name: '注册数',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: register_member
                },
                {
                    name: '签到数',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: sign_member
                },
                {
                    name: '完成任务数',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: over_task
                },
                {
                    name: '提现金额',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: withdraw_money
                },
                {
                    name: '领取任务人数',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: get_task_member
                },
                {
                    name: '完成任务人数',
                    type: 'line',
                    smooth: true,
                    itemStyle: {normal: {areaStyle: {type: 'default'}}},
                    data: over_task_member
                }
            ]
        };

        return option;
    }

    myChart.setOption(chartOption()); // 加载图表
</script>