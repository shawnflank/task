<?php

namespace backend\modules\task\controllers;

use backend\modules\task\forms\TaskForm;
use common\models\task\TaskCategory;
use Yii;
use common\models\task\Task;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;

/**
 * Task
 *
 * Class TaskController
 * @package backend\modules\task\controllers
 */
class TaskController extends BaseController
{

//    public function actions()
//    {
//        return [
//            'upload' => [
//                'class' => 'kucha\ueditor\UEditorAction',
//                'config' => [
////                    "imageUrlPrefix" => Yii::$app->request->getHostInfo(),//图片访问路径前缀
//                    "imageUrlPrefix" => "",//图片访问路径前缀
//                    "imagePathFormat" => "/uploads/image/{yyyy}{mm}{dd}/{time}{rand:6}" //上传保存路径
//                ],
//            ]
//        ];
//    }

    use Curd;

    /**
     * @var Task
     */
    public $modelClass = Task::class;


    /**
     * 首页
     * @return array|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');//获取ID
            Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
            $model = Task::findOne($id);
            $output = '';
            $message = '';
            //由于传递的数据是二维数组，将其转为一维
            $attribute = Yii::$app->request->post('editableAttribute');//获取名称
            $posted = current(Yii::$app->request->post('Task'));
            $post = ['Task' => $posted];
            if ($model->load($post) && $model->save(false)) {
                $output = $model->$attribute;
                if ($attribute == 'c_id') {
                    $datas = \yii\helpers\ArrayHelper::map(\common\models\task\TaskCategory::find()->asArray()->all(), 'id', 'title');
                    $output = $datas[$model->$attribute];
                }
                if ($attribute == 'title') {
                    $output = mb_strlen($model->$attribute) > 8 ? mb_substr($model->$attribute, 0, 8, 'utf-8') . ".." : $model->$attribute;
                }
            } else {
                //由于本插件不会自动捕捉model的error，所以需要放在$message中展示出来
                $message = $model->getFirstError($attribute);
            }
            return ['output' => $output, 'message' => $message];
        } else {
            $searchModel = new SearchModel([
                'model' => $this->modelClass,
                'scenario' => 'default',
                'partialMatchAttributes' => [], // 模糊查询
                'defaultOrder' => [
                    'sort' => SORT_DESC,
                    'created_at' => SORT_DESC,
                ],
                'pageSize' => $this->pageSize
            ]);

            $dataProvider = $searchModel
                ->search(Yii::$app->request->queryParams);
            $dataProvider->query
//                ->with('category')
                ->with('manager');
            $c_id = Yii::$app->request->get('c_id');
            if (!empty($c_id)) {
                $dataProvider->query->andWhere(['c_id' => $c_id]);
            }
            $taskCategory = \yii\helpers\ArrayHelper::map(TaskCategory::find()->asArray()->all(), 'id', 'title');
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'taskCategory' => $taskCategory,
            ]);
        }
    }

    /**
     * 第一步 创建
     *
     * @return mixed
     */
    public function actionAdd()
    {
        $model = new TaskForm();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                return $this->redirect(['edit', 'c_id' => $model->c_id]);
            } else {
                return $this->message("操作失败", $this->redirect(['index']), 'error');
            }
        }
        return $this->render($this->action->id, [
            'model' => $model,
        ]);
    }

    /**
     * 编辑
     * 第二步创建
     *
     * @return mixed
     */
    public function actionEdit()
    {
        $c_id = Yii::$app->request->get('c_id', null);
        $id = Yii::$app->request->get('id', null);
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if (empty(Yii::$app->request->post('Task')['release_body'])) {
                return $this->message("发布主体必须添加", $this->redirect(['index']), 'error');
            }
            if ($model->c_id == 1 || $model->c_id == 4) {
                $model->release_body = serialize($model->release_body);
            }
            $model->save();
            ///redis 写入 任务数量
            $redis = Yii::$app->redis;
            $task_key = 'task_number_' . $model->id;
            $redis->set($task_key, $model->task_number);
            return $this->message("操作成功", $this->redirect(['index']));
        }
        if (!empty($c_id)) {
            $model->c_id = $c_id;
        }
        if (!empty($model->release_body)) {
            if ($model->c_id == 1 || $model->c_id == 4) {
                $model->release_body = unserialize($model->release_body);
            }
        }
        return $this->render($this->action->id, [
            'model' => $model,
        ]);
    }
}
