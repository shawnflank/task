<?php

namespace backend\modules\task\controllers;

use Yii;
use common\models\task\TaskCategory;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;

/**
 * TaskCategory
 *
 * Class TaskCategoryController
 * @package backend\modules\task\controllers
 */
class TaskCategoryController extends BaseController
{
    use Curd;

    /**
     * @var TaskCategory
     */
    public $modelClass = TaskCategory::class;


    /**
     * 首页
     * @return array|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');//获取ID
            Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
            $model = TaskCategory::findOne($id);
            $output = '';
            $message = '';
            //由于传递的数据是二维数组，将其转为一维
            $attribute = Yii::$app->request->post('editableAttribute');//获取名称
            $posted = current(Yii::$app->request->post('TaskCategory'));
            $post = ['TaskCategory' => $posted];
            if ($model->load($post) && $model->save(false)) {
                // 将最新内容存入缓存
                $task_category = TaskCategory::getAllCategory();
                // 缓存 7天
                Yii::$app->cache->set('task_category',$task_category,60 * 60 * 24 * 7);

                $output = $model->$attribute;
            } else {
                //由于本插件不会自动捕捉model的error，所以需要放在$message中展示出来
                $message = $model->getFirstError($attribute);
            };
            return ['output' => $output, 'message' => $message];
        } else {
            $searchModel = new SearchModel([
                'model' => $this->modelClass,
                'scenario' => 'default',
                'partialMatchAttributes' => [], // 模糊查询
                'defaultOrder' => [
                    'sort' => SORT_DESC,
                    'created_at' => SORT_DESC,
                ],
                'pageSize' => $this->pageSize
            ]);

            $dataProvider = $searchModel
                ->search(Yii::$app->request->queryParams);

            $dataProvider->query
                ->with('manager');
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        }
    }

    /**
     * ajax编辑/创建
     *
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\ExitException
     */
    public function actionAjaxEdit()
    {
        $id = Yii::$app->request->get('id');
        $model = $this->findModel($id);

        // ajax 校验
        $this->activeFormValidate($model);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                // 将最新内容存入缓存
                $task_category = TaskCategory::getAllCategory();
                // 缓存 7天
                Yii::$app->cache->set('task_category',$task_category,60 * 60 * 24 * 7);

                return $this->message("操作成功", $this->redirect(['index']));
            } else {
                return $this->message($this->getError($model), $this->redirect(['index']), 'error');
            }
        }

        return $this->renderAjax($this->action->id, [
            'model' => $model,
        ]);
    }
}
