<?php

namespace backend\modules\task\controllers;

use Yii;
use common\models\task\GradeSetting;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;

/**
* GradeSetting
*
* Class GradeSettingController
* @package backend\modules\task\controllers
*/
class GradeSettingController extends BaseController
{
    use Curd;

    /**
    * @var GradeSetting
    */
    public $modelClass = GradeSetting::class;


    /**
    * 首页
    *
    * @return string
    * @throws \yii\web\NotFoundHttpException
    */
    public function actionIndex()
    {
        $searchModel = new SearchModel([
            'model' => $this->modelClass,
            'scenario' => 'default',
            'partialMatchAttributes' => [], // 模糊查询
            'defaultOrder' => [
                'id' => SORT_DESC
            ],
            'pageSize' => $this->pageSize
        ]);

        $dataProvider = $searchModel
            ->search(Yii::$app->request->queryParams);
        $dataProvider->query
            ->with('manager');
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}
