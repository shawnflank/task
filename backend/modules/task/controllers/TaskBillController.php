<?php

namespace backend\modules\task\controllers;

use backend\modules\member\forms\RechargeForm;
use common\enums\AppEnum;
use common\helpers\BcMathHelper;
use common\helpers\CommonPluginHelper;
use common\helpers\DateHelper;
use common\helpers\ResultDataHelper;
use common\models\common\Statistics;
use common\models\member\CreditsLog;
use common\models\member\Member;
use common\models\sys\Manager;
use common\models\task\GradeSetting;
use common\models\task\Task;
use Yii;
use common\models\task\TaskBill;
use common\components\Curd;
use common\models\base\SearchModel;
use backend\controllers\BaseController;
use yii\web\Controller;

/**
 * TaskBill
 *
 * Class TaskBillController
 * @package backend\modules\task\controllers
 */
class TaskBillController extends BaseController
{
    use Curd;

    /**
     * @var TaskBill
     */
    public $modelClass = TaskBill::class;


    /**
     * 首页
     * @return array|string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post('hasEditable')) {
            $id = Yii::$app->request->post('editableKey');//获取ID
            Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
            $model = TaskBill::findOne($id);
            $output = '';
            $message = '';
            //由于传递的数据是二维数组，将其转为一维
            $attribute = Yii::$app->request->post('editableAttribute');//获取名称
            $posted = current(Yii::$app->request->post('TaskBill'));
            $post = ['TaskBill' => $posted];
            if ($attribute == 'notice') {
                $model = Member::findOne( $model->member_id);
                $model->notice = $posted['notice'];
                $model->save();
            }
            if ($model->load($post) && $model->save(false)) {
                $output = $model->$attribute;
                if ($attribute == 'remark') {
                    if (!empty($model->$attribute) && mb_strlen($model->$attribute) > 5) {
                        $output = mb_substr($model->$attribute, 0, 5, 'utf-8') . "..";
                    }
                }
                if ($attribute == 'w_id') {
                    $datas = \yii\helpers\ArrayHelper::map(\common\models\common\ReviewsWechat::find()->asArray()->all(), 'id', 'wechat_number');
                    $output = $datas[$model->$attribute];
                }
            } else {
                //由于本插件不会自动捕捉model的error，所以需要放在$message中展示出来
                $message = $model->getFirstError($attribute);
            };
            return ['output' => $output, 'message' => $message];
        } else {
            $searchModel = new SearchModel([
                'model' => $this->modelClass,
                'scenario' => 'default',
                'partialMatchAttributes' => [], // 模糊查询
                'defaultOrder' => [
                    'updated_at' => SORT_DESC,
                ],
                'pageSize' => $this->pageSize
            ]);

            $dataProvider = $searchModel
                ->search(Yii::$app->request->queryParams);
            $dataProvider->query
                ->with([
                    'task',
                    'member',
                    'wechatNumber'
                ]);
            $reviewsWechat = \yii\helpers\ArrayHelper::map(\common\models\common\ReviewsWechat::find()->asArray()->all(), 'id', 'wechat_number');
            $managerArray = \yii\helpers\ArrayHelper::map(Manager::find()->select(['id', 'username'])->asArray()->all(), 'id', 'username');
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'managerArray' => $managerArray,
                'reviewsWechat' => $reviewsWechat,
            ]);
        }
    }

    /**
     * 审核任务
     * @return mixed|string
     * @throws \yii\db\Exception
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionEdit()
    {
        $id = Yii::$app->request->get('id', null);
        $model = TaskBill::find()
            ->where(['id' => $id])
            ->with('member')
            ->with('task')
            ->with(['qualificationAudit' => function ($query) {
                $query->where(['status' => 1]);
            }])
            ->one();
        $old_status = $model->status;
        if (Yii::$app->request->isPost) {
            if ($old_status != 2) {
                return $this->message("该任务已完成审核", $this->redirect(Yii::$app->request->referrer), 'error');
            }
            $taskBill = Yii::$app->request->post('TaskBill');
            if (empty($status = $taskBill['status'])) {
                return $this->message("审核状态必选", $this->redirect(Yii::$app->request->referrer), 'error');
            }
            // 开始执行审核任务流程
            // 开启事务
            $transaction = Yii::$app->db->beginTransaction();
            try {
                // 锁住
                $task_bill_sql = "select `id` from task_task_bill where `id`={$id} and `status` = 2 for update";
                $task_bill_lock = Yii::$app->db->createCommand($task_bill_sql)->queryOne();
                $model = TaskBill::find()
                    ->where(['id' => $task_bill_lock['id'], 'status' => 2])
                    ->with('member')
                    ->with('task')
                    ->one();
                if ($status == 4) { // 审核通过
                    $today = DateHelper::today();
                    if (!TaskBill::find()->where(['member_id' => $model->member_id, 'status' => TaskBill::TaskBillStatus_4])->andWhere(['between', 'updated_at', $today['start'], $today['end']])->exists()) {
                        // 加入统计
                        Statistics::updateOverTaskMember(date("Y-m-d"));
                    }
                    $remark = $taskBill['remark'] ?? "任务审核已通过";
                    $model->status = $status;
                    $model->remark = $remark;
                    $model->review_time = time();
                    $model->created_by = Yii::$app->user->identity['id'];
                    $model->save();
                    //开始给用户加钱
                    $rechargeForm = new RechargeForm([
                        'type' => RechargeForm::TYPE_MONEY,
                        'money' => $model->task->commission,
                        'int' => 0,
                        'remark' => $remark,
                    ]);
                    $rechargeForm->save($member = $model->member, CreditsLog::OVER_TASK_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER);

                    //开始给用户添加积分
                    if ($model->task->integral != 0) {
                        $remark = $taskBill['remark'] ?? "任务审核已通过";
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_INT,
                            'money' => 0,
                            'int' => $model->task->integral,
                            'remark' => $remark,
                        ]);
                        $rechargeForm->save($member = $model->member, CreditsLog::OVER_TASK_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER);
                    }

                    // 如果当前任务有押金,则退还回去
                    if ($model->task->deposit_money > 0) {
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_MONEY,
                            'money' => $model->task->deposit_money,
                            'int' => 0,
                            'remark' => "任务审核已通过，退还押金" . $model->task->deposit_money . "元！",
                        ]);
                        $rechargeForm->save($member, CreditsLog::DEPOSIT_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER);
                    }
                    // 向该用户发送消息
                    Yii::$app->services->webSocket->sendMessageToMember($model->member_id, '任务审核', $remark, '');

                    // 完成任务,根据任务经验,给会员添加经验值
                    if ($model->task->experience != 0) {
                        // 首先添加经验值
                        $memberInfo = Member::findOne($model->member_id);
                        $memberInfo->experience = $memberInfo->experience + $model->task->experience;
                        // 拿到经验配置表的信息
                        $gradeSetting = GradeSetting::getGradeByExperience($memberInfo->experience);
                        $memberInfo->level_id = $gradeSetting;
                        $memberInfo->save(false);
                    }

                    // 加入统计表
                    Statistics::updateOverTask(date("Y-m-d"));

                    // 判断一级代理是否开启
                    if (!empty($primary_agent_percentage = Yii::$app->debris->config('primary_agent_percentage')) && !empty($member->recommend_id)) {
                        // 计算一级代理推荐佣金
                        $one_commission = BcMathHelper::mul(BcMathHelper::div($primary_agent_percentage), $model->task->commission);
                        $one_remark = "您推荐的用户" . $member->username . "成功完成任务,获得推荐佣金" . $one_commission . "元";
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_MONEY,
                            'money' => $one_commission,
                            'int' => 0,
                            'remark' => $one_remark,
                        ]);
                        $rechargeForm->save($member_one = Member::findOne($member->recommend_id), CreditsLog::RECOMMEND_COMMISSION, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, $model->id);
                        // 向该用户发送消息
                        Yii::$app->services->webSocket->sendMessageToMember($member->recommend_id, '获得推荐佣金', $one_remark, '');
                    }
                    // 判断二级代理是否开启
                    if (!empty($secondary_agent_percentage = Yii::$app->debris->config('secondary_agent_percentage')) && !empty($member_one->recommend_id)) {
                        // 计算二级代理推荐佣金
                        $two_commission = BcMathHelper::mul(BcMathHelper::div($secondary_agent_percentage), $model->task->commission);
                        $two_remark = "您的下级" . $member_one->username . "推荐的用户成功完成任务,获得推荐佣金" . $two_commission . "元";
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_MONEY,
                            'money' => $two_commission,
                            'int' => 0,
                            'remark' => $two_remark,
                        ]);
                        $rechargeForm->save($member_two = Member::findOne($member_one->recommend_id), CreditsLog::RECOMMEND_COMMISSION, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, $model->id);
                        // 向该用户发送消息
                        Yii::$app->services->webSocket->sendMessageToMember($member_one->recommend_id, '获得推荐佣金', $two_remark, '');
                    }
                    // 判断三级代理是否开启
                    if (!empty($tertiary_agent_percentage = Yii::$app->debris->config('tertiary_agent_percentage')) && !empty($member_two->recommend_id)) {
                        // 计算一级代理推荐佣金
                        $three_commission = BcMathHelper::mul(BcMathHelper::div($tertiary_agent_percentage), $model->task->commission);
                        $three_remark = "您的下级" . $member_two->username . "推荐的用户成功完成任务,获得推荐佣金" . $three_commission . "元";
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_MONEY,
                            'money' => $three_commission,
                            'int' => 0,
                            'remark' => $three_remark,
                        ]);
                        $rechargeForm->save(Member::findOne($member_two->recommend_id), CreditsLog::RECOMMEND_COMMISSION, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, $model->id);
                        // 向该用户发送消息
                        Yii::$app->services->webSocket->sendMessageToMember($member_two->recommend_id, '获得推荐佣金', $three_remark, '');
                    }

                } else {//审核不通过
                    $remark = $taskBill['remark'] ?? "任务审核已驳回";
                    //  判断提交次数
                    if ($model->task_submit_number >= $model->task->reject_submit_switch) {
                        $model->status = 5;
                        // 如果当前任务有押金,则退还回去
                        if ($model->task->deposit_money > 0) {
                            $rechargeForm = new RechargeForm([
                                'type' => RechargeForm::TYPE_MONEY,
                                'money' => $model->task->deposit_money,
                                'int' => 0,
                                'remark' => $remark,
                            ]);
                            $rechargeForm->save($member = Member::findOne($model->member_id), CreditsLog::DEPOSIT_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER);
                        }
                    } else {
                        $model->status = $status;
                    }
                    $model->review_time = time();
                    $model->remark = $remark;
                    $model->created_by = Yii::$app->user->identity['id'];
                    $model->save();
                    // 向该用户发送消息
                    Yii::$app->services->webSocket->sendMessageToMember($model->member_id, '任务审核', $remark, '');
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                return $this->message("审核失败", $this->redirect(Yii::$app->request->referrer), 'error');
            }
            return $this->message("审核成功", $this->redirect(Yii::$app->request->referrer));
        }
        $model->task_content = unserialize($model->task_content);
        if ($model->task->c_id == 1 || $model->task->c_id == 4) {
            $model->task->release_body = unserialize($model->task->release_body);
        }
        $model->qualificationAudit->verification_img_data = unserialize($model->qualificationAudit->verification_img_data);
        return $this->renderAjax($this->action->id, [
            'model' => $model,
        ]);
    }


    /**
     * 批量通过任务
     * @return array
     * @throws \yii\db\Exception
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function actionPassAll()
    {
        $ids = explode(',', Yii::$app->request->get('ids'));
        if (!empty($ids)) {
            // 先判断所选订单内是否已经被别人处理过
            foreach ($ids as $v1) {
                if (empty(TaskBill::find()->where(['id' => $v1, 'status' => 2])->exists())) {
                    return $this->message('所选项包含已审核内容', $this->redirect(Yii::$app->request->referrer), 'error');
                }
            }
            //再进行操作
            foreach ($ids as $v1) {
                // 开启事务
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $task_bill_sql = "select `id` from task_task_bill where `id`={$v1} and `status` = 2 for update";
                    $task_bill_lock = Yii::$app->db->createCommand($task_bill_sql)->queryOne();
                    $model = TaskBill::find()
                        ->where(['id' => $task_bill_lock['id'], 'status' => 2])
                        ->with('member')
                        ->with('task')
                        ->one();
                    $today = DateHelper::today();
                    if (!TaskBill::find()->where(['member_id' => $model->member_id, 'status' => TaskBill::TaskBillStatus_4])->andWhere(['between', 'updated_at', $today['start'], $today['end']])->exists()) {
                        // 加入统计
                        Statistics::updateOverTaskMember(date("Y-m-d"));
                    }
                    $remark = "任务审核已通过";
                    $model->status = 4;
                    $model->remark = $remark;
                    $model->review_time = time();
                    $model->created_by = Yii::$app->user->identity['id'];
                    $model->save();
                    //开始给用户加钱
                    $rechargeForm = new RechargeForm([
                        'type' => RechargeForm::TYPE_MONEY,
                        'money' => $model->task->commission,
                        'int' => 0,
                        'remark' => $remark,
                    ]);
                    $rechargeForm->save($member = $model->member, CreditsLog::OVER_TASK_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER);

                    //开始给用户添加积分
                    if ($model->task->integral != 0) {
                        $remark = $taskBill['remark'] ?? "任务审核已通过";
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_INT,
                            'money' => 0,
                            'int' => $model->task->integral,
                            'remark' => $remark,
                        ]);
                        $rechargeForm->save($member = $model->member, CreditsLog::OVER_TASK_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER);
                    }

                    // 如果当前任务有押金,则退还回去
                    if ($model->task->deposit_money > 0) {
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_MONEY,
                            'money' => $model->task->deposit_money,
                            'int' => 0,
                            'remark' => "任务审核已通过，退还押金" . $model->task->deposit_money . "元！",
                        ]);
                        $rechargeForm->save($member, CreditsLog::DEPOSIT_TYPE, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER);
                    }
                    // 向该用户发送消息
                    Yii::$app->services->webSocket->sendMessageToMember($model->member_id, '任务审核', $remark, '');

                    // 完成任务,根据任务经验,给会员添加经验值
                    if ($model->task->experience != 0) {
                        // 首先添加经验值
                        $memberInfo = Member::findOne($model->member_id);
                        $memberInfo->experience = $memberInfo->experience + $model->task->experience;
                        // 拿到经验配置表的信息
                        $gradeSetting = GradeSetting::getGradeByExperience($memberInfo->experience);
                        $memberInfo->level_id = $gradeSetting;
                        $memberInfo->save(false);
                    }

                    // 加入统计表
                    Statistics::updateOverTask(date("Y-m-d"));

                    // 判断一级代理是否开启
                    if (!empty($primary_agent_percentage = Yii::$app->debris->config('primary_agent_percentage')) && !empty($member->recommend_id)) {
                        // 计算一级代理推荐佣金
                        $one_commission = BcMathHelper::mul(BcMathHelper::div($primary_agent_percentage), $model->task->commission);
                        $one_remark = "您推荐的用户" . $member->username . "成功完成任务,获得推荐佣金" . $one_commission . "元";
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_MONEY,
                            'money' => $one_commission,
                            'int' => 0,
                            'remark' => $one_remark,
                        ]);
                        $rechargeForm->save($member_one = Member::findOne($member->recommend_id), CreditsLog::RECOMMEND_COMMISSION, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, $model->id);
                        // 向该用户发送消息
                        Yii::$app->services->webSocket->sendMessageToMember($member->recommend_id, '获得推荐佣金', $one_remark, '');
                    }
                    // 判断二级代理是否开启
                    if (!empty($secondary_agent_percentage = Yii::$app->debris->config('secondary_agent_percentage')) && !empty($member_one->recommend_id)) {
                        // 计算二级代理推荐佣金
                        $two_commission = BcMathHelper::mul(BcMathHelper::div($secondary_agent_percentage), $model->task->commission);
                        $two_remark = "您的下级" . $member_one->username . "推荐的用户成功完成任务,获得推荐佣金" . $two_commission . "元";
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_MONEY,
                            'money' => $two_commission,
                            'int' => 0,
                            'remark' => $two_remark,
                        ]);
                        $rechargeForm->save($member_two = Member::findOne($member_one->recommend_id), CreditsLog::RECOMMEND_COMMISSION, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, $model->id);
                        // 向该用户发送消息
                        Yii::$app->services->webSocket->sendMessageToMember($member_one->recommend_id, '获得推荐佣金', $two_remark, '');
                    }
                    // 判断三级代理是否开启
                    if (!empty($tertiary_agent_percentage = Yii::$app->debris->config('tertiary_agent_percentage')) && !empty($member_two->recommend_id)) {
                        // 计算一级代理推荐佣金
                        $three_commission = BcMathHelper::mul(BcMathHelper::div($tertiary_agent_percentage), $model->task->commission);
                        $three_remark = "您的下级" . $member_two->username . "推荐的用户成功完成任务,获得推荐佣金" . $three_commission . "元";
                        $rechargeForm = new RechargeForm([
                            'type' => RechargeForm::TYPE_MONEY,
                            'money' => $three_commission,
                            'int' => 0,
                            'remark' => $three_remark,
                        ]);
                        $rechargeForm->save(Member::findOne($member_two->recommend_id), CreditsLog::RECOMMEND_COMMISSION, AppEnum::BACKEND, CreditsLog::CREDIT_GROUP_MANAGER, $model->id);
                        // 向该用户发送消息
                        Yii::$app->services->webSocket->sendMessageToMember($member_two->recommend_id, '获得推荐佣金', $three_remark, '');
                    }
                    $transaction->commit();
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    return $this->message("审核失败", $this->redirect(Yii::$app->request->referrer), 'error');
                }
            }
            return $this->message("审核成功！", $this->redirect(Yii::$app->request->referrer));
        } else {
            return $this->message("请选择订单", $this->redirect(Yii::$app->request->referrer), 'error');
        }
    }
}
