<?php

use common\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use common\widgets\webuploader\Files;

/* @var $this yii\web\View */
/* @var $model common\models\task\Task */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->isNewRecord ? '创建' : '编辑';
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<?php
//var_dump(unserialize($model->release_body));exit;
?>
<style>
    .table-condensed {
        display: none;
    }

    .drp-calendar .calendar-time {
        display: block;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'template' => "<div class='col-sm-2 text-right'>{label}</div><div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
                    ],
                ]); ?>
                <div class="col-sm-12">
                    <?= $form->field($model, 'c_id',['options'=>['class'=>['chart']]])->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\task\TaskCategory::find()->asArray()->all(), 'id', 'title'), ['readonly' => 'readonly']) ?>
                    <?= $form->field($model, 'title',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'banner',['options'=>['class'=>['chart']]])->widget(\backend\widgets\cropper\Cropper::class, [
                        'formData' => [
                            'drive' => Yii::$app->debris->config('backend_upload_drive'),// 默认本地 支持 qiniu/oss 上传
                        ],
                    ]); ?>
                    <?= $form->field($model, 'commission',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'integral',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'experience',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'task_number',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'deposit_money',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true])->hint("*默认0元则无押金", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'consume_integral',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true])->hint("*默认0则无消耗", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'receive_time',['options'=>['class'=>['chart']]])->hint("*默认为空则无时间限制", ['style' => 'color:red'])->widget('\kartik\daterange\DateRangePicker',
                        [
                            'convertFormat' => true,
                            'name' => 'receive_time',
                            'hideInput' => true,
                            'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                            'pluginOptions' => [
                                'timePicker' => true,
                                'locale' => [
                                    'format' => 'H:i:s',
                                    'separator' => '~'
                                ],
//                                'opens' => 'left'
                            ],
                            'pluginEvents' => [
                                "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                            ]
                        ]) ?>
                    <?= $form->field($model, 'receive_to_submit_time',['options'=>['class'=>['chart']]])->textInput()->hint("*默认0分则无时间限制", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'over_to_recevice_time',['options'=>['class'=>['chart']]])->textInput()->hint("*默认0分则无时间限制", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'deposit_return',['options'=>['class'=>['chart']]])->radioList([1 => '是', 0 => '否']) ?>
                    <?= $form->field($model, 'upload_img_number',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'reject_submit_switch',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'release_title',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?php
                    if ($model->c_id == 1 || $model->c_id == 4) { //等于1,4为图文任务
                        echo $form->field($model, 'release_body',['options'=>['class'=>['chart']]])->widget(\common\widgets\webuploader\Files::class, [
                            'type' => 'images',
                            'theme' => 'default',
                            'config' => [
                                'pick' => [
                                    'multiple' => true,
                                ],
                                'formData' => [
                                    'drive' => Yii::$app->debris->config('backend_upload_drive'),// 默认本地 支持 qiniu/oss 上传
                                ],
                            ]
                        ]);
                    } elseif ($model->c_id == 3) { // 等于3为视频任务
                        echo $form->field($model, 'release_body',['options'=>['class'=>['chart']]])->widget(Files::class, [
                            'type' => 'videos',
                            'theme' => 'default',
                            'themeConfig' => [],
                            'config' => [
                                'pick' => [
                                    'multiple' => false,
                                ],
                                'accept' => [
                                    'mimeTypes' => 'video/*'
                                ],
                                'formData' => [
                                    'drive' => Yii::$app->debris->config('backend_upload_drive'),// 默认本地 支持 qiniu/oss 上传
                                ],
                            ]
                        ]);
                    } else {
                        echo $form->field($model, 'release_body',['options'=>['class'=>['chart']]])->textInput();
                    }
                    ?>
                    <?= $form->field($model, 'release_comment',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'remark',['options'=>['class'=>['chart']]])->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'receive_hint',['options'=>['class'=>['chart']]])->textarea(['rows' => 6]) ?>
                    <?= $form->field($model, 'l_id',['options'=>['class'=>['chart']]])->radioList(\yii\helpers\ArrayHelper::map(\common\models\task\TaskLabel::find()->asArray()->all(), 'id', 'label')) ?>
                    <?= $form->field($model, 'sort',['options'=>['class'=>['chart']]])->textInput()->hint("*越大越靠前", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'is_repeat_receive',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true])->hint("*等于0时则不限制次数", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'wechat_number',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ->hint("若填入则用户领取任务时展示此微信号，若不填则展示用户分配的微信号", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'grade',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true])->hint("*会员等级等于此值才可见此任务", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'is_open',['options'=>['class'=>['chart']]])->radioList([1 => '是', 0 => '否'])->hint("*若开启,则所有会员都可见此任务", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'receive_Intervals',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true])->hint("*单位：小时，若填入0，则不限制", ['style' => 'color:red']) ?>
                    <?= $form->field($model, 'status',['options'=>['class'=>['chart']]])->radioList([1 => '启用', 0 => '禁用']) ?>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-center">
                        <button class="btn btn-primary" type="submit">保存</button>
                        <span class="btn btn-white" onclick="history.go(-1)">返回</span>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
