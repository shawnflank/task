<?php

use common\helpers\Html;
use yii\widgets\ActiveForm;
use common\widgets\webuploader\Files;

/* @var $this yii\web\View */
/* @var $model common\models\task\TaskBill */
/* @var $form yii\widgets\ActiveForm */


$this->title = $model->isNewRecord ? '创建' : '编辑';
$this->params['breadcrumbs'][] = ['label' => 'Task Bills', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #ajaxModal {
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .modal-dialog {
        width: 60%;
    }

    .modal-content {
        width: 100%;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'template' => "<div class='col-sm-2 text-right'>{label}</div><div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
                    ],
                ]); ?>
                <div class="col-sm-12">
                    <?= $form->field($model->member, 'username',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly']) ?>

                    <?php
                    echo $form->field($model->qualificationAudit, 'verification_img_data',['options'=>['class'=>['chart']]])->widget(\common\widgets\webuploader\Files::class, [
                        'type' => 'images',
                        'theme' => 'default',
                        'config' => [
                            'pick' => [
                                'multiple' => true,
                            ],
                            'formData' => [
                                'drive' => 'local',// 默认本地 支持 qiniu/oss 上传
                            ],
                        ]
                    ]);
                    ?>

                    <?= $form->field($model->qualificationAudit, 'verification_text_data',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly']) ?>

                    <?= $form->field($model->task, 'title',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly']) ?>
                    <?= $form->field($model->task, 'commission',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly']) ?>
                    <?= $form->field($model->task, 'remark',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly']) ?>
                    <?= $form->field($model->task, 'release_title',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly'])->label('要求发布内容') ?>

                    <?php
                    if ($model->task->c_id == 1 || $model->task->c_id == 4) { //等于1,4为图文任务
                        echo $form->field($model->task, 'release_body',['options'=>['class'=>['chart']]])->label('要求发布主体')->widget(\common\widgets\webuploader\Files::class, [
                            'type' => 'images',
                            'theme' => 'default',
                            'config' => [
                                'pick' => [
                                    'multiple' => true,
                                ],
                                'formData' => [
                                    'drive' => 'local',// 默认本地 支持 qiniu/oss 上传
                                ],
                            ]
                        ]);
                    } elseif ($model->task->c_id == 3) { // 等于3为视频任务
                        echo $form->field($model->task, 'release_body',['options'=>['class'=>['chart']]])->label('要求发布主体')->widget(Files::class, [
                            'type' => 'videos',
                            'theme' => 'default',
                            'themeConfig' => [],
                            'config' => [
                                'pick' => [
                                    'multiple' => false,
                                ],
                                'accept' => [
                                    'mimeTypes' => 'video/*'
                                ],
                                'formData' => [
                                    'drive' => 'local',// 默认本地 支持 qiniu/oss 上传
                                ],
                            ]
                        ]);
                    } else {
                        echo $form->field($model->task, 'release_body',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly'])->label('要求发布主体');
                    }
                    ?>

                    <?= $form->field($model->task, 'release_comment',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly'])->label('要求发布评论') ?>
                    <?= $form->field($model->task, 'reject_submit_switch',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly'])->label('允许提交次数') ?>
                    <?= $form->field($model, 'task_submit_number',['options'=>['class'=>['chart']]])->textInput(['readonly' => 'readonly'])->label('已提交任务次数') ?>
                    <?php
                    echo $form->field($model, 'task_content',['options'=>['class'=>['chart']]])->widget(\common\widgets\webuploader\Files::class, [
                        'type' => 'images',
                        'theme' => 'default',
                        'config' => [
                            'pick' => [
                                'multiple' => true,
                            ],
                            'formData' => [
                                'drive' => 'local',// 默认本地 支持 qiniu/oss 上传
                            ],
                        ]
                    ]);
                    ?>
                    <?= $form->field($model, 'remark',['options'=>['class'=>['chart']]])->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'status',['options'=>['class'=>['chart']]])->radioList([4 => '通过', 3 => '拒绝']) ?>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-center">
                        <?php if ($model->status == 2) { ?>
                            <button class="btn btn-primary" type="submit">保存</button>
                        <?php } ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
