<?php

use common\helpers\Html;
use common\helpers\Url;
use kartik\grid\GridView;
use common\models\task\TaskBill;
use common\helpers\DateHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '任务订单列表';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
                <div class="box-tools">
                </div>
            </div>
            <div class="box-body table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'hover' => true,
                    "options" => ["class" => "grid-view", "style" => "overflow:auto", "id" => "grid"],
                    'tableOptions' => ['class' => 'table table-hover'],
                    'columns' => [
//                        [
//                            'class' => 'yii\grid\SerialColumn',
//                            'visible' => false,
//                        ],
                        // 若要全选则关闭上面打开下面的代码
                        [
                            'class' => '\kartik\grid\CheckboxColumn',
                            'rowSelectedClass' => GridView::TYPE_INFO,
                            'visible' => true,
                        ],

                        'id',
                        [
                            'label' => '会员信息',
                            'attribute' => 'member_id',
                            'filter' => Html::activeTextInput($searchModel, 'member_id', [
                                    'class' => 'form-control',
                                    'placeholder' => '会员账号'
                                ]
                            ),
                            'value' => function ($model) {
                                return "账号：" . $model->member->username . '<br>' .
                                    "昵称：" . $model->member->nickname . '<br>' ;
                            },
                            'format' => 'raw',
                        ],

                        [
                            'class' => '\kartik\grid\EditableColumn',
                            'attribute' => 'notice',
                            'value' => function ($model) {
                                if (!empty($model->member->notice)) {
                                    if (mb_strlen($model->member->notice) > 10) {
                                        $notice = mb_substr($model->member->notice, 0, 10, 'utf-8') . "..";
                                    } else {
                                        $notice = $model->member->notice;
                                    }
                                } else {
                                    $notice = "(无)";
                                }

                                return $notice;
                            },
                        ],

                        [
                            'label' => '任务信息',
                            'attribute' => 't_id',
                            'filter' => Html::activeTextInput($searchModel, 't_id', [
                                    'class' => 'form-control',
                                    'placeholder' => '任务名称'
                                ]
                            ),
                            'value' => function ($model) {
                                return "任务类型：" . $model->task->category->title . '<br>' .
                                    "任务名称：" . $model->task->title . '<br>' .
                                    "任务佣金：" . $model->task->commission . '<br>';
                            },
                            'format' => 'raw',
                        ],
                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'remark',
                            'editableOptions' => [
                                'asPopover' => false,
                                'inputType' => \kartik\editable\Editable::INPUT_TEXTAREA,//只需添加如下代码
                                'options' => [
                                    'rows' => 4,
                                ],
                            ],
                            'value' => function ($model) {
                                if (!empty($model->remark) && mb_strlen($model->remark) > 5) {
                                    return mb_substr($model->remark, 0, 5, 'utf-8') . "..";
                                } else {
                                    return $model->remark;
                                }
                            },
                            'headerOptions' => ['width' => '109px'],
                        ],
//                        'task_submit_number',
//                        'task_content',
//                        'remark',

                        [
                            'attribute' => 'task_submit_number',
                            'headerOptions' => ['width' => '75px'],
                        ],

                        [
                            'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'w_id',
                            'filterType' => GridView::FILTER_SELECT2,
                            'editableOptions' => [
                                'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
//                                'asPopover' => false,
                                'data' => $reviewsWechat,
                            ],
                            'filterWidgetOptions' => [
                                'data' => $reviewsWechat,
                                'options' => [
                                    'prompt' => "请选择",
                                ],
                                'hideSearch' => true,
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ],
                            'filter' => $reviewsWechat,
                            'value' => function ($model) use ($reviewsWechat) {
                                if ($model->w_id != 0) {
                                    return isset($reviewsWechat[$model->w_id]) ? $reviewsWechat[$model->w_id] : '失效';
                                } else {
                                    return "未绑定";
                                }
                            },
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'filter' => Html::activeDropDownList($searchModel, 'status', TaskBill::$taskBillStatusArray, [
                                    'prompt' => '全部',
                                    'class' => 'form-control'
                                ]
                            ),
                            'value' => function ($model) {

                                return '<span class="label label-' . TaskBill::$taskBill_status_color[$model->status] . '">' . TaskBill::$taskBillStatusArray[$model->status] . '</span>';
                            },
                        ],
                        [
                            'attribute' => 'created_by',
                            'filter' => Html::activeDropDownList($searchModel, 'created_by', $managerArray, [
                                    'prompt' => '全部',
                                    'class' => 'form-control'
                                ]
                            ),
                            'value' => function ($model) use ($managerArray) {
                                return $managerArray[$model->created_by] ?? "(暂无)";
                            },
                        ],
                        [
                            'attribute' => 'created_at',
                            'filter' => \kartik\daterange\DateRangePicker::widget([
                                'model' => $searchModel,
                                'convertFormat' => true,
                                'name' => 'created_at',
                                'attribute' => 'created_at',
                                'hideInput' => true,
                                'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                'pluginOptions' => [
                                    'timePicker' => true,
                                    'locale' => [
                                        'format' => 'Y-m-d',
                                        'separator' => '~'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                ]
                            ]),
                            'value' => function ($model) {
                                return DateHelper::dateTime($model->created_at);
                            },
                            'headerOptions' => ['width' => '102px'],
                        ],
                        [
                            'attribute' => 'updated_at',
                            'filter' => \kartik\daterange\DateRangePicker::widget([
                                'model' => $searchModel,
                                'convertFormat' => true,
                                'name' => 'updated_at',
                                'attribute' => 'updated_at',
                                'hideInput' => true,
                                'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                'pluginOptions' => [
                                    'timePicker' => true,
                                    'locale' => [
                                        'format' => 'Y-m-d',
                                        'separator' => '~'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                ]
                            ]),
                            'value' => function ($model) {
                                if ($model->updated_at == 0) {
                                    return "未提交";
                                }
                                return DateHelper::dateTime($model->updated_at);
                            },
                            'headerOptions' => ['width' => '102px'],
                        ],
                        [
                            'attribute' => 'review_time',
                            'filter' => \kartik\daterange\DateRangePicker::widget([
                                'model' => $searchModel,
                                'convertFormat' => true,
                                'name' => 'review_time',
                                'attribute' => 'review_time',
                                'hideInput' => true,
                                'options' => ['placeholder' => '请选择时间段...', 'class' => 'form-control'],
                                'pluginOptions' => [
                                    'timePicker' => true,
                                    'locale' => [
                                        'format' => 'Y-m-d',
                                        'separator' => '~'
                                    ],
                                    'opens' => 'left'
                                ],
                                'pluginEvents' => [
                                    "cancel.daterangepicker" => "function(ev, picker) {
                            $(picker.element[0].children[1]).val('');
                            $(picker.element[0].children[0].children[1]).val('').trigger('change');
                        }"
                                ]
                            ]),
                            'value' => function ($model) {
                                if ($model->review_time == 0) {
                                    return "未审核";
                                }
                                return DateHelper::dateTime($model->review_time);
                            },
                            'headerOptions' => ['width' => '102px'],
                        ],

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'header' => '操作',
                            'template' => '{review}{delete}',
                            'buttons' => [
                                'review' => function ($url, $model, $key) {
                                    // 为2时是已提交任务
                                    if ($model->status == 2) {
                                        return Html::linkButton(['edit', 'id' => $model->id], '审核', [
                                            'class' => 'btn btn-success btn-sm',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#ajaxModal',
                                        ]);
                                    } else {
                                        return Html::linkButton(['edit', 'id' => $model->id], '详情', [
                                            'class' => 'btn btn-primary btn-sm',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#ajaxModal',
                                        ]);
                                    }
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::delete(['delete', 'id' => $model->id]);
                                },
                            ]
                        ]
                    ],
                    'panel' => [
                        'heading' => false,
                        'before' => '<div class="box-header pull-left"><i class="fa fa-fw fa-sun-o"></i><h3 class="box-title">数据管理</h3></div>',
                        'footer' => false,
                        'after' => '<div class="pull-left" style="margin-top: 8px">{summary}</div><div class="kv-panel-pager pull-right">{pager}</div><div class="clearfix"></div>',
                    ],
                    'panelFooterTemplate' => '{footer}<div class="clearfix"></div>',
                    'toolbar' => [
                        '<div class="pull-left">'
                        . Html::a('<i class="glyphicon  glyphicon-ok-circle"></i>批量通过', Url::to('pass-all'), ['class' => 'btn btn-success', 'id' => 'bulk_forbid'])
                        . '</div>',
                        '{toggleData}',
                        '{export}'
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('
$("#bulk_forbid").on("click", function (e) {
    e.preventDefault();
    var keys = $("#grid").yiiGridView("getSelectedRows");
    if(keys.length < 1) {
        return rfError("", "没有选中任何项");
    }
    var href = $(this).attr("href");
    window.location.href = href + "?ids=" + keys.join();
});
');
?>
