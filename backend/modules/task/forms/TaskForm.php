<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------


namespace backend\modules\task\forms;

use common\models\task\TaskCategory;
use yii\base\Model;

class TaskForm extends Model
{
    public $c_id;

    public function rules()
    {
        return [
            [['c_id'], 'integer'],
            [['c_id'], 'in', 'range' => TaskCategory::find()->select(['id'])->column()],
            [['c_id'], 'default', 'value' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'c_id' => '分类',
        ];
    }
}