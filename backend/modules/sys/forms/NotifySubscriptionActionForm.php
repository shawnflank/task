<?php

namespace backend\modules\sys\forms;

use yii\base\Model;

/**
 * Class NotifySubscriptionActionForm
 * @package backend\modules\sys\forms
 * @author 原创脉冲 <QQ：2790684490>
 */
class NotifySubscriptionActionForm extends Model
{
    public $sys;
    public $dingtalk;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sys', 'dingtalk'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sys' => '系统',
            'dingtalk' => '钉钉',
        ];
    }
}
