<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <?= Yii::$app->debris->config('web_copyright'); ?>
    </div>
    当前版本：<?= Yii::$app->version; ?>
</footer>
<?php
$this->registerJsFile('@web/resources/dist/js/web_socket.js');
$this->registerJsFile('@web/resources/dist/js/ycmc.js');
?>