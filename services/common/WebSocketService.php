<?php
// +----------------------------------------------------------------------------------------
// | 原创项目
// +----------------------------------------------------------------------------------------
// | 版权所有 原创脉冲工作室
// +----------------------------------------------------------------------------------------
// |  联系方式：
// |  QQ：2790684490
// |  skype：live:.cid.3adbd0e19c228153
// |  Telegram：@coderleo
// +----------------------------------------------------------------------------------------
// | 开发团队:原创脉冲
// +----------------------------------------------------------------------------------------

namespace services\common;

use common\components\Service;
use common\helpers\ArrayHelper;
use common\models\common\MessageLists;
use GatewayClient\Gateway;

class WebSocketService extends Service
{
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        Gateway::$registerAddress = "127.0.0.1:1239";
    }

    /**
     * 向admin组里发送消息
     * @param $member_id
     * @param $title
     * @param $content
     * @param $jump_url
     * @author 原创脉冲 <QQ：2790684490>
     */
    public function sendMessageToGroup($member_id, $title, $content, $jump_url)
    {
        if ($message = MessageLists::createdNewModel($member_id, $title, $content, $jump_url)) {
            $data = ArrayHelper::toArray($message, [
                'common\models\MessageLists' => [
                    'id',
                    'content',
                    'created_at',
                    'title',
                    'jump_url',
                    'jump_type'
                ],
            ]);
            $this->sendGroupMessage('admin', $data, 'new_message');
        }
    }

    /**
     * 向用户发送消息
     * @param $member_id
     * @param $title
     * @param $content
     * @param $jump_url
     * @param string $message_type
     */
    public function sendMessageToMember($member_id, $title, $content, $jump_url,$message_type = "member_message")
    {
        if ($message = MessageLists::createdNewModel($member_id, $title, $content, $jump_url,1)) {
            $data = ArrayHelper::toArray($message, [
                'common\models\MessageLists' => [
                    'id',
                    'content',
                    'created_at',
                    'title',
                    'jump_url',
                    'jump_type'
                ],
            ]);
            $this->sendOneMessage($member_id, $data, $message_type);
        }
    }


    /**
     * 群发消息
     * @param $message
     * @param $type
     * @throws \Exception
     */
    public function sendAllMessage($message, $type)
    {
        $online_clients = Gateway::getAllClientSessions();
        if ($online_clients && $message) {
            foreach ($online_clients as $k => $v) {
                $online_clientALL[] = $k;
            }
            $datas = [
                "type" => $type,
                "data" => $message,
            ];
            Gateway::sendToAll(json_encode($datas), $online_clientALL);
        }
    }


    /**
     * 单发消息
     * @param $user_id
     * @param $message
     * @param $type
     */
    public function sendOneMessage($user_id, $message, $type)
    {
        $datas = [
            "type" => $type,
            "data" => $message,
        ];
        Gateway::sendToUid($user_id, json_encode($datas));
    }


    /**
     * 向组里消息
     */
    public function sendGroupMessage($group, $message, $type)
    {
        $datas = [
            "type" => $type,
            "data" => $message,
        ];
        Gateway::sendToGroup($group, json_encode($datas));
    }

    /**
     * 向组里添加client_id
     */
    public function addClientIdToGroup($client_id, $group)
    {
        Gateway::joinGroup($client_id, $group);
    }
}