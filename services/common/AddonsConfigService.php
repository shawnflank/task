<?php

namespace services\common;

use common\components\Service;
use common\models\common\AddonsConfig;

/**
 * Class AddonsConfigService
 * @package services\common
 * @author 原创脉冲 <QQ：2790684490>
 */
class AddonsConfigService extends Service
{
    /**
     * @param $name
     * @return array|\yii\db\ActiveRecord|null
     */
    public function findByName($name)
    {
        return AddonsConfig::find()
            ->where(['addons_name' => $name, 'merchant_id' => $this->getMerchantId()])
            ->one();
    }
}