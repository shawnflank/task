<?php

namespace services\common;

use Yii;
use common\components\Service;

/**
 * Class AuthService
 * @package services\common
 * @author 原创脉冲 <QQ：2790684490>
 */
class AuthService extends Service
{
    /**
     * 是否超级管理员
     *
     * @return bool
     */
    public function isSuperAdmin()
    {
        return Yii::$app->user->id == Yii::$app->params['adminAccount'];
    }
}