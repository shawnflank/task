<?php
/**
 * Created by PhpStorm.
 * User: Serlon
 * Date: 2019/11/24
 * Time: 2:39
 */

namespace services\common;
use common\components\Service;
use common\enums\StatusEnum;
use common\models\common\ImgCategory;


class ImgListsService extends Service
{
    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getListWithValue()
    {
        return ImgCategory::find()
            ->select(['id','key'])
            ->where(['status' => StatusEnum::ENABLED])
            ->with(['imgDetails'])
            ->asArray()
            ->all();
    }

}