<?php

namespace services\wechat;

use common\models\wechat\ReplyDefault;
use common\components\Service;

/**
 * Class ReplyDefaultService
 * @package services\wechat
 * @author 原创脉冲 <QQ：2790684490>
 */
class ReplyDefaultService extends Service
{
    /**
     * @return array|ReplyDefault|null|\yii\db\ActiveRecord
     */
    public function findOne()
    {
        if (empty(($model = ReplyDefault::find()->andFilterWhere(['merchant_id' => $this->getMerchantId()])->one()))) {
            return new ReplyDefault();
        }

        return $model;
    }
}