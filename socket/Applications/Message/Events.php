<?php

use \GatewayWorker\Lib\Gateway;
use Workerman\Lib\Timer;
use Workerman\MySQL\Connection;

require_once __DIR__ . '/../../src/Connection.php';
require_once __DIR__ . '/../Config/config.php';

class Events
{
    public static $db = null;

//    public static function onWorkerStart($worker)
//    {
//        self::$db = new Connection(DB_CONFIG['host'], DB_CONFIG['port'], DB_CONFIG['user'], DB_CONFIG['password'], DB_CONFIG['db_name']);
//    }

    public static function onConnect($client_id)
    {
        Gateway::sendToClient($client_id, json_encode([
            'type' => 'init',
            'client_id' => $client_id,
        ]));
        $_SESSION['auth_timer_id'] = Timer::add(5, function($client_id){
            Gateway::closeClient($client_id);
        }, array($client_id), false);
    }

    public static function onMessage($client_id, $message)
    {
        $req_data = json_decode($message, true);
        if ($req_data['type'] == 'init') {
            Timer::del($_SESSION['auth_timer_id']);
        }
    }

    /**
     * 当用户断开连接时触发的方法
     */
    public static function onClose($client_id)
    {

    }

}
