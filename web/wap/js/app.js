$(function(){
    // 鏈嶅姟鏉℃寮圭獥
    $('.modal_term').bind('click',function(){
        $('#termModal').modal('show');
        $.post(app.host+'welcome/get_page',{'mark':'term'},function(response){
            $('#termModal .modal-title').html(response.title);
            $('#termModal .modal-body').html(response.content);
        },'json');
    })

    // 闅愬紡澹版槑寮圭獥
    $('.modal_privacy').bind('click',function(){
        $('#privacyModal').modal('show');
        $.post(app.host+'welcome/get_page',{'mark':'privacy'},function(response){
            $('#privacyModal .modal-title').html(response.title);
            $('#privacyModal .modal-body').html(response.content);
        },'json');
    })
});

/**
 * 娑堟伅鎻愮ず锛堝叏灞€锛�
 * @param opts
 */
function alertApp(opts){
    opts.msg = opts.msg ? opts.msg : '';
    opts.icon = opts.icon ? opts.icon : 1;
    opts.call = opts.call ? opts.call : function(){};
    if(opts.msg){
        if(opts.icon==1){ layer.alert(opts.msg, {icon:opts.icon,title:false},opts.call); }
        else{ layer.msg(opts.msg, {icon:opts.icon},opts.call); }
    }
}

/**
 * 鍙戦€佹ā鏉块€氱煡锛堥偖浠讹級
 * @param mark 妯℃澘鏍囪瘑
 * @param btn  鍙戦€佹寜閽彞鏌� $(this)
 */
function sendMail(mark, btn){
    var D = {to:'',mark:mark};
    if(mark == 'forget_verify'){
        D.to = $('#emailForget').val();
    }else if(mark == 'register_verify'){
        D.to = $('#emailReg').val();
    }

    // 鎸夐挳鍔犺浇涓�
    btn.button('loading');
    // 鍙戦€侀偖浠�
    $.post(app.host+'welcome/send_mail',D,function(response){
        btn.button('reset');
        if(response.code > 0){
            layer.msg(response.msg, { icon:2 });
        }else{
            layer.alert(response.msg, { icon:1,title:false });
        }
    },'json');
}

/**
 * 鍒濆鍖杅ileinput鎺т欢锛堢涓€娆″垵濮嬪寲锛�
 * @param ctrlName
 * @param uploadUrl
 */


/**
 * 鍒濆鍖杅ileinput鎺т欢锛堢涓€娆″垵濮嬪寲锛�
 * @param ctrlName
 * @param uploadUrl
 * @param opts
 */
function loadFileInput(ctrlName, uploadUrl, opts) {
    var control = $('#' + ctrlName);
    opts = (typeof opts != 'undefined') ? opts : {};
    control.fileinput({
        language: 'zh',
        uploadUrl: uploadUrl,
        allowedFileExtensions : (typeof opts.ext != 'undefined') ? opts.ext : ['jpg', 'png','gif'],
        showUpload: false,
        showCaption: (typeof opts.showCaption != 'undefined') ? opts.showCaption : false,
        showPreview: (typeof opts.showPreview != 'undefined') ? opts.showPreview : true,
        showRemove: (typeof opts.showRemove != 'undefined') ? opts.showRemove : false,
        browseClass: "btn btn-default",
        removeClass: "btn btn-default",
        cancelClass: "btn btn-default",
        uploadClass: "btn btn-default",
    });
}