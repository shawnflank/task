WEB_SOCKET_SWF_LOCATION = "/backend/resources/dist/swf/WebSocketMain.swf";
var WEB_SOCKET_DEBUG = true;
var WEB_SOCKET_SUPPRESS_CROSS_DOMAIN_SWF_ERROR = true;
var socket_ping_itv; // 心跳定时器
var ws; //websocket实例
var lockReconnect = false; //避免重复连接
// 连接socket
var my_host = document.domain;
var my_domain = my_host.split('.');
if (my_domain.length === 3) {
    my_host = my_domain[1] + '.' + my_domain[2];
}
var url = (location.protocol === 'https:' ? 'wss:' : 'ws:') + 'ws.' + my_host + ":8282";


// 创建socket连接
function createWebSocket(url) {
    try {
        ws = new WebSocket(url);
        socketHandle();
    } catch (e) {
        reconnect(url);
    }
}


// 重连
function reconnect() {
    if (lockReconnect) return;
    lockReconnect = true;
    //没连接上会一直重连，设置延迟避免请求过多
    setTimeout(function () {
        createWebSocket(url);
        lockReconnect = false;
    }, 2000);
}

function socketHandle() {
    ws.onerror = function () {
        reconnect(url);
    };
    if (ws.readyState == 3) {
        reconnect(url);
    }
    ws.onclose = function () {
        reconnect(url);
    };
    ws.onopen = function () {
        // console.log('连接成功');
        // 发送心跳
        if (socket_ping_itv) {
            clearInterval(socket_ping_itv);
        }
        socket_ping_itv = setInterval(function () {
            ws.send('ping');
        }, 10000);
    };
    ws.onmessage = function (e) {
        // json数据转换成js对象
        var data = eval("(" + e.data + ")");
        var type = data.type || '';
        console.log(data);
        switch (type) {
            case 'init':
                ws.send(JSON.stringify({type: 'init'}));
                binding(data);
                break;
            case 'new_message':
                getMessage(data);
                break;
            default:
                break;
        }
    };
}

// 绑定后台用户
function binding(data) {
    // console.log(data);
    $.ajax({
        type: 'post',
        data: {
            'client_id': data.client_id
        },
        url: '/backend/common/message-lists/binding',
        success: function (d) {
            // console.log(d);
            if (d.code == 200) {
                 console.log(d.message);
            }else{
                 console.log(d.message);
            }
        },
    });
}

//接收短信
function getMessage(data) {
    $('.iframe_music').remove();
    $('body').append('<video controls="" class="iframe_music" autoplay="" name="media" style="display: none"><source src="/backend/resources/dist/music/music.mp3" type="audio/x-wav"></video>');//src='声音地址'



    // 底部消息通知(备用)
    // var config = {
    //     title: data.data.title,//通知标题部分  默认 新消息   可选
    //     body: data.data.content,//通知内容部分
    //     onclick: function () {
    //         window.location.href =  data.data.jump_url;
    //     },
    // };
    // new dToast(config);

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-top-right",
        "onclick": function (e) {
            var m_id = this.m_id;
            $.ajax({
                type: 'get',
                data: {
                    'id': m_id
                },
                url: '/backend/common/message-lists/view',
                success: function (d) {
                    if (d.code == 200) {
                        window.openConTab($('<a class="J_menuItem" id="jumpButton" href="'+d.data.jump_url+'" data-index=""><i class="fa  rf-i"></i><span>'+d.data.title+'</span></a>'));
                    } else {
                        alert(d.message);
                    }
                },
                error: function () {
                    alert('系统繁忙,请稍后再试');
                }
            });
        },
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "1000",//设置自动关闭时间
        "extendedTimeOut": "0",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
    };
    toastr.options.m_id = data.data.id;
    toastr.success(timestampToTime(data.data.created_at), data.data.content);
}

// 转换时间戳
function timestampToTime(timestamp) {
    var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
    var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
    var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
    var s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
    return Y + M + D + h + m + s;
}


createWebSocket(url);